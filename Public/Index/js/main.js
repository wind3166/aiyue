//租練價格
$('.yes').click(function(){
    var id=$(this).val();    
    var td=$(this).parents("td").siblings("td").find(".tdclass");
    $.ajax({       
        type: "post",
        url: rentAjax,
        data: {"id": id},
        dataType: 'json', // 服务器返回的数据类型 可选XML ,Json jsonp script html  text等                                                                        
        success: function (data) {
            td.empty();           
            for(var j in data){
                var newPrice='<option>'+data[j]+'</option>';
                td.append(newPrice);
            }
        }
    });
});

    $('.toggle-menu').click(function (e) {
        $('.menu-responsive').slideToggle().end();
        e.preventDefault();
    });

 //留言
         var ww = $(document.body).width();
        if (ww <= 980){
                $(".messge_nav").click(function(){
                if($(".messge").css("margin-bottom")==="-290px"){
                    $(".messge").animate({marginBottom: "0"}, 300);   
                }else{
                    $(".messge").animate({marginBottom: "-290"}, 300);   
                }
            });
        }else{
                $(".messge_nav").click(function(){
                 if($(".messge").css("margin-left")==="-300px"){
                     $(".messge").animate({marginLeft: "0"}, 300);   
                     $(".message_content").css("float","right");
                     $(this).css("float","left"); 
                 }else{
                     $(".messge").animate({marginLeft: "-300"}, 300);   
                     $(".message_content").css("float","left");
                     $(this).css("float","right"); 
                 }
       });
  }
   


 //get height
var height = $(window).height();
 $(".about-con,.exma").css("min-height",height-120);
 
/*價格為空不能提交*/
    $(".btn-blue").click(function(){
        if($(".book_val").val()==''){
            alert("暫無此組合，請重新選擇課程");
        }else if($("#username").val()=='')
        {
           alert("請輸入您的姓名"); 
        }else if($("#mobile").val()=='')
        {
           alert("請輸入您的手機號碼"); 
        }else{
            $('#form1').submit();
        }
    });


// 课程預約
  $(document).on('click','.form-control>li',function(){
     var id = $(this).parents(".row").prev(".id").html(),
     course = $(this).parents(".row").prev().prev(".booking_title2").text(),
     adress = $(this).parents(".table").find("#adress li.active").text(),
     number = $(this).parents(".table").find("#number li.active").text(),
     level = $(this).parents(".table").find("#level li.active").text(),
     clas = $(this).parents(".table").find("#class li.active").text(),
     time = $(this).parents(".table").find("#time li.active").text(),
     price = $(this).parents(".row").find(".price_num");
   
    // alert(adress);
     
     
     $(this).addClass("active").siblings().removeClass("active");
      var th = $(this).parents("tbody").siblings("thead").find("th").size(),
          td = $(this).parents("tr").find("li.active").size(),
           n = $(this).parents(".table_content").find(".price_num").size();
    if (th===td){ 
           $(this).parents(".table_content").siblings(".text-center").find(".btn").css({"background-color":"#3b4a9d","color":"#fff"});
        $.ajax({       
            type: "post",
            url: priceAjax,
            data: {'coursename':id,'address':adress,'num':number,'grade':level,'Hallnum':clas,'time':time},
            dataType: 'json', // 服务器返回的数据类型 可选XML ,Json jsonp script html  text等                                                                        
            success: function (data) {
              if(data===null){
                price.text("null");
                }else{
                   price.html(data);
               }
            }
        }); 
     }
 });

  $(".btn").not("#submitSignin").not("#btnsubmusic").not("#btnsubinstru").click(function () {
       var th = $(this).parents(".row").find("th").size(),
            td = $(this).parents(".row").find("li.active").size(),
            n = $(this).parents(".row").find(".price_num").text();
        if(th!==td){
            alert("請全選");
             return false;  
        }else if(n==="null"){
            alert("該課程未有數據");  
            return false;
        }else{
            var course = $(this).parents(".row").prev().prev(".booking_title2").html(),
            id= $(this).parents(".row").prev(".id").html(),
            adress = $(this).parents(".row").find("#adress>li.active").text(),
            number = $(this).parents(".row").find("#number>li.active").text(),
            level = $(this).parents(".row").find("#level>li.active").text(),
            clas = $(this).parents(".row").find("#class>li.active").text(),
            time = $(this).parents(".row").find("#time>li.active").text(),
            price = $(this).parents(".row").find(".price_num").html();
            
            
            //alert(adress);
            
            $(".course_val").attr('value',course);
            $(".course_val2").html(course);
            
            $(".adress_val").attr('value',adress);
            $(".adress_val2").html(adress);
            
            $(".number_val").attr('value',number);
            $(".number_val2").html(number);
            
            $(".level_val").attr('value',level);
            $(".level_val2").html(level);
            
            $(".class_val").attr('value',clas);
            $(".class_val2").html(clas);
            
            $(".time_val").attr('value',time);
            $(".time_val2").html(time);
            
            $(".book_val").attr('value',price);
            $(".book_val2").html(price);
            
            $(".id_val").attr('value',id);
            $(".id_val2").html(id);
        }
});
//圖片居中
     $(window).load(function (){
      $(".div_con").find("img").each(function(){
        var wd = $(this).parents(".div_con").width(),
              hd = $(this).parents(".div_con").height(),
              wimg_a = $(this).width(),
              himg_a = $(this).height(); //圖片初始寬高

    if (wd/1.2 > hd) {
            $(this).width("100%");
                    himg = $(this).height();
            if (himg > hd) {
                   $(this).css({"margin-top": - Math.abs(himg - hd) / 2});
            } else {
                   $(this).height("100%");  
            }
    } else {
            if (wimg_a > himg_a) {
                $(this).height("100%");
                wimg = $(this).width();
                if (wimg < wd) {
                    $(this).width("100%");
                } else if (wimg > wd) {
                $(this).css({"margin-left": - Math.abs((wimg - wd) / 2)});
               }
            } else if (wimg_a < himg_a) {
                $(this).width("100%");
                himg = $(this).height();
                if (himg < hd) {
                $(this).height("100%");
            } else if (himg > hd) {
                $(this).css({"margin-top": - Math.abs(himg - hd) / 2});
                }
            } else if (wimg_a === himg_a) {
                 $(this).css({"height": "100%", "width": "100%"});
             }
         }
     });    
     });
         
         
//首页轮播
          var swiper = new Swiper('.swiper0', {
            preventClicks : false,
            spaceBetween: 0,
            slidesPerView: 1, 
            slidesPerColumn: 1,  
            loop : true,
            slidesPerView : 'auto',
            pagination: '.swiper-pagination',
            nextButton: '.swiper-next0',
            prevButton: '.swiper-prev0'
        });
		 var swiper1 = new Swiper('.swiper1', {
                pagination: '.swiper-pagination',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 3, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next1',
                prevButton: '.swiper-prev1'
            });
              var swiper2 = new Swiper('.swiper2', {
                pagination: '.swiper-pagination2',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 3, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next2',
                prevButton: '.swiper-prev2'
            });
              var swiper3 = new Swiper('.swiper3', {
                pagination: '.swiper-pagination3',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 3, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next3',
                prevButton: '.swiper-prev3'
            });
//轮播
        var ww = $(document.body).width();
        if (ww <= 980) {
            var swiper1 = new Swiper('.swiper1', {
                pagination: '.swiper-pagination1',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 1, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next1',
                prevButton: '.swiper-prev1'
            });
             var swiper2 = new Swiper('.swiper2', {
                pagination: '.swiper-pagination2',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 1, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next2',
                prevButton: '.swiper-prev2'
            });
             var swiper3 = new Swiper('.swiper3', {
                pagination: '.swiper-pagination3',
                paginationClickable: true,
                spaceBetween: 30,
                slidesPerView: 1, 
                slidesPerColumn: 1,  
                nextButton: '.swiper-next3',
                prevButton: '.swiper-prev3'
            });
        }
       
            
           $('.ajax-post_instru').click(function(){
             var id = $(this).attr('data-id');
             var url = $(this).attr('data-url');
             var src = $(this).find('img').attr("src");
             $.post(url,{id:id},function(data){
                 
                 $('.modal-text').html(data.detail);
                 $('.modal-title').html(data.title);
                 $('.modal-img img').attr("src",src);
             });
             
          });
          
           
            //google地图
            $(".contact").find("button").click(function () {
                var map = $(this).attr("data-map").split(","),
                    x = map[0],
                    y = map[1];
                $("#map").show();
                $('.gmap3').gmap3({
                    map: {options: {center:[x,y], zoom: 18}}, marker: {latLng:[x,y]}
                });
            });
			
//prettyPhoto置后
$("area[rel^='prettyPhoto']").prettyPhoto();
$(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed: 'fast', theme: 'default', slideshow: 3000, autoplay_slideshow: false});  
 
$(".gallery-cell>a>img").load(function(){
     var H=$(this).height()+50;
$(".flickity-viewport").css({"height":H});
});

 $(".form-control>option").click(function(){
     $(this).attr({"selected":"selected"}).siblings().removeAttr("selected");
      $(this).hasArrt({"selected":"selected"}).css({"background":" #000"});

 });

//音樂教程文字提示
// $(".address").click(function(){
//     var title=$(this).parents(".box").find(".booking_title2").text();
//     var address=title+">"+$(this).val();
//     $(this).parents(".box").find(".tips").find("span").eq(1).text(address);
// });
// $(".number").click(function(){
//     var number=">"+$(this).val();
//     $(this).parents(".box").find(".tips").find("span").eq(2).text(number);
// });
// $(".level").click(function(){
//     var level=">"+$(this).val();
//     $(this).parents(".box").find(".tips").find("span").eq(3).text(level);
// });
// $(".time").click(function(){
//     var time=">"+$(this).val();
//     $(this).parents(".box").find(".tips").find("span").eq(4).text(time);
// });