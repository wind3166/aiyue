/*  彈出層 @allenleong */

function layer_open() {

     var CliW = document.documentElement.clientWidth;
     var CliH = document.documentElement.clientHeight;
     var LW =  $('#layer').width()/2;
     
            $('.layer_bg').css({
                    "width": CliW,
                    "height": CliH
            });
            $('.layer_bg').fadeIn();
            $('body').css({
                    "overflow": "hidden"
            });
            $('.layer').css({
                    "left": CliW / 2 - LW
            });
     $('.layer_bg').addClass('open');
            $('.layer').animate({
                    top: "100px"
            });
       return true;     
    }

    function layer_close() {

        $('.layer').animate({
            top: "-100px"
    });
    $('.layer_bg').removeClass('open');
        $('.layer_bg').fadeOut();


    }

    $(window).resize(function() {
     if($('.layer_bg').hasClass('open'))

     {
            layer_open();
      }

    });


    $('.showlayer').click(function() {
 
       layer_open();


});
   