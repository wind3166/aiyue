<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

namespace Addons\EditorForAdmin;
use Common\Controller\Addon;

/**
 * 編輯器插件
 * @author yangweijie <yangweijiester@gmail.com>
 */

	class EditorForAdminAddon extends Addon{

		public $info = array(
			'name'=>'EditorForAdmin',
			'title'=>'後臺編輯器',
			'description'=>'用於增強整站長文本的輸入和顯示',
			'status'=>1,
			'author'=>'thinkphp',
			'version'=>'0.1'
		);

		public function install(){
			return true;
		}

		public function uninstall(){
			return true;
		}

		/**
		 * 編輯器掛載的後臺文檔模型文章內容鉤子
		 * @param array('name'=>'表單name','value'=>'表單對應的值')
		 */
		public function adminArticleEdit($data){
			$this->assign('addons_data', $data);
			$this->assign('addons_config', $this->getConfig());
			$this->display('content');
		}
	}