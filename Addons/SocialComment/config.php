<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

return array(
	'comment_type'=>array(//配置在表單中的鍵名 ,這個會是config[random]
		'title'=>'使用類型:',	 //表單的文字
		'type'=>'select',		 //表單的類型：text、textarea、checkbox、radio、select等
		'options'=>array(		 //select 和radion、checkbox的子選項
			'1'=>'有言',		 //值=>文字
			'2'=>'多說',
		),
		'value'=>'1',			 //表單的默認值
	),
	'group'=>array(
		'type'=>'group',
		'options'=>array(
			'youyan'=>array(
				'title'=>'友言配置',
				'options'=>array(
					'comment_uid_youyan'=>array(
						'title'=>'賬號id:',
						'type'=>'text',
						'value'=>'90040',
						'tip'=>'填寫自己登錄友言後的uid,填寫後可進相應官方後臺'
					),
				)
			),
			'duoshuo'=>array(
				'title'=>'多說配置',
				'options'=>array(
					'comment_short_name_duoshuo'=>array(
						'title'=>'短域名',
						'type'=>'text',
						'value'=>'',
						'tip'=>'每個站點壹個域名'
					),
					'comment_form_pos_duoshuo'=>array(
						'title'=>'表單位置:',
						'type'=>'radio',
						'options'=>array(
							'top'=>'頂部',
							'buttom'=>'底部'
						),
						'value'=>'buttom'
					),
					'comment_data_list_duoshuo'=>array(
						'title'=>'單頁顯示評論數',
						'type'=>'text',
						'value'=>'10'
					),
					'comment_data_order_duoshuo'=>array(
						'title'=>'評論顯示順序',
						'type'=>'radio',
						'options'=>array(
							'asc'=>'從舊到新',
							'desc'=>'從新到舊'
						),
						'value'=>'asc'
					)
				)
			)
		)
	)
);