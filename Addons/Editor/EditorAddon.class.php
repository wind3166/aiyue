<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

namespace Addons\Editor;
use Common\Controller\Addon;

/**
 * 編輯器插件
 * @author yangweijie <yangweijiester@gmail.com>
 */

	class EditorAddon extends Addon{

		public $info = array(
				'name'=>'Editor',
				'title'=>'前臺編輯器',
				'description'=>'用於增強整站長文本的輸入和顯示',
				'status'=>1,
				'author'=>'thinkphp',
				'version'=>'0.1'
			);

		public function install(){
			return true;
		}

		public function uninstall(){
			return true;
		}

		/**
		 * 編輯器掛載的文章內容鉤子
		 * @param array('name'=>'表單name','value'=>'表單對應的值')
		 */
		public function documentEditFormContent($data){
			$this->assign('addons_data', $data);
			$this->assign('addons_config', $this->getConfig());
			$this->display('content');
		}

		/**
		 * 討論提交的鉤子使用編輯器插件擴展
		 * @param array('name'=>'表單name','value'=>'表單對應的值')
		 */
		public function topicComment ($data){
			$this->assign('addons_data', $data);
			$this->assign('addons_config', $this->getConfig());
			$this->display('content');
		}

	}