<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Addons\Attachment\Controller;
use Home\Controller\AddonsController; 

class AttachmentController extends AddonsController{
	
	/* 附件下載 */
	public function download(){
		/* 獲取附件ID */
		$id = I('get.id');
		if(empty($id) || !is_numeric($id)){
			$this->error('附件ID無效！');
		}

		/* 下載附件 */
		$Attachment = D('Addons://Attachment/Attachment');
		if(false === $Attachment->download($id)){
			$this->error($Attachment->getError());
		}

	}

	/* 上傳附件 */
	public function upload(){
		/* 返回標準數據 */
		$return  = array('status' => 1, 'info' => '上傳成功', 'data' => '');

		/* 獲取當前分類附件配置信息 */
		$default  = C('ATTACHMENT_DEFAULT');
		$category = get_category(I('get.category'));

		/* 分類正確性檢測 */
		if(empty($category)){
			$return['status'] = 0;
			$return['info']   = '沒有指定分類或分類不正確；';
		} else {
			$config = $category['extend']['attachment'];
			$config = empty($config) ? $default : array_merge($default, $config);

			/* 檢測並上傳附件 */
			if(in_array('2', str2arr($config['allow_type']))){
				$setting = C('ATTACHMENT_UPLOAD');

				/* 調用文件上傳組件上傳文件 */
				$File = D('File');
				$info = $File->upload($_FILES, $setting, $config['driver'], $config['driver_config']);
				/* 記錄附件信息 */
				if($info){
					$return['data'] = think_encrypt(json_encode($info['attachment']));
				} else {
					$return['status'] = 0;
					$return['info']   = $File->getError();
				}
			} else {
				$return['info']   = '該分類不允許上傳文件附件！';
				$return['status'] = 0;
			}
		}

		/* 返回JSON數據 */
		$this->ajaxReturn($return);
	}

}