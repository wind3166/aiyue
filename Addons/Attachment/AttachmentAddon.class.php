<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Addons\Attachment;
use Common\Controller\Addon;

/**
 * 附件插件
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class AttachmentAddon extends Addon{

	public $info = array(
		'name'        => 'Attachment',
		'title'       => '附件',
		'description' => '用於文檔模型上傳附件',
		'status'      => 1,
		'author'      => 'thinkphp',
		'version'     => '0.1'
	);

	public $admin_list = array(
		'listKey' => array(
			'title'=>'文件名',
			'size'=>'大小',
			'update_time_text'=>'更新時間',
			'document_title'=>'文檔標題'
		),
		'model'=>'Attachment',
		'order'=>'id asc'
	);

	public $custom_adminlist = 'adminlist.html';

	public function install(){
		return true;
	}

	public function uninstall(){
		return true;
	}

	/* 顯示文檔模型編輯頁插件擴展信息表單 */
	public function documentEditForm($param = array()){
		$this->assign($param);
		$this->display(T('Addons://Attachment@Article/edit'));
	}

	/* 文檔末尾顯示附件列表 */
	public function documentDetailAfter($info = array()){
		if(empty($info) || empty($info['id'])){ //數據不正確
			return ;
		}

		/* 獲取當前文檔附件 */
		$Attachment = D('Addons://Attachment/Attachment');
		$map = array('record_id' => $info['id'], 'status' => 1);
		$list = $Attachment->field(true)->where($map)->select();
		if(!$list){ //不存在附件
			return ;
		}

		/* 模板賦值並渲染模板 */
		$this->assign('list', $list);
		$this->display(T('Addons://Attachment@Article/detail'));
	}

	/**
	 * 文檔保存成功後執行行為
	 * @param  array  $data     文檔數據
	 * @param  array  $catecory 分類數據
	 */
	public function documentSaveComplete($param){
		if (MODULE_NAME == 'Home') {
			list($data, $category) = $param;
			/* 附件默認配置項 */
			$default  = C('ATTACHMENT_DEFAULT');

			/* 合並當前配置 */
			$config = $category['extend']['attachment'];
			$config = empty($config) ? $default : array_merge($default, $config);
			$attach = I('post.attachment');

			/* 該分類不允許上傳附件 */
			if(!$config['is_upload'] || !in_array($attach['type'], str2arr($config['allow_type']))){
				return ;
			}

			switch ($attach['type']) {
				case 1: //外鏈
					# code...
					break;
				case 2: //文件
					$info = json_decode(think_decrypt($attach['info']), true);
					if(!empty($info)){
						$Attachment = D('Addons://Attachment/Attachment');
						$Attachment->saveFile($info['name'], $info, $data['id']);
					} else {
						return; //TODO:非法附件上傳，可記錄日誌
					}
					break;
			}
		}
	}
}