<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: yangweijie <yangweijiester@gmail.com> <code-tech.diandian.com>
// +----------------------------------------------------------------------

return array(
	'title'=>array(//配置在表單中的鍵名 ,這個會是config[title]
		'title'=>'顯示標題:',//表單的文字
		'type'=>'text',		 //表單的類型：text、textarea、checkbox、radio、select等
		'value'=>'系統信息',			 //表單的默認值
	),
	'width'=>array(
		'title'=>'顯示寬度:',
		'type'=>'select',
		'options'=>array(
			'1'=>'1格',
			'2'=>'2格',
			'4'=>'4格'
		),
		'value'=>'2'
	),
	'display'=>array(
		'title'=>'是否顯示:',
		'type'=>'radio',
		'options'=>array(
			'1'=>'顯示',
			'0'=>'不顯示'
		),
		'value'=>'1'
	)
);