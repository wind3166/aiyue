-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- 主机: localhost:3306
-- 生成日期: 2016-03-24 11:55:09
-- 服务器版本: 5.5.48-cll
-- PHP 版本: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `lovemusi_aiyue`
--

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_action`
--

CREATE TABLE IF NOT EXISTS `aiyue_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '行為唯壹標識',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '行為說明',
  `remark` char(140) NOT NULL DEFAULT '' COMMENT '行為描述',
  `rule` text NOT NULL COMMENT '行為規則',
  `log` text NOT NULL COMMENT '日誌規則',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系統行為表' AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `aiyue_action`
--

INSERT INTO `aiyue_action` (`id`, `name`, `title`, `remark`, `rule`, `log`, `type`, `status`, `update_time`) VALUES
(1, 'user_login', '用戶登錄', '積分+10，每天壹次', 'table:member|field:score|condition:uid={$self} AND status>-1|rule:score+10|cycle:24|max:1;', '[user|get_nickname]在[time|time_format]登錄了後臺', 1, 1, 1387181220),
(2, 'add_article', '發布文章', '積分+5，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:5', '', 2, 0, 1380173180),
(3, 'review', '評論', '評論積分+1，無限制', 'table:member|field:score|condition:uid={$self}|rule:score+1', '', 2, 1, 1383285646),
(4, 'add_document', '發表文檔', '積分+10，每天上限5次', 'table:member|field:score|condition:uid={$self}|rule:score+10|cycle:24|max:5', '[user|get_nickname]在[time|time_format]發表了壹篇文章。\r\n表[model]，記錄編號[record]。', 2, 0, 1386139726),
(5, 'add_document_topic', '發表討論', '積分+5，每天上限10次', 'table:member|field:score|condition:uid={$self}|rule:score+5|cycle:24|max:10', '', 2, 0, 1383285551),
(6, 'update_config', '更新配置', '新增或修改或刪除配置', '', '', 1, 1, 1383294988),
(7, 'update_model', '更新模型', '新增或修改模型', '', '', 1, 1, 1383295057),
(8, 'update_attribute', '更新屬性', '新增或更新或刪除屬性', '', '', 1, 1, 1383295963),
(9, 'update_channel', '更新導航', '新增或修改或刪除導航', '', '', 1, 1, 1383296301),
(10, 'update_menu', '更新菜單', '新增或修改或刪除菜單', '', '', 1, 1, 1383296392),
(11, 'update_category', '更新分類', '新增或修改或刪除分類', '', '', 1, 1, 1383296765);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_action_log`
--

CREATE TABLE IF NOT EXISTS `aiyue_action_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `action_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '行為id',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行用戶id',
  `action_ip` bigint(20) NOT NULL COMMENT '執行行為者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '觸發行為的表',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '觸發行為的數據id',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '日誌備註',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '執行行為的時間',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行為日誌表' AUTO_INCREMENT=849 ;

--
-- 转存表中的数据 `aiyue_action_log`
--

INSERT INTO `aiyue_action_log` (`id`, `action_id`, `user_id`, `action_ip`, `model`, `record_id`, `remark`, `status`, `create_time`) VALUES
(603, 7, 1, 2130706433, 'model', 24, '操作url：/aiyue/Admin/Model/update.html', 1, 1437705249),
(604, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-24 15:17登錄了後臺', 1, 1437722231),
(605, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-24 17:55登錄了後臺', 1, 1437731757),
(606, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-24 18:31登錄了後臺', 1, 1437733905),
(607, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-25 14:14登錄了後臺', 1, 1437804895),
(608, 7, 1, 2130706433, 'model', 25, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806466),
(609, 8, 1, 2130706433, 'attribute', 125, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437806508),
(610, 7, 1, 2130706433, 'model', 25, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806533),
(611, 7, 1, 2130706433, 'model', 25, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806564),
(612, 8, 1, 2130706433, 'attribute', 126, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437806627),
(613, 7, 1, 2130706433, 'model', 24, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806636),
(614, 7, 1, 2130706433, 'model', 23, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806642),
(615, 7, 1, 2130706433, 'model', 22, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806652),
(616, 7, 1, 2130706433, 'model', 20, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806665),
(617, 7, 1, 2130706433, 'model', 17, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806671),
(618, 7, 1, 2130706433, 'model', 15, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806679),
(619, 7, 1, 2130706433, 'model', 19, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806692),
(620, 7, 1, 2130706433, 'model', 14, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806727),
(621, 7, 1, 2130706433, 'model', 8, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806733),
(622, 7, 1, 2130706433, 'model', 5, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806748),
(623, 7, 1, 2130706433, 'model', 6, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806753),
(624, 7, 1, 2130706433, 'model', 7, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806760),
(625, 7, 1, 2130706433, 'model', 4, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806769),
(626, 7, 1, 2130706433, 'model', 3, '操作url：/aiyue/Admin/Model/update.html', 1, 1437806777),
(627, 11, 1, 2130706433, 'category', 75, '操作url：/aiyue/Admin/Category/add.html', 1, 1437806902),
(628, 11, 1, 2130706433, 'category', 76, '操作url：/aiyue/Admin/Category/add.html', 1, 1437806920),
(629, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 09:53登錄了後臺', 1, 1437962030),
(630, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 09:58登錄了後臺', 1, 1437962314),
(631, 8, 1, 2130706433, 'attribute', 127, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437962365),
(632, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 11:30登錄了後臺', 1, 1437967855),
(633, 7, 1, 2130706433, 'model', 26, '操作url：/aiyue/Admin/Model/update.html', 1, 1437975684),
(634, 8, 1, 2130706433, 'attribute', 128, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976203),
(635, 8, 1, 2130706433, 'attribute', 129, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976249),
(636, 8, 1, 2130706433, 'attribute', 130, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976273),
(637, 8, 1, 2130706433, 'attribute', 131, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976317),
(638, 8, 1, 2130706433, 'attribute', 132, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976335),
(639, 8, 1, 2130706433, 'attribute', 133, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976345),
(640, 8, 1, 2130706433, 'attribute', 134, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437976355),
(641, 8, 1, 2130706433, 'attribute', 135, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437978630),
(642, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 14:37登錄了後臺', 1, 1437979042),
(643, 8, 1, 2130706433, 'attribute', 136, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437979267),
(644, 7, 1, 2130706433, 'model', 2, '操作url：/aiyue/Admin/Model/update.html', 1, 1437981264),
(645, 8, 1, 2130706433, 'attribute', 135, '操作url：/aiyue/Admin/Attribute/remove/id/135.html', 1, 1437982236),
(646, 8, 1, 2130706433, 'attribute', 137, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1437985958),
(647, 7, 1, 2130706433, 'model', 9, '操作url：/aiyue/Admin/Model/update.html', 1, 1437991673),
(648, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 20:02登錄了後臺', 1, 1437998552),
(649, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-27 20:15登錄了後臺', 1, 1437999328),
(650, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-28 17:43登錄了後臺', 1, 1438076606),
(651, 1, 1, 0, 'member', 1, 'admin在2015-07-29 14:38登錄了後臺', 1, 1438151937),
(652, 8, 1, 0, 'attribute', 138, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438152036),
(653, 7, 1, 2130706433, 'model', 27, '操作url：/aiyue/Admin/Model/update.html', 1, 1438159122),
(654, 8, 1, 2130706433, 'attribute', 139, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438159323),
(655, 7, 1, 2130706433, 'model', 28, '操作url：/aiyue/Admin/Model/update.html', 1, 1438159391),
(656, 8, 1, 2130706433, 'attribute', 140, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438159414),
(657, 8, 1, 2130706433, 'attribute', 140, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438159436),
(658, 8, 1, 2130706433, 'attribute', 141, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438159506),
(659, 8, 1, 2130706433, 'attribute', 142, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438159554),
(660, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-31 09:20登錄了後臺', 1, 1438305631),
(661, 1, 1, 2130706433, 'member', 1, 'admin在2015-07-31 13:35登錄了後臺', 1, 1438320933),
(662, 11, 1, 2130706433, 'category', 45, '操作url：/aiyue/Admin/Category/edit.html', 1, 1438327865),
(663, 9, 1, 2130706433, 'channel', 7, '操作url：/aiyue/Admin/Channel/edit.html', 1, 1438327948),
(664, 11, 1, 2130706433, 'category', 77, '操作url：/aiyue/Admin/Category/add.html', 1, 1438328726),
(665, 11, 1, 2130706433, 'category', 66, '操作url：/aiyue/Admin/Category/edit.html', 1, 1438333907),
(666, 11, 1, 2130706433, 'category', 66, '操作url：/aiyue/Admin/Category/edit.html', 1, 1438333934),
(667, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-01 09:34登錄了後臺', 1, 1438392881),
(668, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-01 13:59登錄了後臺', 1, 1438408794),
(669, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-05 18:31登錄了後臺', 1, 1438770691),
(670, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-06 12:00登錄了後臺', 1, 1438833652),
(671, 8, 1, 2130706433, 'attribute', 44, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438833828),
(672, 8, 1, 2130706433, 'attribute', 58, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1438833879),
(673, 7, 1, 2130706433, 'model', 13, '操作url：/aiyue/Admin/Model/update.html', 1, 1438840434),
(674, 7, 1, 2130706433, 'model', 13, '操作url：/aiyue/Admin/Model/update.html', 1, 1438840480),
(675, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-06 14:05登錄了後臺', 1, 1438841107),
(676, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-06 14:31登錄了後臺', 1, 1438842716),
(677, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-06 16:06登錄了後臺', 1, 1438848368),
(678, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-07 14:34登錄了後臺', 1, 1438929288),
(679, 1, 1, -1062731450, 'member', 1, 'admin在2015-08-07 14:38登錄了後臺', 1, 1438929492),
(680, 1, 1, -1062731452, 'member', 1, 'admin在2015-08-07 14:38登錄了後臺', 1, 1438929522),
(681, 11, 1, 2130706433, 'category', 63, '操作url：/aiyue/Admin/Category/edit.html', 1, 1438930521),
(682, 11, 1, 2130706433, 'category', 70, '操作url：/aiyue/Admin/Category/edit.html', 1, 1438930527),
(683, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-07 15:17登錄了後臺', 1, 1438931846),
(684, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-10 14:16登錄了後臺', 1, 1439187409),
(685, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-13 13:54登錄了後臺', 1, 1439445288),
(686, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-13 14:39登錄了後臺', 1, 1439447955),
(687, 8, 1, 2130706433, 'attribute', 118, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439448035),
(688, 8, 1, 2130706433, 'attribute', 23, '操作url：/aiyue/Admin/Attribute/remove/id/23.html', 1, 1439448930),
(689, 8, 1, 2130706433, 'attribute', 26, '操作url：/aiyue/Admin/Attribute/remove/id/26.html', 1, 1439448945),
(690, 8, 1, 2130706433, 'attribute', 25, '操作url：/aiyue/Admin/Attribute/remove/id/25.html', 1, 1439448949),
(691, 7, 1, 2130706433, 'model', 2, '操作url：/aiyue/Admin/Model/update.html', 1, 1439448976),
(692, 8, 1, 2130706433, 'attribute', 119, '操作url：/aiyue/Admin/Attribute/remove/id/119.html', 1, 1439449278),
(693, 7, 1, 2130706433, 'model', 23, '操作url：/aiyue/Admin/Model/update.html', 1, 1439449305),
(694, 8, 1, 2130706433, 'attribute', 24, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439449346),
(695, 8, 1, 2130706433, 'attribute', 143, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439449376),
(696, 8, 1, 2130706433, 'attribute', 24, '操作url：/aiyue/Admin/Attribute/remove/id/24.html', 1, 1439449380),
(697, 7, 1, 2130706433, 'model', 2, '操作url：/aiyue/Admin/Model/update.html', 1, 1439449393),
(698, 7, 1, 2130706433, 'model', 23, '操作url：/aiyue/Admin/Model/update.html', 1, 1439449744),
(699, 8, 1, 2130706433, 'attribute', 118, '操作url：/aiyue/Admin/Attribute/remove/id/118.html', 1, 1439449825),
(700, 7, 1, 2130706433, 'model', 23, '操作url：/aiyue/Admin/Model/update.html', 1, 1439449838),
(701, 7, 1, 2130706433, 'model', 12, '操作url：/aiyue/Admin/Model/update.html', 1, 1439455106),
(702, 8, 1, 2130706433, 'attribute', 45, '操作url：/aiyue/Admin/Attribute/remove/id/45.html', 1, 1439455213),
(703, 7, 1, 2130706433, 'model', 9, '操作url：/aiyue/Admin/Model/update.html', 1, 1439455222),
(704, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-13 20:32登錄了後臺', 1, 1439469171),
(705, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-13 20:32登錄了後臺', 1, 1439469171),
(706, 7, 1, 2130706433, 'model', 29, '操作url：/aiyue/Admin/Model/update.html', 1, 1439469341),
(707, 8, 1, 2130706433, 'attribute', 144, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469372),
(708, 7, 1, 2130706433, 'model', 29, '操作url：/aiyue/Admin/Model/update.html', 1, 1439469393),
(709, 7, 1, 2130706433, 'model', 29, '操作url：/aiyue/Admin/Model/update.html', 1, 1439469407),
(710, 11, 1, 2130706433, 'category', 77, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439469416),
(711, 11, 1, 2130706433, 'category', 77, '操作url：/aiyue/Admin/Category/remove/id/77.html', 1, 1439469554),
(712, 8, 1, 2130706433, 'attribute', 75, '操作url：/aiyue/Admin/Attribute/remove/id/75.html', 1, 1439469571),
(713, 8, 1, 2130706433, 'attribute', 34, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469590),
(714, 8, 1, 2130706433, 'attribute', 145, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469797),
(715, 7, 1, 2130706433, 'model', 29, '操作url：/aiyue/Admin/Model/update.html', 1, 1439469807),
(716, 8, 1, 2130706433, 'attribute', 146, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469837),
(717, 8, 1, 2130706433, 'attribute', 146, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469869),
(718, 8, 1, 2130706433, 'attribute', 146, '操作url：/aiyue/Admin/Attribute/remove/id/146.html', 1, 1439469892),
(719, 8, 1, 2130706433, 'attribute', 147, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439469915),
(720, 8, 1, 2130706433, 'attribute', 147, '操作url：/aiyue/Admin/Attribute/remove/id/147.html', 1, 1439469933),
(721, 7, 1, 2130706433, 'model', 4, '操作url：/aiyue/Admin/Model/update.html', 1, 1439469944),
(722, 7, 1, 2130706433, 'model', 16, '操作url：/aiyue/Admin/Model/update.html', 1, 1439470239),
(723, 6, 1, 2130706433, 'config', 20, '操作url：/aiyue/Admin/Config/edit.html', 1, 1439470463),
(724, 8, 1, 2130706433, 'attribute', 34, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439471237),
(725, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-15 10:06登錄了後臺', 1, 1439604395),
(726, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-15 17:59登錄了後臺', 1, 1439632793),
(727, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-18 09:46登錄了後臺', 1, 1439862376),
(728, 7, 1, 2130706433, 'model', 18, '操作url：/aiyue/Admin/Model/update.html', 1, 1439863344),
(729, 9, 1, 2130706433, 'channel', 9, '操作url：/aiyue/Admin/Channel/edit.html', 1, 1439864011),
(730, 9, 1, 2130706433, 'channel', 8, '操作url：/aiyue/Admin/Channel/edit.html', 1, 1439864015),
(731, 9, 1, 2130706433, 'channel', 10, '操作url：/aiyue/Admin/Channel/edit.html', 1, 1439864019),
(732, 11, 1, 2130706433, 'category', 72, '操作url：/aiyue/Admin/Category/remove/id/72.html', 1, 1439864227),
(733, 11, 1, 2130706433, 'category', 78, '操作url：/aiyue/Admin/Category/add.html', 1, 1439864289),
(734, 11, 1, 2130706433, 'category', 78, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439864300),
(735, 11, 1, 2130706433, 'category', 78, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439864321),
(736, 11, 1, 2130706433, 'category', 78, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439864334),
(737, 8, 1, 2130706433, 'attribute', 106, '操作url：/aiyue/Admin/Attribute/remove/id/106.html', 1, 1439864377),
(738, 8, 1, 2130706433, 'attribute', 105, '操作url：/aiyue/Admin/Attribute/remove/id/105.html', 1, 1439864387),
(739, 8, 1, 2130706433, 'attribute', 104, '操作url：/aiyue/Admin/Attribute/remove/id/104.html', 1, 1439864389),
(740, 8, 1, 2130706433, 'attribute', 103, '操作url：/aiyue/Admin/Attribute/remove/id/103.html', 1, 1439864392),
(741, 8, 1, 2130706433, 'attribute', 102, '操作url：/aiyue/Admin/Attribute/remove/id/102.html', 1, 1439864395),
(742, 8, 1, 2130706433, 'attribute', 101, '操作url：/aiyue/Admin/Attribute/remove/id/101.html', 1, 1439864398),
(743, 8, 1, 2130706433, 'attribute', 100, '操作url：/aiyue/Admin/Attribute/remove/id/100.html', 1, 1439864400),
(744, 8, 1, 2130706433, 'attribute', 94, '操作url：/aiyue/Admin/Attribute/remove/id/94.html', 1, 1439864409),
(745, 8, 1, 2130706433, 'attribute', 93, '操作url：/aiyue/Admin/Attribute/remove/id/93.html', 1, 1439864412),
(746, 8, 1, 2130706433, 'attribute', 92, '操作url：/aiyue/Admin/Attribute/remove/id/92.html', 1, 1439864414),
(747, 8, 1, 2130706433, 'attribute', 91, '操作url：/aiyue/Admin/Attribute/remove/id/91.html', 1, 1439864417),
(748, 8, 1, 2130706433, 'attribute', 90, '操作url：/aiyue/Admin/Attribute/remove/id/90.html', 1, 1439864420),
(749, 8, 1, 2130706433, 'attribute', 89, '操作url：/aiyue/Admin/Attribute/remove/id/89.html', 1, 1439864423),
(750, 8, 1, 2130706433, 'attribute', 148, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439864489),
(751, 7, 1, 2130706433, 'model', 18, '操作url：/aiyue/Admin/Model/update.html', 1, 1439864498),
(752, 7, 1, 2130706433, 'model', 30, '操作url：/aiyue/Admin/Model/update.html', 1, 1439865399),
(753, 8, 1, 2130706433, 'attribute', 149, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865449),
(754, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-18 10:37登錄了後臺', 1, 1439865460),
(755, 8, 1, 2130706433, 'attribute', 150, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865502),
(756, 8, 1, 2130706433, 'attribute', 151, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865527),
(757, 8, 1, 2130706433, 'attribute', 152, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865561),
(758, 8, 1, 2130706433, 'attribute', 153, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865575),
(759, 8, 1, 2130706433, 'attribute', 154, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1439865604),
(760, 7, 1, 2130706433, 'model', 30, '操作url：/aiyue/Admin/Model/update.html', 1, 1439865623),
(761, 7, 1, 2130706433, 'model', 30, '操作url：/aiyue/Admin/Model/update.html', 1, 1439865651),
(762, 11, 1, 2130706433, 'category', 65, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439865665),
(763, 7, 1, 2130706433, 'model', 30, '操作url：/aiyue/Admin/Model/update.html', 1, 1439865766),
(764, 11, 1, 2130706433, 'category', 66, '操作url：/aiyue/Admin/Category/edit.html', 1, 1439865865),
(765, 11, 1, 2130706433, 'category', 79, '操作url：/aiyue/Admin/Category/add.html', 1, 1439866797),
(766, 11, 1, 2130706433, 'category', 80, '操作url：/aiyue/Admin/Category/add.html', 1, 1439866830),
(767, 7, 1, 2130706433, 'model', 20, '操作url：/aiyue/Admin/Model/update.html', 1, 1439878333),
(768, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-25 09:52登錄了後臺', 1, 1440467573),
(769, 7, 1, 2130706433, 'model', 14, '操作url：/aiyue/Admin/Model/update.html', 1, 1440470760),
(770, 8, 1, 2130706433, 'attribute', 97, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1440471346),
(771, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-25 11:23登錄了後臺', 1, 1440473015),
(772, 7, 1, 2130706433, 'model', 14, '操作url：/aiyue/Admin/Model/update.html', 1, 1440558512),
(773, 1, 1, 2130706433, 'member', 1, 'admin在2015-08-28 14:19登錄了後臺', 1, 1440742751),
(774, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-07 10:58登錄了後臺', 1, 1441594727),
(775, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-07 11:06登錄了後臺', 1, 1441595182),
(776, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-09 15:10登錄了後臺', 1, 1441782646),
(777, 8, 1, 2130706433, 'attribute', 38, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1441783394),
(778, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-09 15:38登錄了後臺', 1, 1441784284),
(779, 1, 1, -1062731332, 'member', 1, 'admin在2015-09-09 15:52登錄了後臺', 1, 1441785137),
(780, 1, 1, -1062731329, 'member', 1, 'admin在2015-09-09 15:54登錄了後臺', 1, 1441785284),
(781, 7, 1, 2130706433, 'model', 14, '操作url：/aiyue/Admin/Model/update.html', 1, 1441789886),
(782, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-09 17:32登錄了後臺', 1, 1441791126),
(783, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-09 17:53登錄了後臺', 1, 1441792380),
(784, 1, 1, 2130706433, 'member', 1, 'admin在2015-09-11 10:35登錄了後臺', 1, 1441938950),
(785, 8, 1, 2130706433, 'attribute', 79, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1441938985),
(786, 9, 1, 2130706433, 'channel', 10, '操作url：/aiyue/Admin/Channel/edit.html', 1, 1441941665),
(787, 8, 1, 2130706433, 'attribute', 155, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1441942175),
(788, 8, 1, 2130706433, 'attribute', 156, '操作url：/aiyue/Admin/Attribute/update.html', 1, 1441942197),
(789, 1, 1, 2102994539, 'member', 1, 'admin在2015-09-11 11:52登錄了後臺', 1, 1441943570),
(790, 1, 1, 1900842020, 'member', 1, 'admin在2015-09-11 12:15登錄了後臺', 1, 1441944911),
(791, 1, 1, 2102994539, 'member', 1, 'admin在2015-09-11 13:33登錄了後臺', 1, 1441949626),
(792, 1, 1, 2102994539, 'member', 1, 'admin在2015-09-11 13:36登錄了後臺', 1, 1441949805),
(793, 1, 1, 2102994539, 'member', 1, 'admin在2015-09-11 14:06登錄了後臺', 1, 1441951576),
(794, 1, 1, 2005143087, 'member', 1, 'admin在2015-09-11 14:28登錄了後臺', 1, 1441952910),
(795, 1, 1, 2005143087, 'member', 1, 'admin在2015-09-11 16:13登錄了後臺', 1, 1441959205),
(796, 1, 1, 2102994539, 'member', 1, 'admin在2015-09-11 17:28登錄了後臺', 1, 1441963735),
(797, 1, 1, 2005143087, 'member', 1, 'admin在2015-09-11 18:53登錄了後臺', 1, 1441968813),
(798, 1, 1, 1900867435, 'member', 1, 'admin在2015-09-14 10:03登錄了後臺', 1, 1442196180),
(799, 1, 1, 1900867435, 'member', 1, 'admin在2015-09-14 10:03登錄了後臺', 1, 1442196189),
(800, 1, 1, 3072326376, 'member', 1, 'admin在2015-09-14 10:21登錄了後臺', 1, 1442197311),
(801, 1, 1, 3072326376, 'member', 1, 'admin在2015-09-14 10:59登錄了後臺', 1, 1442199587),
(802, 1, 1, 1022802356, 'member', 1, 'admin在2015-09-14 11:07登錄了後臺', 1, 1442200072),
(803, 1, 1, 1022802356, 'member', 1, 'admin在2015-09-21 16:07登錄了後臺', 1, 1442822876),
(804, 1, 1, 3073441310, 'member', 1, 'admin在2015-09-23 12:05登錄了後臺', 1, 1442981120),
(805, 1, 1, 3073441310, 'member', 1, 'admin在2015-09-23 18:12登錄了後臺', 1, 1443003123),
(806, 1, 1, 3073441310, 'member', 1, 'admin在2015-09-23 18:27登錄了後臺', 1, 1443004038),
(807, 6, 1, 3073441310, 'config', 1, '操作url：/Admin/Config/edit.html', 1, 1443004068),
(808, 6, 1, 3073441310, 'config', 1, '操作url：/Admin/Config/edit.html', 1, 1443004077),
(809, 6, 1, 3073441310, 'config', 38, '操作url：/Admin/Config/edit.html', 1, 1443004108),
(810, 6, 1, 3073441310, 'config', 39, '操作url：/Admin/Config/edit.html', 1, 1443004194),
(811, 6, 1, 3073441310, 'config', 40, '操作url：/Admin/Config/edit.html', 1, 1443004215),
(812, 6, 1, 3073441310, 'config', 42, '操作url：/Admin/Config/edit.html', 1, 1443004242),
(813, 6, 1, 3073441310, 'config', 43, '操作url：/Admin/Config/edit.html', 1, 1443004269),
(814, 6, 1, 3073441310, 'config', 44, '操作url：/Admin/Config/edit.html', 1, 1443004336),
(815, 6, 1, 3073441310, 'config', 45, '操作url：/Admin/Config/edit.html', 1, 1443004351),
(816, 6, 1, 3073441310, 'config', 46, '操作url：/Admin/Config/edit.html', 1, 1443004365),
(817, 6, 1, 3073441310, 'config', 47, '操作url：/Admin/Config/edit.html', 1, 1443004379),
(818, 6, 1, 3073441310, 'config', 48, '操作url：/Admin/Config/edit.html', 1, 1443004397),
(819, 6, 1, 3073441310, 'config', 49, '操作url：/Admin/Config/edit.html', 1, 1443004409),
(820, 1, 1, 3073441715, 'member', 1, 'admin在2015-10-15 11:05登錄了後臺', 1, 1444878329),
(821, 1, 1, 2053413504, 'member', 1, 'admin在2015-10-15 13:05登錄了後臺', 1, 1444885551),
(822, 1, 1, 1022802522, 'member', 1, 'admin在2016-02-29 00:27登錄了後臺', 1, 1456676865),
(823, 1, 1, 460162282, 'member', 1, 'admin在2016-02-29 14:12登錄了後臺', 1, 1456726324),
(824, 1, 1, 460162282, 'member', 1, 'admin在2016-02-29 14:30登錄了後臺', 1, 1456727425),
(825, 1, 1, 2053422703, 'member', 1, 'admin在2016-02-29 16:03登錄了後臺', 1, 1456733006),
(826, 1, 1, 242993959, 'member', 1, 'admin在2016-02-29 16:06登錄了後臺', 1, 1456733175),
(827, 1, 1, 242993959, 'member', 1, 'admin在2016-02-29 16:22登錄了後臺', 1, 1456734164),
(828, 1, 1, 242993959, 'member', 1, 'admin在2016-02-29 17:49登錄了後臺', 1, 1456739394),
(829, 1, 1, 2130706433, 'member', 1, 'admin在2016-03-01 11:18登錄了後臺', 1, 1456802296),
(830, 7, 1, 2130706433, 'model', 23, '操作url：/project/aiyue/Admin/Model/update.html', 1, 1456803161),
(831, 8, 1, 2130706433, 'attribute', 157, '操作url：/project/aiyue/Admin/Attribute/update.html', 1, 1456803242),
(832, 7, 1, 2130706433, 'model', 23, '操作url：/project/aiyue/Admin/Model/update.html', 1, 1456803260),
(833, 1, 1, 2130706433, 'member', 1, 'admin在2016-03-07 15:12登錄了後臺', 1, 1457334758),
(834, 7, 1, 2130706433, 'model', 17, '操作url：/project/aiyue/Admin/Model/update.html', 1, 1457334863),
(835, 7, 1, 2130706433, 'model', 17, '操作url：/project/aiyue/Admin/Model/update.html', 1, 1457341516),
(836, 1, 1, 2053408283, 'member', 1, 'admin在2016-03-10 15:18登錄了後臺', 1, 1457594300),
(837, 1, 1, 1022765284, 'member', 1, 'admin在2016-03-10 15:38登錄了後臺', 1, 1457595482),
(838, 1, 1, 1022794884, 'member', 1, 'admin在2016-03-10 20:33登錄了後臺', 1, 1457613235),
(839, 1, 1, 1899259767, 'member', 1, 'admin在2016-03-13 18:32登錄了後臺', 1, 1457865136),
(840, 1, 1, 2053421924, 'member', 1, 'admin在2016-03-17 15:39登錄了後臺', 1, 1458200385),
(841, 1, 1, 1022765284, 'member', 1, 'admin在2016-03-18 17:29登錄了後臺', 1, 1458293361),
(842, 1, 1, 2005167843, 'member', 1, 'admin在2016-03-18 17:53登錄了後臺', 1, 1458294796),
(843, 1, 1, 1022765284, 'member', 1, 'admin在2016-03-18 20:34登錄了後臺', 1, 1458304468),
(844, 1, 1, 460167375, 'member', 1, 'admin在2016-03-18 20:34登錄了後臺', 1, 1458304481),
(845, 1, 1, 1022765284, 'member', 1, 'admin在2016-03-21 13:50登錄了後臺', 1, 1458539450),
(846, 1, 1, 242992661, 'member', 1, 'admin在2016-03-24 11:37登錄了後臺', 1, 1458790659),
(847, 11, 1, 242992661, 'category', 42, '操作url：/Admin/Category/edit.html', 1, 1458790841),
(848, 11, 1, 242992661, 'category', 42, '操作url：/Admin/Category/edit.html', 1, 1458790950);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_addons`
--

CREATE TABLE IF NOT EXISTS `aiyue_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL COMMENT '插件名或標識',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '狀態',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本號',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安裝時間',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有後臺列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='插件表' AUTO_INCREMENT=20 ;

--
-- 转存表中的数据 `aiyue_addons`
--

INSERT INTO `aiyue_addons` (`id`, `name`, `title`, `description`, `status`, `config`, `author`, `version`, `create_time`, `has_adminlist`) VALUES
(15, 'EditorForAdmin', '後臺編輯器', '用於增強整站長文本的輸入和顯示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"500px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1383126253, 0),
(2, 'SiteStat', '站點統計信息', '統計站點的基礎信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"1","display":"1","status":"0"}', 'thinkphp', '0.1', 1379512015, 0),
(3, 'DevTeam', '開發團隊信息', '開發團隊成員信息', 1, '{"title":"OneThink\\u5f00\\u53d1\\u56e2\\u961f","width":"2","display":"1"}', 'thinkphp', '0.1', 1379512022, 0),
(4, 'SystemInfo', '系統環境信息', '用於顯示壹些服務器的信息', 1, '{"title":"\\u7cfb\\u7edf\\u4fe1\\u606f","width":"2","display":"1"}', 'thinkphp', '0.1', 1379512036, 0),
(5, 'Editor', '前臺編輯器', '用於增強整站長文本的輸入和顯示', 1, '{"editor_type":"2","editor_wysiwyg":"1","editor_height":"300px","editor_resize_type":"1"}', 'thinkphp', '0.1', 1379830910, 0),
(6, 'Attachment', '附件', '用於文檔模型上傳附件', 1, 'null', 'thinkphp', '0.1', 1379842319, 1),
(9, 'SocialComment', '通用社交化評論', '集成了各種社交化評論插件，輕松集成到系統中。', 1, '{"comment_type":"1","comment_uid_youyan":"","comment_short_name_duoshuo":"","comment_data_list_duoshuo":""}', 'thinkphp', '0.1', 1380273962, 0),
(19, 'UploadImages', '多图上传', '多图上传', 1, 'null', '职业养猪户', '1.0', 1441782686, 0);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_appointment`
--

CREATE TABLE IF NOT EXISTS `aiyue_appointment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `mobile` varchar(255) NOT NULL COMMENT '手機',
  `address` varchar(255) NOT NULL COMMENT '門店',
  `course` varchar(255) NOT NULL COMMENT '課程',
  `number` varchar(255) NOT NULL COMMENT '人數',
  `level` varchar(255) NOT NULL COMMENT '級別',
  `class` varchar(255) NOT NULL COMMENT '年級',
  `price` varchar(255) NOT NULL COMMENT '價格',
  `time` varchar(255) NOT NULL COMMENT '時間',
  `courseid` varchar(255) NOT NULL COMMENT '課程id',
  `email` varchar(255) NOT NULL COMMENT '電子郵箱',
  `age` int(10) unsigned NOT NULL COMMENT '學員年齡',
  `readstatus` varchar(255) NOT NULL DEFAULT '未讀',
  `memberID` int(10) unsigned NOT NULL COMMENT '会员ID',
  PRIMARY KEY (`id`,`readstatus`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=128 ;

--
-- 转存表中的数据 `aiyue_appointment`
--

INSERT INTO `aiyue_appointment` (`id`, `name`, `mobile`, `address`, `course`, `number`, `level`, `class`, `price`, `time`, `courseid`, `email`, `age`, `readstatus`, `memberID`) VALUES
(103, '李先生', '123456', '嘉應店', '大提琴、小提琴、長笛個別授課', '1人班', '1-3級', '1堂', '300', '45分鐘/堂', '', '771412508@qq.com', 10, '未讀', 13),
(104, '12311', '131546465', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '13254656564', 123, '未讀', 0),
(105, '李先生', '123456', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '771412508@qq.com', 10, '未讀', 13),
(106, '李先生', '123456', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '771412508@qq.com', 10, '未讀', 13),
(107, '李先生', '123456', '嘉應店', '大提琴、小提琴、長笛個別授課', '1人班', '1-3級', '1堂', '300', '45分鐘/堂', '', '771412508@qq.com', 10, '未讀', 13),
(108, '李先生', '123456', '嘉應店', '大提琴、小提琴、長笛個別授課', '1人班', '1-3級', '1堂', '300', '45分鐘/堂', '', '771412508@qq.com', 10, '未讀', 13),
(109, 'sada', '12346', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '123456@qq.com', 123, '未讀', 0),
(110, 'sadas', '13564123426', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '45644@qq.com', 13, '未讀', 0),
(111, 'sadas', '13564123426', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '45644@qq.com', 13, '未讀', 0),
(112, 'sadas', '13564123426', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', '45644@qq.com', 13, '已讀', 0),
(113, 'a', 'a', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', 'a', 0, '已讀', 0),
(114, 'akak', 'a', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '500', '45分鐘/堂課', '', 'a', 0, '已讀', 0),
(120, '黃黃', '15919191919', '激成店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '2堂', '700', '45分鐘/堂課', '', '794025348@qq.com', 20, '未讀', 0),
(121, 'bing', '15919191919', '激成店', '團隊樂理及視唱練耳教學', '多人班', '1-3級', '1堂', '500', '60分鐘/堂', '', '794025348@qq.com', 20, '未讀', 0),
(122, 'bing', '15919191919', '激成店', '團隊樂理及視唱練耳教學', '多人班', '1-3級', '1堂', '500', '60分鐘/堂', '', '794025438@qq.com', 10, '未讀', 0),
(123, 'bing', '15919191919', '激成店', '團隊樂理及視唱練耳教學', '多人班', '1-3級', '1堂', '500', '60分鐘/堂', '', '794025348@qq.com', 10, '未讀', 0),
(124, 'bing', '15919191919', '龍崇店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '600', '45分鐘/堂課', '', '794025348@qq.com', 10, '未讀', 21),
(125, 'bing', '15919191919', '激成店', '團隊樂理及視唱練耳教學', '多人班', '1-3級', '1堂', '500', '60分鐘/堂', '', '794025348@qq.com', 10, '已讀', 21),
(126, '冰', '159191901701', '龍崇店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '600', '45分鐘/堂課', '', '794025348@qq.com', 10, '已讀', 25),
(127, '11', '22', '龍崇店', '個別樂理及視唱練耳教學', '1人班', '1-3級', '1堂', '600', '45分鐘/堂課', '', '3', 44, '已讀', 0);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_attachment`
--

CREATE TABLE IF NOT EXISTS `aiyue_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '附件顯示名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件類型',
  `source` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '資源ID',
  `record_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '關聯記錄ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小',
  `dir` int(12) unsigned NOT NULL DEFAULT '0' COMMENT '上級目錄ID',
  `sort` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '狀態',
  PRIMARY KEY (`id`),
  KEY `idx_record_status` (`record_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='附件表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_attribute`
--

CREATE TABLE IF NOT EXISTS `aiyue_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '字段名',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '字段註釋',
  `field` varchar(100) NOT NULL DEFAULT '' COMMENT '字段定義',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '數據類型',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '字段默認值',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '備註',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否顯示',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '參數',
  `model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '模型id',
  `is_must` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否必填',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `validate_rule` varchar(255) NOT NULL,
  `validate_time` tinyint(1) unsigned NOT NULL,
  `error_info` varchar(100) NOT NULL,
  `validate_type` varchar(25) NOT NULL,
  `auto_rule` varchar(100) NOT NULL,
  `auto_time` tinyint(1) unsigned NOT NULL,
  `auto_type` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='模型屬性表' AUTO_INCREMENT=158 ;

--
-- 转存表中的数据 `aiyue_attribute`
--

INSERT INTO `aiyue_attribute` (`id`, `name`, `title`, `field`, `type`, `value`, `remark`, `is_show`, `extra`, `model_id`, `is_must`, `status`, `update_time`, `create_time`, `validate_rule`, `validate_time`, `error_info`, `validate_type`, `auto_rule`, `auto_time`, `auto_type`) VALUES
(1, 'uid', '用戶ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1384508362, 1383891233, '', 0, '', '', '', 0, ''),
(2, 'name', '名稱', 'char(40) NOT NULL ', 'string', '', '同壹根節點下標識不重復', 1, '', 1, 0, 1, 1431151492, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(3, 'title', '標題', 'char(80) NOT NULL ', 'string', '', '文檔標題', 1, '', 1, 0, 1, 1383894778, 1383891233, '', 0, '', '', '', 0, ''),
(4, 'category_id', '所屬分類', 'int(10) unsigned NOT NULL ', 'string', '', '', 0, '', 1, 0, 1, 1384508336, 1383891233, '', 0, '', '', '', 0, ''),
(5, 'description', '描述', 'char(140) NOT NULL ', 'textarea', '', '', 1, '', 1, 0, 1, 1383894927, 1383891233, '', 0, '', '', '', 0, ''),
(6, 'root', '根節點', 'int(10) unsigned NOT NULL ', 'num', '0', '該文檔的頂級文檔編號', 0, '', 1, 0, 1, 1384508323, 1383891233, '', 0, '', '', '', 0, ''),
(7, 'pid', '所屬ID', 'int(10) unsigned NOT NULL ', 'num', '0', '父文檔編號', 0, '', 1, 0, 1, 1384508543, 1383891233, '', 0, '', '', '', 0, ''),
(8, 'model_id', '內容模型ID', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '該文檔所對應的模型', 0, '', 1, 0, 1, 1384508350, 1383891233, '', 0, '', '', '', 0, ''),
(9, 'type', '內容類型', 'tinyint(3) unsigned NOT NULL ', 'select', '2', '', 1, '1:目錄\r\n2:主題\r\n3:段落', 1, 0, 1, 1384511157, 1383891233, '', 0, '', '', '', 0, ''),
(10, 'position', '推薦位', 'smallint(5) unsigned NOT NULL ', 'checkbox', '0', '多個推薦則將其推薦值相加', 1, '1:列表推薦\r\n2:頻道頁推薦\r\n4:首頁推薦', 1, 0, 1, 1383895640, 1383891233, '', 0, '', '', '', 0, ''),
(11, 'link_id', '外鏈', 'int(10) unsigned NOT NULL ', 'num', '0', '0-非外鏈，大於0-外鏈ID,需要函數進行鏈接與編號的轉換', 1, '', 1, 0, 1, 1383895757, 1383891233, '', 0, '', '', '', 0, ''),
(12, 'cover_id', '封面', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-無封面，大於0-封面圖片ID，需要函數處理', 1, '', 1, 0, 1, 1384147827, 1383891233, '', 0, '', '', '', 0, ''),
(13, 'display', '可見性', 'tinyint(3) unsigned NOT NULL ', 'radio', '1', '', 1, '0:不可見\r\n1:所有人可見', 1, 0, 1, 1386662271, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(14, 'deadline', '截至時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '0-永久有效', 1, '', 1, 0, 1, 1387163248, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(15, 'attach', '附件數量', 'tinyint(3) unsigned NOT NULL ', 'num', '0', '', 0, '', 1, 0, 1, 1387260355, 1383891233, '', 0, '', 'regex', '', 0, 'function'),
(16, 'view', '瀏覽量', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895835, 1383891233, '', 0, '', '', '', 0, ''),
(17, 'comment', '評論數', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 1, 0, 1, 1383895846, 1383891233, '', 0, '', '', '', 0, ''),
(18, 'extend', '擴展統計字段', 'int(10) unsigned NOT NULL ', 'num', '0', '根據需求自行使用', 0, '', 1, 0, 1, 1384508264, 1383891233, '', 0, '', '', '', 0, ''),
(19, 'level', '優先級', 'int(10) unsigned NOT NULL ', 'num', '0', '越高排序越靠前', 1, '', 1, 0, 1, 1383895894, 1383891233, '', 0, '', '', '', 0, ''),
(20, 'create_time', '創建時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 1, '', 1, 0, 1, 1383895903, 1383891233, '', 0, '', '', '', 0, ''),
(21, 'update_time', '更新時間', 'int(10) unsigned NOT NULL ', 'datetime', '0', '', 0, '', 1, 0, 1, 1384508277, 1383891233, '', 0, '', '', '', 0, ''),
(22, 'status', '數據狀態', 'tinyint(4) NOT NULL ', 'radio', '0', '', 0, '-1:刪除\r\n0:禁用\r\n1:正常\r\n2:待審核\r\n3:草稿', 1, 0, 1, 1384508496, 1383891233, '', 0, '', '', '', 0, ''),
(143, 'show', '', 'int(10) UNSIGNED NOT NULL', 'num', '1', '', 1, '', 2, 0, 1, 1439449376, 1439449376, '', 3, '', 'regex', '', 3, 'function'),
(27, 'parse', '內容解析類型', 'tinyint(3) unsigned NOT NULL ', 'select', '0', '', 0, '0:html\r\n1:ubb\r\n2:markdown', 3, 0, 1, 1387260461, 1383891252, '', 0, '', 'regex', '', 0, 'function'),
(28, 'content', '下載詳細描述', 'text NOT NULL ', 'editor', '', '', 1, '', 3, 0, 1, 1383896438, 1383891252, '', 0, '', '', '', 0, ''),
(29, 'template', '詳情頁顯示模板', 'varchar(100) NOT NULL ', 'string', '', '', 1, '', 3, 0, 1, 1383896429, 1383891252, '', 0, '', '', '', 0, ''),
(30, 'file_id', '文件ID', 'int(10) unsigned NOT NULL ', 'file', '0', '需要函數處理', 1, '', 3, 0, 1, 1383896415, 1383891252, '', 0, '', '', '', 0, ''),
(31, 'download', '下載次數', 'int(10) unsigned NOT NULL ', 'num', '0', '', 1, '', 3, 0, 1, 1383896380, 1383891252, '', 0, '', '', '', 0, ''),
(32, 'size', '文件大小', 'bigint(20) unsigned NOT NULL ', 'num', '0', '單位bit', 1, '', 3, 0, 1, 1383896371, 1383891252, '', 0, '', '', '', 0, ''),
(145, 'content', '課程簡介', 'text NOT NULL', 'editor', '', '', 1, '', 29, 0, 1, 1439469797, 1439469797, '', 3, '', 'regex', '', 3, 'function'),
(34, 'pic', '考試安排表', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 4, 0, 1, 1439471237, 1430126339, '', 3, '', 'regex', '', 3, 'function'),
(43, 'content', '描述', 'text NOT NULL', 'textarea', '', '每個用 |字符 分開', 1, '', 7, 0, 1, 1437378902, 1430383216, '', 3, '', 'regex', '', 3, 'function'),
(37, 'content', '描述', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 5, 0, 1, 1430273805, 1430273805, '', 3, '', 'regex', '', 3, 'function'),
(38, 'image', '圖片', 'int(10) UNSIGNED NOT NULL', 'pictures', '', '', 1, '', 5, 0, 1, 1441783394, 1430274376, '', 3, '', 'regex', '', 3, 'function'),
(115, 'house', '房間類型', 'varchar(255) NOT NULL', 'bool', '', '', 1, '', 15, 0, 1, 1432519742, 1432518658, '', 3, '', 'regex', '', 3, 'function'),
(41, 'houseName', '房間名', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 6, 0, 1, 1432370808, 1430275119, '', 3, '', 'regex', '', 3, 'function'),
(42, 'note', '備註', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 6, 0, 1, 1432370828, 1430275145, '', 3, '', 'regex', '', 3, 'function'),
(44, 'image', 'logo圖片', 'int(10) UNSIGNED NOT NULL', 'pictures', '', '', 1, '', 8, 0, 1, 1438833828, 1430625197, '', 3, '', 'regex', '', 3, 'function'),
(144, 'pic', '上傳圖片', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 29, 0, 1, 1439469372, 1439469372, '', 3, '', 'regex', '', 3, 'function'),
(46, 'name', '姓名', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 10, 0, 1, 1430977955, 1430977955, '', 3, '', 'regex', '', 3, 'function'),
(47, 'phone', '電話', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 10, 0, 1, 1430978115, 1430978115, '', 3, '', 'regex', '', 3, 'function'),
(48, 'email', '郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 10, 0, 1, 1430978210, 1430978136, '', 3, '', 'regex', '', 3, 'function'),
(49, 'content', '內容', 'text NOT NULL', 'textarea', '', '', 1, '', 10, 0, 1, 1430978228, 1430978228, '', 3, '', 'regex', '', 3, 'function'),
(50, 'time', '時間', 'int(10) UNSIGNED NOT NULL', 'datetime', '', '', 1, '', 10, 0, 1, 1430978808, 1430978808, '', 3, '', 'regex', '', 3, 'function'),
(51, 'status', '狀態', 'int(10) UNSIGNED NOT NULL', 'num', '', '', 1, '', 10, 0, 1, 1430985732, 1430985682, '', 3, '', 'regex', '', 3, 'function'),
(52, 'name', '姓名', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1430987277, 1430987277, '', 3, '', 'regex', '', 3, 'function'),
(53, 'mobile', '手機', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1430987297, 1430987297, '', 3, '', 'regex', '', 3, 'function'),
(54, 'address', '門店', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1430987314, 1430987314, '', 3, '', 'regex', '', 3, 'function'),
(55, 'course', '課程', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1430987363, 1430987363, '', 3, '', 'regex', '', 3, 'function'),
(56, 'content', '詳情介紹', 'text NOT NULL', 'textarea', '', '', 1, '', 12, 0, 1, 1431140566, 1431140566, '', 3, '', 'regex', '', 3, 'function'),
(57, 'images', '圖片', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 12, 0, 1, 1431142732, 1431142732, '', 3, '', 'regex', '', 3, 'function'),
(58, 'image', '圖片', 'int(10) UNSIGNED NOT NULL', 'pictures', '', '', 1, '', 13, 0, 1, 1438833879, 1431495989, '', 3, '', 'regex', '', 3, 'function'),
(59, 'content', '介紹', 'text NOT NULL', 'textarea', '', '', 1, '', 13, 0, 1, 1431496097, 1431496097, '', 3, '', 'regex', '', 3, 'function'),
(60, 'link', '鏈接', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 13, 0, 1, 1431496114, 1431496114, '', 3, '', 'regex', '', 3, 'function'),
(62, 'address', '上課地點', 'varchar(100) NOT NULL', 'checkboxs', '', '', 1, '46', 14, 0, 1, 1437546030, 1431503681, '', 3, '', 'regex', '', 3, 'function'),
(63, 'num', '班級人數', 'varchar(255) NOT NULL', 'string', '', '以，號隔開', 1, '', 14, 0, 1, 1431504460, 1431503701, '', 3, '', 'regex', '', 3, 'function'),
(65, 'grade', '級別', 'varchar(255) NOT NULL', 'string', '', '以，號隔開', 1, '', 14, 0, 1, 1431504441, 1431503801, '', 3, '', 'regex', '', 3, 'function'),
(67, 'Hallnum', '堂數', 'varchar(255) NOT NULL', 'string', '', '以，號隔開', 1, '', 14, 0, 1, 1431504422, 1431503900, '', 3, '', 'regex', '', 3, 'function'),
(68, 'price', '每堂價格', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 14, 0, 1, 1431504415, 1431503938, '', 3, '', 'regex', '', 3, 'function'),
(82, 'note', '備註', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 14, 0, 1, 1431935912, 1431935912, '', 3, '', 'regex', '', 3, 'function'),
(85, 'num', '班級人數', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936152, 1431936152, '', 3, '', 'regex', '', 3, 'function'),
(86, 'grade', '級別', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936175, 1431936175, '', 3, '', 'regex', '', 3, 'function'),
(70, 'time', '時間', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 14, 0, 1, 1431514915, 1431514915, '', 3, '', 'regex', '', 3, 'function'),
(84, 'time', '時間', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936138, 1431936138, '', 3, '', 'regex', '', 3, 'function'),
(83, 'address', '地點', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936113, 1431936113, '', 3, '', 'regex', '', 3, 'function'),
(72, 'address', '上課地點', 'varchar(255) NOT NULL', 'string', '', '下拉選擇房間類型', 1, '', 15, 0, 1, 1432373109, 1431574537, '', 3, '', 'regex', '', 3, 'function'),
(73, 'vipprice', '會員價', 'varchar(255) NOT NULL', 'string', '', '會員收費（以，號隔開）', 1, '', 15, 0, 1, 1432519164, 1431574556, '', 3, '', 'regex', '', 3, 'function'),
(76, 'address', '地址', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 16, 0, 1, 1431681753, 1431681753, '', 3, '', 'regex', '', 3, 'function'),
(77, 'phone', '電話', 'varchar(255) NOT NULL', 'string', '', '多個電話號碼以，隔開', 1, '', 16, 0, 1, 1431681888, 1431681768, '', 3, '', 'regex', '', 3, 'function'),
(78, 'Email', 'E－mail', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 16, 0, 1, 1431681795, 1431681795, '', 3, '', 'regex', '', 3, 'function'),
(79, 'images', '圖片', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 16, 0, 1, 1441938985, 1431681815, '', 3, '', 'regex', '', 3, 'function'),
(80, 'map', '坐標', 'varchar(255) NOT NULL', 'string', '', '選擇地圖坐標', 1, '', 16, 0, 1, 1431681863, 1431681838, '', 3, '', 'regex', '', 3, 'function'),
(87, 'Hallnum', '堂數', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936243, 1431936243, '', 3, '', 'regex', '', 3, 'function'),
(88, 'price', '價格', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1431936260, 1431936260, '', 3, '', 'regex', '', 3, 'function'),
(148, 'sevice', '服務內容', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 18, 0, 1, 1439864489, 1439864489, '', 3, '', 'regex', '', 3, 'function'),
(149, 'copyright', '版權信息', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 30, 0, 1, 1439865449, 1439865449, '', 3, '', 'regex', '', 3, 'function'),
(150, 'facebook', 'Facebook帳號', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 30, 0, 1, 1439865502, 1439865502, '', 3, '', 'regex', '', 3, 'function'),
(151, 'youtube', 'You Tube帳號', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 30, 0, 1, 1439865527, 1439865527, '', 3, '', 'regex', '', 3, 'function'),
(152, 'description', '網站描述', 'text NOT NULL', 'textarea', '', '', 1, '', 30, 0, 1, 1439865561, 1439865561, '', 3, '', 'regex', '', 3, 'function'),
(95, 'coursename', '課程名稱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 17, 0, 1, 1432026468, 1432026468, '', 3, '', 'regex', '', 3, 'function'),
(96, 'instruName', '樂器名稱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 19, 0, 1, 1432281209, 1432281209, '', 3, '', 'regex', '', 3, 'function'),
(97, 'images', '圖片', 'int(10) UNSIGNED NOT NULL', 'pictures', '', '', 1, '', 20, 0, 1, 1440471346, 1432281754, '', 3, '', 'regex', '', 3, 'function'),
(98, 'instru', '樂器名稱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 20, 0, 1, 1432287698, 1432287698, '', 3, '', 'regex', '', 3, 'function'),
(99, 'price', '非會員價', 'varchar(255) NOT NULL', 'string', '', '非會員的價格（以，號隔開）', 1, '', 15, 0, 1, 1432519149, 1432362636, '', 3, '', 'regex', '', 3, 'function'),
(153, 'keyword', '網站關鍵字', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 30, 0, 1, 1439865575, 1439865575, '', 3, '', 'regex', '', 3, 'function'),
(154, 'GOOGLEANALYTICS', 'GOOGLE ANALYTICS設置', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 30, 0, 1, 1439865604, 1439865604, '', 3, '', 'regex', '', 3, 'function'),
(108, 'number', '人數', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370118, 1432370118, '', 3, '', 'regex', '', 3, 'function'),
(109, 'level', '級別', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370132, 1432370132, '', 3, '', 'regex', '', 3, 'function'),
(110, 'class', '年級', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370150, 1432370150, '', 3, '', 'regex', '', 3, 'function'),
(111, 'price', '價格', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370170, 1432370170, '', 3, '', 'regex', '', 3, 'function'),
(112, 'time', '時間', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370184, 1432370184, '', 3, '', 'regex', '', 3, 'function'),
(113, 'courseid', '課程id', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1432370206, 1432370206, '', 3, '', 'regex', '', 3, 'function'),
(116, 'pic', '背景圖', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 22, 0, 1, 1437375642, 1437375642, '', 3, '', 'regex', '', 3, 'function'),
(117, 'pic', '上傳圖片', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 23, 0, 1, 1437378101, 1437378101, '', 3, '', 'regex', '', 3, 'function'),
(120, 'pic', '背景圖', 'varchar(255) NOT NULL', 'pictures', '', '', 1, '', 9, 0, 1, 1437378979, 1437378979, '', 3, '', 'regex', '', 3, 'function'),
(121, 'email', '電子郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 11, 0, 1, 1437533829, 1437533829, '', 3, '', 'regex', '', 3, 'function'),
(122, 'email', '電子郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 24, 0, 1, 1437536069, 1437536069, '', 3, '', 'regex', '', 3, 'function'),
(124, 'emails', '電子郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 1, 0, 1, 1437640642, 1437640642, '', 3, '', 'regex', '', 3, 'function'),
(125, 'sid', '標識', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 25, 0, 1, 1437806509, 1437806509, '', 3, '', 'regex', '', 3, 'function'),
(126, 'detail', '內容', 'text NOT NULL', 'editor', '', '', 1, '', 1, 0, 1, 1437806627, 1437806627, '', 3, '', 'regex', '', 3, 'function'),
(127, 'age', '學員年齡', 'int(10) UNSIGNED NOT NULL', 'num', '', '', 1, '', 11, 0, 1, 1437962365, 1437962365, '', 3, '', 'regex', '', 3, 'function'),
(128, 'ccount', '賬號', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437976203, 1437976203, '', 3, '', 'regex', '', 3, 'function'),
(129, 'uids', '會員卡號', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437976250, 1437976250, '', 3, '', 'regex', '', 3, 'function'),
(130, 'password', '密碼', 'varchar(255) NOT NULL', 'password', '', '', 1, '', 26, 0, 1, 1437976273, 1437976273, '', 3, '', 'regex', '', 3, 'function'),
(131, 'tel', '電話', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437976317, 1437976317, '', 3, '', 'regex', '', 3, 'function'),
(132, 'email', '郵箱', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437976335, 1437976335, '', 3, '', 'regex', '', 3, 'function'),
(133, 'add', '地址', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437976345, 1437976345, '', 3, '', 'regex', '', 3, 'function'),
(134, 'age', '年齡', 'int(10) UNSIGNED NOT NULL', 'num', '', '', 1, '', 26, 0, 1, 1437976355, 1437976355, '', 3, '', 'regex', '', 3, 'function'),
(137, 'create_time', '更新時間', 'int(10) UNSIGNED NOT NULL', 'datetime', '', '', 1, '', 26, 0, 1, 1437985958, 1437985958, '', 3, '', 'regex', '', 3, 'function'),
(136, 'status', '狀態', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 26, 0, 1, 1437979267, 1437979267, '', 3, '', 'regex', '', 3, 'function'),
(138, 'memberID', '会员ID', 'int(10) UNSIGNED NOT NULL', 'num', '', '', 1, '', 11, 1, 1, 1438152036, 1438152036, '', 3, '', 'regex', '', 3, 'function'),
(140, 'title', '通知标题', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 28, 1, 1, 1438159436, 1438159414, '', 3, '', 'regex', '', 3, 'function'),
(141, 'txt', '内容', 'text NOT NULL', 'textarea', '', '', 1, '', 28, 0, 1, 1438159506, 1438159506, '', 3, '', 'regex', '', 3, 'function'),
(142, 'uid', '会员ID', 'int(10) UNSIGNED NOT NULL', 'num', '', '', 1, '', 28, 0, 1, 1438159554, 1438159554, '', 3, '', 'regex', '', 3, 'function'),
(155, 'tel', '电话', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 15, 0, 1, 1441942175, 1441942175, '', 3, '', 'regex', '', 3, 'function'),
(156, 'add', '地址', 'varchar(255) NOT NULL', 'string', '', '', 1, '', 15, 0, 1, 1441942197, 1441942197, '', 3, '', 'regex', '', 3, 'function'),
(157, 'url', '鏈接', 'varchar(255) NOT NULL', 'string', '', '完整鏈接', 1, '', 23, 0, 1, 1456803242, 1456803242, '', 3, '', 'regex', '', 3, 'function');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_auth_extend`
--

CREATE TABLE IF NOT EXISTS `aiyue_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用戶id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '擴展表中數據的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '擴展類型標識 1:欄目分類權限;2:模型權限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用戶組與分類的對應關系表';

--
-- 转存表中的数据 `aiyue_auth_extend`
--

INSERT INTO `aiyue_auth_extend` (`group_id`, `extend_id`, `type`) VALUES
(1, 1, 1),
(1, 1, 2),
(1, 2, 1),
(1, 2, 2),
(1, 3, 1),
(1, 3, 2),
(1, 4, 1),
(1, 37, 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_auth_group`
--

CREATE TABLE IF NOT EXISTS `aiyue_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶組id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '用戶組所屬模塊',
  `type` tinyint(4) NOT NULL COMMENT '組類型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用戶組中文名稱',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用戶組狀態：為1正常，為0禁用,-1為刪除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用戶組擁有的規則id，多個規則 , 隔開',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `aiyue_auth_group`
--

INSERT INTO `aiyue_auth_group` (`id`, `module`, `type`, `title`, `description`, `status`, `rules`) VALUES
(1, 'admin', 1, '默認用戶組', '', 1, '1,2,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,100,102,103,105,106'),
(2, 'admin', 1, '管理員', '商家管理員', 1, '1,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,82,83,84,88,89,90,91,92,93,100,102,103,195,218,219'),
(3, 'admin', 1, '用護', '普通用護', 1, '1,3,7,8,9,10,11,12,13,14,15,16,17,18,79,108,109,211,218,219,220');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_auth_group_access`
--

CREATE TABLE IF NOT EXISTS `aiyue_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用戶組id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `aiyue_auth_group_access`
--

INSERT INTO `aiyue_auth_group_access` (`uid`, `group_id`) VALUES
(2, 3);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_auth_rule`
--

CREATE TABLE IF NOT EXISTS `aiyue_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '規則id,自增主鍵',
  `module` varchar(20) NOT NULL COMMENT '規則所屬module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜單',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '規則唯壹英文標識',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '規則中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:無效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '規則附加條件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=221 ;

--
-- 转存表中的数据 `aiyue_auth_rule`
--

INSERT INTO `aiyue_auth_rule` (`id`, `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
(1, 'admin', 2, 'Admin/Index/index', '首頁', 1, ''),
(2, 'admin', 2, 'Admin/Article/mydocument', '內容', -1, ''),
(3, 'admin', 2, 'Admin/User/index', '用戶', 1, ''),
(4, 'admin', 2, 'Admin/Addons/index', '擴展', 1, ''),
(5, 'admin', 2, 'Admin/Config/group', '系統', 1, ''),
(7, 'admin', 1, 'Admin/article/add', '新增', 1, ''),
(8, 'admin', 1, 'Admin/article/edit', '編輯', 1, ''),
(9, 'admin', 1, 'Admin/article/setStatus', '改變狀態', 1, ''),
(10, 'admin', 1, 'Admin/article/update', '保存', 1, ''),
(11, 'admin', 1, 'Admin/article/autoSave', '保存草稿', 1, ''),
(12, 'admin', 1, 'Admin/article/move', '移動', 1, ''),
(13, 'admin', 1, 'Admin/article/copy', '復制', 1, ''),
(14, 'admin', 1, 'Admin/article/paste', '粘貼', 1, ''),
(15, 'admin', 1, 'Admin/article/permit', '還原', 1, ''),
(16, 'admin', 1, 'Admin/article/clear', '清空', 1, ''),
(17, 'admin', 1, 'Admin/article/index', '文檔列表', 1, ''),
(18, 'admin', 1, 'Admin/article/recycle', '回收站', 1, ''),
(19, 'admin', 1, 'Admin/User/addaction', '新增用戶行為', 1, ''),
(20, 'admin', 1, 'Admin/User/editaction', '編輯用戶行為', 1, ''),
(21, 'admin', 1, 'Admin/User/saveAction', '保存用戶行為', 1, ''),
(22, 'admin', 1, 'Admin/User/setStatus', '變更行為狀態', 1, ''),
(23, 'admin', 1, 'Admin/User/changeStatus?method=forbidUser', '禁用會員', 1, ''),
(24, 'admin', 1, 'Admin/User/changeStatus?method=resumeUser', '啟用會員', 1, ''),
(25, 'admin', 1, 'Admin/User/changeStatus?method=deleteUser', '刪除會員', 1, ''),
(26, 'admin', 1, 'Admin/User/index', '用戶信息', 1, ''),
(27, 'admin', 1, 'Admin/User/action', '用戶行為', 1, ''),
(28, 'admin', 1, 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', 1, ''),
(29, 'admin', 1, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', 1, ''),
(30, 'admin', 1, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', 1, ''),
(31, 'admin', 1, 'Admin/AuthManager/createGroup', '新增', 1, ''),
(32, 'admin', 1, 'Admin/AuthManager/editGroup', '編輯', 1, ''),
(33, 'admin', 1, 'Admin/AuthManager/writeGroup', '保存用戶組', 1, ''),
(34, 'admin', 1, 'Admin/AuthManager/group', '授權', 1, ''),
(35, 'admin', 1, 'Admin/AuthManager/access', '訪問授權', 1, ''),
(36, 'admin', 1, 'Admin/AuthManager/user', '成員授權', 1, ''),
(37, 'admin', 1, 'Admin/AuthManager/removeFromGroup', '解除授權', 1, ''),
(38, 'admin', 1, 'Admin/AuthManager/addToGroup', '保存成員授權', 1, ''),
(39, 'admin', 1, 'Admin/AuthManager/category', '分類授權', 1, ''),
(40, 'admin', 1, 'Admin/AuthManager/addToCategory', '保存分類授權', 1, ''),
(41, 'admin', 1, 'Admin/AuthManager/index', '權限管理', 1, ''),
(42, 'admin', 1, 'Admin/Addons/create', '創建', 1, ''),
(43, 'admin', 1, 'Admin/Addons/checkForm', '檢測創建', 1, ''),
(44, 'admin', 1, 'Admin/Addons/preview', '預覽', 1, ''),
(45, 'admin', 1, 'Admin/Addons/build', '快速生成插件', 1, ''),
(46, 'admin', 1, 'Admin/Addons/config', '設置', 1, ''),
(47, 'admin', 1, 'Admin/Addons/disable', '禁用', 1, ''),
(48, 'admin', 1, 'Admin/Addons/enable', '啟用', 1, ''),
(49, 'admin', 1, 'Admin/Addons/install', '安裝', 1, ''),
(50, 'admin', 1, 'Admin/Addons/uninstall', '卸載', 1, ''),
(51, 'admin', 1, 'Admin/Addons/saveconfig', '更新配置', 1, ''),
(52, 'admin', 1, 'Admin/Addons/adminList', '插件後臺列表', 1, ''),
(53, 'admin', 1, 'Admin/Addons/execute', 'URL方式訪問插件', 1, ''),
(54, 'admin', 1, 'Admin/Addons/index', '插件管理', 1, ''),
(55, 'admin', 1, 'Admin/Addons/hooks', '鉤子管理', 1, ''),
(56, 'admin', 1, 'Admin/model/add', '新增', 1, ''),
(57, 'admin', 1, 'Admin/model/edit', '編輯', 1, ''),
(58, 'admin', 1, 'Admin/model/setStatus', '改變狀態', 1, ''),
(59, 'admin', 1, 'Admin/model/update', '保存數據', 1, ''),
(60, 'admin', 1, 'Admin/Model/index', '模型管理', 1, ''),
(61, 'admin', 1, 'Admin/Config/edit', '編輯', 1, ''),
(62, 'admin', 1, 'Admin/Config/del', '刪除', 1, ''),
(63, 'admin', 1, 'Admin/Config/add', '新增', 1, ''),
(64, 'admin', 1, 'Admin/Config/save', '保存', 1, ''),
(65, 'admin', 1, 'Admin/Config/group', '網站設置', 1, ''),
(66, 'admin', 1, 'Admin/Config/index', '配置管理', 1, ''),
(67, 'admin', 1, 'Admin/Channel/add', '新增', 1, ''),
(68, 'admin', 1, 'Admin/Channel/edit', '編輯', 1, ''),
(69, 'admin', 1, 'Admin/Channel/del', '刪除', 1, ''),
(70, 'admin', 1, 'Admin/Channel/index', '導航管理', 1, ''),
(71, 'admin', 1, 'Admin/Category/edit', '編輯', 1, ''),
(72, 'admin', 1, 'Admin/Category/add', '新增', 1, ''),
(73, 'admin', 1, 'Admin/Category/remove', '刪除', 1, ''),
(74, 'admin', 1, 'Admin/Category/index', '分類管理', 1, ''),
(75, 'admin', 1, 'Admin/file/upload', '上傳控件', -1, ''),
(76, 'admin', 1, 'Admin/file/uploadPicture', '上傳圖片', -1, ''),
(77, 'admin', 1, 'Admin/file/download', '下載', -1, ''),
(94, 'admin', 1, 'Admin/AuthManager/modelauth', '模型授權', 1, ''),
(79, 'admin', 1, 'Admin/article/batchOperate', '導入', 1, ''),
(80, 'admin', 1, 'Admin/Database/index?type=export', '備份數據庫', 1, ''),
(81, 'admin', 1, 'Admin/Database/index?type=import', '還原數據庫', 1, ''),
(82, 'admin', 1, 'Admin/Database/export', '備份', 1, ''),
(83, 'admin', 1, 'Admin/Database/optimize', '優化表', 1, ''),
(84, 'admin', 1, 'Admin/Database/repair', '修復表', 1, ''),
(86, 'admin', 1, 'Admin/Database/import', '恢復', 1, ''),
(87, 'admin', 1, 'Admin/Database/del', '刪除', 1, ''),
(88, 'admin', 1, 'Admin/User/add', '新增用戶', 1, ''),
(89, 'admin', 1, 'Admin/Attribute/index', '屬性管理', 1, ''),
(90, 'admin', 1, 'Admin/Attribute/add', '新增', 1, ''),
(91, 'admin', 1, 'Admin/Attribute/edit', '編輯', 1, ''),
(92, 'admin', 1, 'Admin/Attribute/setStatus', '改變狀態', 1, ''),
(93, 'admin', 1, 'Admin/Attribute/update', '保存數據', 1, ''),
(95, 'admin', 1, 'Admin/AuthManager/addToModel', '保存模型授權', 1, ''),
(96, 'admin', 1, 'Admin/Category/move', '移動', -1, ''),
(97, 'admin', 1, 'Admin/Category/merge', '合並', -1, ''),
(98, 'admin', 1, 'Admin/Config/menu', '後臺菜單管理', -1, ''),
(99, 'admin', 1, 'Admin/Article/mydocument', '內容', -1, ''),
(100, 'admin', 1, 'Admin/Menu/index', '菜單管理', 1, ''),
(101, 'admin', 1, 'Admin/other', '其他', -1, ''),
(102, 'admin', 1, 'Admin/Menu/add', '新增', 1, ''),
(103, 'admin', 1, 'Admin/Menu/edit', '編輯', 1, ''),
(104, 'admin', 1, 'Admin/Think/lists?model=article', '文章管理', -1, ''),
(105, 'admin', 1, 'Admin/Think/lists?model=download', '下載管理', 1, ''),
(106, 'admin', 1, 'Admin/Think/lists?model=config', '配置管理', 1, ''),
(107, 'admin', 1, 'Admin/Action/actionlog', '行為日誌', 1, ''),
(108, 'admin', 1, 'Admin/User/updatePassword', '修改密碼', 1, ''),
(109, 'admin', 1, 'Admin/User/updateNickname', '修改昵稱', 1, ''),
(110, 'admin', 1, 'Admin/action/edit', '查看行為日誌', 1, ''),
(205, 'admin', 1, 'Admin/think/add', '新增數據', 1, ''),
(111, 'admin', 2, 'Admin/article/index', '文檔列表', -1, ''),
(112, 'admin', 2, 'Admin/article/add', '新增', -1, ''),
(113, 'admin', 2, 'Admin/article/edit', '編輯', -1, ''),
(114, 'admin', 2, 'Admin/article/setStatus', '改變狀態', -1, ''),
(115, 'admin', 2, 'Admin/article/update', '保存', -1, ''),
(116, 'admin', 2, 'Admin/article/autoSave', '保存草稿', -1, ''),
(117, 'admin', 2, 'Admin/article/move', '移動', -1, ''),
(118, 'admin', 2, 'Admin/article/copy', '復制', -1, ''),
(119, 'admin', 2, 'Admin/article/paste', '粘貼', -1, ''),
(120, 'admin', 2, 'Admin/article/batchOperate', '導入', -1, ''),
(121, 'admin', 2, 'Admin/article/recycle', '回收站', -1, ''),
(122, 'admin', 2, 'Admin/article/permit', '還原', -1, ''),
(123, 'admin', 2, 'Admin/article/clear', '清空', -1, ''),
(124, 'admin', 2, 'Admin/User/add', '新增用戶', -1, ''),
(125, 'admin', 2, 'Admin/User/action', '用戶行為', -1, ''),
(126, 'admin', 2, 'Admin/User/addAction', '新增用戶行為', -1, ''),
(127, 'admin', 2, 'Admin/User/editAction', '編輯用戶行為', -1, ''),
(128, 'admin', 2, 'Admin/User/saveAction', '保存用戶行為', -1, ''),
(129, 'admin', 2, 'Admin/User/setStatus', '變更行為狀態', -1, ''),
(130, 'admin', 2, 'Admin/User/changeStatus?method=forbidUser', '禁用會員', -1, ''),
(131, 'admin', 2, 'Admin/User/changeStatus?method=resumeUser', '啟用會員', -1, ''),
(132, 'admin', 2, 'Admin/User/changeStatus?method=deleteUser', '刪除會員', -1, ''),
(133, 'admin', 2, 'Admin/AuthManager/index', '權限管理', -1, ''),
(134, 'admin', 2, 'Admin/AuthManager/changeStatus?method=deleteGroup', '刪除', -1, ''),
(135, 'admin', 2, 'Admin/AuthManager/changeStatus?method=forbidGroup', '禁用', -1, ''),
(136, 'admin', 2, 'Admin/AuthManager/changeStatus?method=resumeGroup', '恢復', -1, ''),
(137, 'admin', 2, 'Admin/AuthManager/createGroup', '新增', -1, ''),
(138, 'admin', 2, 'Admin/AuthManager/editGroup', '編輯', -1, ''),
(139, 'admin', 2, 'Admin/AuthManager/writeGroup', '保存用戶組', -1, ''),
(140, 'admin', 2, 'Admin/AuthManager/group', '授權', -1, ''),
(141, 'admin', 2, 'Admin/AuthManager/access', '訪問授權', -1, ''),
(142, 'admin', 2, 'Admin/AuthManager/user', '成員授權', -1, ''),
(143, 'admin', 2, 'Admin/AuthManager/removeFromGroup', '解除授權', -1, ''),
(144, 'admin', 2, 'Admin/AuthManager/addToGroup', '保存成員授權', -1, ''),
(145, 'admin', 2, 'Admin/AuthManager/category', '分類授權', -1, ''),
(146, 'admin', 2, 'Admin/AuthManager/addToCategory', '保存分類授權', -1, ''),
(147, 'admin', 2, 'Admin/AuthManager/modelauth', '模型授權', -1, ''),
(148, 'admin', 2, 'Admin/AuthManager/addToModel', '保存模型授權', -1, ''),
(149, 'admin', 2, 'Admin/Addons/create', '創建', -1, ''),
(150, 'admin', 2, 'Admin/Addons/checkForm', '檢測創建', -1, ''),
(151, 'admin', 2, 'Admin/Addons/preview', '預覽', -1, ''),
(152, 'admin', 2, 'Admin/Addons/build', '快速生成插件', -1, ''),
(153, 'admin', 2, 'Admin/Addons/config', '設置', -1, ''),
(154, 'admin', 2, 'Admin/Addons/disable', '禁用', -1, ''),
(155, 'admin', 2, 'Admin/Addons/enable', '啟用', -1, ''),
(156, 'admin', 2, 'Admin/Addons/install', '安裝', -1, ''),
(157, 'admin', 2, 'Admin/Addons/uninstall', '卸載', -1, ''),
(158, 'admin', 2, 'Admin/Addons/saveconfig', '更新配置', -1, ''),
(159, 'admin', 2, 'Admin/Addons/adminList', '插件後臺列表', -1, ''),
(160, 'admin', 2, 'Admin/Addons/execute', 'URL方式訪問插件', -1, ''),
(161, 'admin', 2, 'Admin/Addons/hooks', '鉤子管理', -1, ''),
(162, 'admin', 2, 'Admin/Model/index', '模型管理', -1, ''),
(163, 'admin', 2, 'Admin/model/add', '新增', -1, ''),
(164, 'admin', 2, 'Admin/model/edit', '編輯', -1, ''),
(165, 'admin', 2, 'Admin/model/setStatus', '改變狀態', -1, ''),
(166, 'admin', 2, 'Admin/model/update', '保存數據', -1, ''),
(167, 'admin', 2, 'Admin/Attribute/index', '屬性管理', -1, ''),
(168, 'admin', 2, 'Admin/Attribute/add', '新增', -1, ''),
(169, 'admin', 2, 'Admin/Attribute/edit', '編輯', -1, ''),
(170, 'admin', 2, 'Admin/Attribute/setStatus', '改變狀態', -1, ''),
(171, 'admin', 2, 'Admin/Attribute/update', '保存數據', -1, ''),
(172, 'admin', 2, 'Admin/Config/index', '配置管理', -1, ''),
(173, 'admin', 2, 'Admin/Config/edit', '編輯', -1, ''),
(174, 'admin', 2, 'Admin/Config/del', '刪除', -1, ''),
(175, 'admin', 2, 'Admin/Config/add', '新增', -1, ''),
(176, 'admin', 2, 'Admin/Config/save', '保存', -1, ''),
(177, 'admin', 2, 'Admin/Menu/index', '菜單管理', -1, ''),
(178, 'admin', 2, 'Admin/Channel/index', '導航管理', -1, ''),
(179, 'admin', 2, 'Admin/Channel/add', '新增', -1, ''),
(180, 'admin', 2, 'Admin/Channel/edit', '編輯', -1, ''),
(181, 'admin', 2, 'Admin/Channel/del', '刪除', -1, ''),
(182, 'admin', 2, 'Admin/Category/index', '分類管理', -1, ''),
(183, 'admin', 2, 'Admin/Category/edit', '編輯', -1, ''),
(184, 'admin', 2, 'Admin/Category/add', '新增', -1, ''),
(185, 'admin', 2, 'Admin/Category/remove', '刪除', -1, ''),
(186, 'admin', 2, 'Admin/Category/move', '移動', -1, ''),
(187, 'admin', 2, 'Admin/Category/merge', '合並', -1, ''),
(188, 'admin', 2, 'Admin/Database/index?type=export', '備份數據庫', -1, ''),
(189, 'admin', 2, 'Admin/Database/export', '備份', -1, ''),
(190, 'admin', 2, 'Admin/Database/optimize', '優化表', -1, ''),
(191, 'admin', 2, 'Admin/Database/repair', '修復表', -1, ''),
(192, 'admin', 2, 'Admin/Database/index?type=import', '還原數據庫', -1, ''),
(193, 'admin', 2, 'Admin/Database/import', '恢復', -1, ''),
(194, 'admin', 2, 'Admin/Database/del', '刪除', -1, ''),
(195, 'admin', 2, 'Admin/other', '其他', 1, ''),
(196, 'admin', 2, 'Admin/Menu/add', '新增', -1, ''),
(197, 'admin', 2, 'Admin/Menu/edit', '編輯', -1, ''),
(198, 'admin', 2, 'Admin/Think/lists?model=article', '應用', -1, ''),
(199, 'admin', 2, 'Admin/Think/lists?model=download', '下載管理', -1, ''),
(200, 'admin', 2, 'Admin/Think/lists?model=config', '應用', -1, ''),
(201, 'admin', 2, 'Admin/Action/actionlog', '行為日誌', -1, ''),
(202, 'admin', 2, 'Admin/User/updatePassword', '修改密碼', -1, ''),
(203, 'admin', 2, 'Admin/User/updateNickname', '修改昵稱', -1, ''),
(204, 'admin', 2, 'Admin/action/edit', '查看行為日誌', -1, ''),
(206, 'admin', 1, 'Admin/think/edit', '編輯數據', 1, ''),
(207, 'admin', 1, 'Admin/Menu/import', '導入', 1, ''),
(208, 'admin', 1, 'Admin/Model/generate', '生成', 1, ''),
(209, 'admin', 1, 'Admin/Addons/addHook', '新增鉤子', 1, ''),
(210, 'admin', 1, 'Admin/Addons/edithook', '編輯鉤子', 1, ''),
(211, 'admin', 1, 'Admin/Article/sort', '文檔排序', 1, ''),
(212, 'admin', 1, 'Admin/Config/sort', '排序', 1, ''),
(213, 'admin', 1, 'Admin/Menu/sort', '排序', 1, ''),
(214, 'admin', 1, 'Admin/Channel/sort', '排序', 1, ''),
(215, 'admin', 1, 'Admin/Category/operate/type/move', '移動', 1, ''),
(216, 'admin', 1, 'Admin/Category/operate/type/merge', '合並', 1, ''),
(217, 'admin', 2, 'Admin/Article/index?cate_id=48', '內容', -1, ''),
(218, 'admin', 1, 'Admin/article/appointment', '課程預約', 1, ''),
(219, 'admin', 1, 'Admin/Article/fback', '留言板', 1, ''),
(220, 'admin', 2, 'Admin/Article/fback', '內容', 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_category`
--

CREATE TABLE IF NOT EXISTS `aiyue_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分類ID',
  `name` varchar(30) NOT NULL COMMENT '標誌',
  `title` varchar(50) NOT NULL COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每頁行數',
  `meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的網頁標題',
  `keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '關鍵字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `template_index` varchar(100) NOT NULL COMMENT '頻道頁模板',
  `template_lists` varchar(100) NOT NULL COMMENT '列表頁模板',
  `template_detail` varchar(100) NOT NULL COMMENT '詳情頁模板',
  `template_edit` varchar(100) NOT NULL COMMENT '編輯頁模板',
  `model` varchar(100) NOT NULL DEFAULT '' COMMENT '關聯模型',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '允許發布的內容類型',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許發布內容',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '可見性',
  `reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允許回復',
  `check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '發布的文章是否需要審核',
  `reply_model` varchar(100) NOT NULL DEFAULT '',
  `extend` text NOT NULL COMMENT '擴展設置',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分類圖標',
  `show` int(10) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='分類表' AUTO_INCREMENT=81 ;

--
-- 转存表中的数据 `aiyue_category`
--

INSERT INTO `aiyue_category` (`id`, `name`, `title`, `pid`, `sort`, `list_row`, `meta_title`, `keywords`, `description`, `template_index`, `template_lists`, `template_detail`, `template_edit`, `model`, `type`, `link_id`, `allow_publish`, `display`, `reply`, `check`, `reply_model`, `extend`, `create_time`, `update_time`, `status`, `icon`, `show`) VALUES
(50, 'music_teacher', '導師介紹', 41, 0, 10, '', '', '', '', '', '', '', '9', '2', 0, 1, 1, 1, 0, '', '', 1430298999, 1430966326, 1, 0, 1),
(51, 'instru_teacher', '導師介紹', 42, 0, 10, '', '', '', '', '', '', '', '9', '2', 0, 1, 1, 1, 0, '', '', 1430299067, 1430966342, 0, 0, 1),
(39, 'index', '首頁', 0, 2, 10, '', '', '', '', '', '', '', '', '2', 0, 0, 1, 1, 0, '', '', 1429089796, 1430383409, 1, 0, 1),
(40, 'aboutaiyue', '關於愛樂', 0, 3, 10, '', '', '', '', '', '', '', '2', '2', 0, 1, 1, 1, 0, '', '', 1429089849, 1429698534, 1, 0, 1),
(41, 'music curriculum', '音樂課程', 0, 4, 10, '', '', '', '', '', '', '', '5', '2', 0, 0, 1, 1, 0, '', '', 1429089933, 1431670863, 1, 0, 1),
(42, 'teaching', '上課地點', 0, 5, 10, '', '', '', '', '', '', '', '5', '2', 0, 0, 1, 1, 0, '', '', 1429090036, 1458790950, 1, 0, 1),
(43, 'piano', '樂器', 0, 6, 10, '', '', '', '', '', '', '', '5', '2', 0, 0, 1, 1, 0, '', '', 1429090070, 1432280606, 1, 0, 1),
(44, 'rent practice', '租練', 0, 7, 10, '', '', '', '', '', '', '', '5', '2', 0, 0, 1, 1, 0, '', '', 1429090126, 1430300137, 1, 0, 1),
(45, 'music exam', 'ABRSM考試', 0, 8, 10, '', '', '', '', '', '', '', '4', '2', 0, 1, 1, 1, 0, '', '', 1429090170, 1438327865, 1, 0, 1),
(46, 'contact us', '聯系我們', 0, 9, 10, '', '', '', '', 'contactlist.html', 'contactlist.html', 'contact.html', '16', '2', 0, 1, 1, 1, 0, '', '', 1429090225, 1431683046, 1, 0, 1),
(47, 'regulations', '學生守則', 0, 10, 10, '', '', '', '', '', '', '', '2', '2', 0, 1, 1, 1, 0, '', '', 1429090281, 1429695531, 1, 0, 1),
(61, 'promotion', '推廣', 39, 1, 10, '', '', '', '', '', '', '', '7,8,22', '2', 0, 1, 1, 1, 0, '', '', 1431482850, 1437377061, 1, 0, 1),
(49, 'logo', 'logo設置', 65, 13, 10, '', '', '', '', '', '', '', '8', '2', 0, 1, 1, 1, 0, '', '', 1430274533, 1432535860, 1, 0, 1),
(52, 'image', '樂器種類', 43, 0, 10, '', '', '', '', '', '', '', '19', '2', 0, 1, 1, 1, 0, '', '', 1430299733, 1432281501, 1, 0, 1),
(53, 'rent_image', '房間圖片', 44, 0, 10, '', '', '', '', '', '', '', '5', '2', 0, 1, 1, 1, 0, '', '', 1430299773, 1432535672, 1, 0, 1),
(65, 'set', '網站設置', 0, 14, 10, '', '', '', '', '', '', '', '30', '2', 0, 1, 1, 1, 0, '', '', 1431673964, 1439865665, 1, 0, 0),
(60, 'store', '愛樂連瑣', 39, 4, 10, '', '', '', '', '', '', '', '12', '2', 0, 1, 1, 1, 0, '', '', 1431143012, 1431143107, 1, 0, 1),
(58, 'characteristic', '特色', 39, 3, 10, '', '', '', '', '', '', '', '13', '2', 0, 1, 1, 1, 0, '', '', 1430383097, 1431496312, 1, 0, 1),
(59, 'adv', '廣告', 39, 2, 10, '', '', '', '', '', '', '', '23', '2', 0, 1, 1, 1, 0, '', '', 1430387785, 1437378219, 1, 0, 1),
(62, 'coursedata', '課程名稱', 41, 1, 10, '', '', '', '', '', '', '', '14', '2', 0, 1, 1, 1, 0, '', '', 1431502172, 1437378784, 1, 0, 1),
(63, 'insCourse', '課程名稱', 42, 1, 10, '', '', '', '', '', '', '', '14', '2', 0, 1, 1, 1, 0, '', '', 1431505487, 1438930521, 0, 0, 1),
(64, 'price', '租琴房價錢', 44, 3, 10, '', '', '', '', '', '', '', '15', '2', 0, 1, 1, 1, 0, '', '', 1431584744, 1432373621, 1, 0, 1),
(66, 'basic', '基本資料', 65, 0, 10, '', '', '', '', '', '', '', '30', '2', 0, 1, 1, 1, 0, '', '', 1431673989, 1439865865, 1, 0, 1),
(67, 'course', '課程組合', 41, 2, 10, '', '', '', '', '', '', '', '17', '2', 0, 1, 1, 1, 0, '', '', 1431934488, 1437378788, 1, 0, 1),
(70, 'course2', '課程組合', 42, 2, 10, '', '', '', '', '', '', '', '17', '2', 0, 1, 1, 1, 0, '', '', 1432194013, 1438930527, 0, 0, 1),
(71, 'instruIma', '樂器圖片', 43, 1, 10, '', '', '', '', '', '', '', '20', '2', 0, 1, 1, 1, 0, '', '', 1432281656, 1432287712, 1, 0, 1),
(78, 'others', '其他服務', 0, 9, 10, '', '', '', '', '', '', '', '18', '', 0, 1, 1, 1, 0, '', '', 1439864289, 1439864334, 1, 0, 1),
(73, 'houses', '房間類型', 44, 1, 10, '', '', '', '', '', '', '', '6', '2', 0, 1, 1, 1, 0, '', '', 1432370897, 1432535675, 1, 0, 1),
(74, 'class', '上課地點', 42, 0, 10, '', '', '', '', '', '', '', '24', '', 0, 1, 1, 1, 0, '', '', 1437536153, 1437536153, 1, 0, 1),
(75, 'c1', '課程簡介', 41, 0, 10, '', '', '', '', '', '', '', '25', '', 0, 1, 1, 1, 0, '', '', 1437806902, 1437806902, 1, 0, 1),
(76, 'c2', '課程簡介', 42, 0, 10, '', '', '', '', '', '', '', '25', '', 0, 1, 1, 1, 0, '', '', 1437806920, 1437806920, 0, 0, 1),
(79, 'yc', '課程簡介', 43, 0, 10, '', '', '', '', '', '', '', '25', '', 0, 1, 1, 1, 0, '', '', 1439866797, 1439866797, 1, 0, 1),
(80, 'zc', '課程簡介', 44, 0, 10, '', '', '', '', '', '', '', '25', '', 0, 1, 1, 1, 0, '', '', 1439866830, 1439866830, 1, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_channel`
--

CREATE TABLE IF NOT EXISTS `aiyue_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '頻道ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級頻道ID',
  `title` char(30) NOT NULL COMMENT '頻道標題',
  `url` char(100) NOT NULL COMMENT '頻道連接',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '導航排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `target` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '新窗口打開',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- 转存表中的数据 `aiyue_channel`
--

INSERT INTO `aiyue_channel` (`id`, `pid`, `title`, `url`, `sort`, `create_time`, `update_time`, `status`, `target`) VALUES
(1, 0, '首頁', 'Index/index', 1, 1379475111, 1429754999, 1, 0),
(2, 0, '關於愛樂', 'Index/about', 2, 1379475131, 1429754071, 1, 0),
(3, 0, '音樂教程', 'Index/teach_music', 3, 1379475154, 1429754331, 1, 0),
(4, 0, '樂器教學', 'Index/teach_instru', 4, 1429754246, 1429754345, 0, 0),
(5, 0, '樂器', 'Index/qin', 5, 1429754282, 1431656984, 1, 0),
(6, 0, '租練', 'Index/rent', 6, 1429754312, 1429754312, 1, 0),
(7, 0, 'ABRSM考試', 'Index/exam', 7, 1429754370, 1438327948, 1, 0),
(8, 0, '聯系我們', 'Index/contact', 9, 1429754393, 1439864015, 1, 0),
(9, 0, '學生守則', 'Index/rules', 10, 1429754412, 1439864011, 1, 0),
(10, 0, '其他服務', 'Index/others', 8, 1439864002, 1441941665, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_config`
--

CREATE TABLE IF NOT EXISTS `aiyue_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名稱',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置說明',
  `group` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置分組',
  `extra` varchar(255) NOT NULL DEFAULT '' COMMENT '配置值',
  `remark` varchar(100) NOT NULL COMMENT '配置說明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '狀態',
  `value` text NOT NULL COMMENT '配置值',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`),
  KEY `type` (`type`),
  KEY `group` (`group`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50 ;

--
-- 转存表中的数据 `aiyue_config`
--

INSERT INTO `aiyue_config` (`id`, `name`, `type`, `title`, `group`, `extra`, `remark`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES
(1, 'WEB_SITE_TITLE', 1, '網站標題', 1, '', '網站名稱', 1378898976, 1443004077, 1, '愛樂琴行', 0),
(2, 'WEB_SITE_DESCRIPTION', 2, '網站描述', 1, '', '網站搜索引擎描述', 1378898976, 1379235841, 1, '愛樂琴行', 1),
(3, 'WEB_SITE_KEYWORD', 2, '網站關鍵字', 1, '', '網站搜索引擎關鍵字', 1378898976, 1381390100, 1, '愛樂琴行', 8),
(4, 'WEB_SITE_CLOSE', 4, '關閉站點', 1, '0:關閉,1:開啟', '站點關閉後其他用護不能訪問，管理員可以正常訪問', 1378898976, 1379235296, 1, '1', 1),
(9, 'CONFIG_TYPE_LIST', 3, '配置類型列表', 4, '', '主要用於數據解析和頁面表單的生成', 1378898976, 1379235348, 1, '0:數字\r\n1:字符\r\n2:文本\r\n3:數組\r\n4:枚舉', 2),
(10, 'WEB_SITE_ICP', 1, '網站備案號', 1, '', '設置在網站底部顯示的備案號，如“滬ICP備12007941號-2', 1378900335, 1379235859, 1, '', 9),
(11, 'DOCUMENT_POSITION', 3, '文檔推薦位', 2, '', '文檔推薦位，推薦到多個位置KEY值相加即可', 1379053380, 1379235329, 1, '1:列表頁推薦\r\n2:頻道頁推薦\r\n4:網站首頁推薦', 3),
(12, 'DOCUMENT_DISPLAY', 3, '文檔可見性', 2, '', '文章可見性僅影響前臺顯示，後臺不收影響', 1379056370, 1379235322, 1, '0:所有人可見\r\n1:僅註冊會員可見\r\n2:僅管理員可見', 4),
(13, 'COLOR_STYLE', 4, '後臺色系', 1, 'default_color:默認\r\nblue_color:紫邏蘭', '後臺顏色風格', 1379122533, 1379235904, 1, 'default_color', 10),
(20, 'CONFIG_GROUP_LIST', 3, '配置分組', 4, '', '配置分組', 1379228036, 1439470463, 1, '1:基本\r\n2:內容\r\n3:用戶\r\n4:系統\r\n5:郵箱', 4),
(21, 'HOOKS_TYPE', 3, '鉤子的類型', 4, '', '類型 1-用於擴展顯示內容，2-用於擴展業務處理', 1379313397, 1379313407, 1, '1:視圖\r\n2:控制器', 6),
(22, 'AUTH_CONFIG', 3, 'Auth配置', 4, '', '自定義Auth.class.php類配置', 1379409310, 1379409564, 1, 'AUTH_ON:1\r\nAUTH_TYPE:2', 8),
(23, 'OPEN_DRAFTBOX', 4, '是否開啟草稿功能', 2, '0:關閉草稿功能\r\n1:開啟草稿功能\r\n', '新增文章時的草稿功能配置', 1379484332, 1379484591, 1, '1', 1),
(24, 'DRAFT_AOTOSAVE_INTERVAL', 0, '自動保存草稿時間', 2, '', '自動保存草稿的時間間隔，單位：秒', 1379484574, 1386143323, 1, '60', 2),
(25, 'LIST_ROWS', 0, '後臺每頁記錄數', 2, '', '後臺數據每頁顯示記錄數', 1379503896, 1380427745, 1, '10', 10),
(26, 'USER_ALLOW_REGISTER', 4, '是否允許用護註冊', 3, '0:關閉註冊\r\n1:允許註冊', '是否開放用護註冊', 1379504487, 1379504580, 1, '1', 3),
(27, 'CODEMIRROR_THEME', 4, '預覽插件的CodeMirror主題', 4, '3024-day:3024 day\r\n3024-night:3024 night\r\nambiance:ambiance\r\nbase16-dark:base16 dark\r\nbase16-light:base16 light\r\nblackboard:blackboard\r\ncobalt:cobalt\r\neclipse:eclipse\r\nelegant:elegant\r\nerlang-dark:erlang-dark\r\nlesser-dark:lesser-dark\r\nmidnight:midnight', '詳情見CodeMirror官網', 1379814385, 1384740813, 1, 'ambiance', 3),
(28, 'DATA_BACKUP_PATH', 1, '數據庫備份根路徑', 4, '', '路徑必須以 / 結尾', 1381482411, 1381482411, 1, './Data/', 5),
(29, 'DATA_BACKUP_PART_SIZE', 0, '數據庫備份卷大小', 4, '', '該值用於限制壓縮後的分卷最大長度。單位：B；建議設置20M', 1381482488, 1381729564, 1, '20971520', 7),
(30, 'DATA_BACKUP_COMPRESS', 4, '數據庫備份文件是否啟用壓縮', 4, '0:不壓縮\r\n1:啟用壓縮', '壓縮備份文件需要PHP環境支持gzopen,gzwrite函數', 1381713345, 1381729544, 1, '1', 9),
(31, 'DATA_BACKUP_COMPRESS_LEVEL', 4, '數據庫備份文件壓縮級別', 4, '1:普通\r\n4:壹般\r\n9:最高', '數據庫備份文件的壓縮級別，該配置在開啟壓縮時生效', 1381713408, 1381713408, 1, '9', 10),
(32, 'DEVELOP_MODE', 4, '開啟開發者模式', 4, '0:關閉\r\n1:開啟', '是否開啟開發者模式', 1383105995, 1383291877, 1, '1', 11),
(33, 'ALLOW_VISIT', 3, '不受限控制器方法', 0, '', '', 1386644047, 1386644741, 1, '0:article/draftbox\r\n1:article/mydocument\r\n2:Category/tree\r\n3:Index/verify\r\n4:file/upload\r\n5:file/download\r\n6:user/updatePassword\r\n7:user/updateNickname\r\n8:user/submitPassword\r\n9:user/submitNickname\r\n10:file/uploadpicture', 0),
(34, 'DENY_VISIT', 3, '超管專限控制器方法', 0, '', '僅超級管理員可訪問的控制器方法', 1386644141, 1386644659, 1, '0:Addons/addhook\r\n1:Addons/edithook\r\n2:Addons/delhook\r\n3:Addons/updateHook\r\n4:Admin/getMenus\r\n5:Admin/recordList\r\n6:AuthManager/updateRules\r\n7:AuthManager/tree', 0),
(35, 'REPLY_LIST_ROWS', 0, '回復列表每頁條數', 2, '', '', 1386645376, 1387178083, 1, '10', 0),
(36, 'ADMIN_ALLOW_IP', 2, '後臺允許訪問IP', 4, '', '多個用逗號分隔，如果不配置表示不限制IP訪問', 1387165454, 1387165553, 1, '', 12),
(37, 'SHOW_PAGE_TRACE', 4, '是否顯示頁面Trace', 4, '0:關閉\r\n1:開啟', '是否顯示頁面Trace信息', 1387165685, 1387165685, 1, '0', 1),
(38, 'WEB_COPYRIGHT', 1, '版權信息', 1, '', '填寫正確的版權信息，會在網站前端底部的版權位置顯示', 1433907037, 1443004108, 1, 'Copyright © 2015 愛樂琴行', 0),
(39, 'FACEBOOK', 1, 'Facebook賬戶', 1, '', '填寫地址時，請加上http://', 1433907125, 1443004194, 1, '', 0),
(40, 'YOUTUBE', 1, 'You Tube賬戶', 1, '', '填寫地址時，請加上http://', 1433907147, 1443004215, 1, '', 0),
(41, 'GOOGLEANALYTICS', 2, 'GOOGLE ANALYTICS設置', 1, '', 'GOOGLE ANALYTICS設置', 1433907164, 1433907218, 1, '', 10),
(42, 'MAIL_HOST', 1, 'smtp服務器的名稱', 5, '', '請填寫smtp服務器的名稱', 1433921310, 1443004242, 1, 'neoweb.cc', 0),
(43, 'MAIL_USERNAME', 1, '你的郵箱名', 5, '', '請填寫你的郵箱名', 1433921329, 1443004269, 1, 'notice@lovemusic.center', 0),
(44, 'MAIL_FROMNAME', 1, '發件人姓名', 5, '', '請填寫發件人姓名', 1433921346, 1443004336, 1, '愛樂琴行', 0),
(45, 'MAIL_PASSWORD', 1, '郵箱密碼', 5, '', '請填寫郵箱密碼', 1433921379, 1443004351, 1, 'e{HNIsx!Fx-S', 0),
(46, 'MAIL_FROMTITLE', 1, '郵件標題', 5, '', '請填寫郵件標題', 1433921395, 1443004365, 1, '愛樂琴行', 0),
(47, 'MAIL_FROMU', 1, '接收人郵箱', 5, '', '請填寫接收人郵箱', 1433921406, 1443004379, 1, '794025348@qq.com', 0),
(48, 'MAIL_CONTENT', 2, '內容', 5, '', '請填寫收到郵件時的提示內容', 1433921425, 1443004397, 1, '愛樂琴行', 0),
(49, 'THINK_WEBADD', 1, '網站地址', 1, '', '填寫域名地址', 1437199965, 1443004409, 1, 'http://www.lovemusic.center/', 0);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document`
--

CREATE TABLE IF NOT EXISTS `aiyue_document` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用戶ID',
  `name` char(40) NOT NULL COMMENT '名稱',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '標題',
  `category_id` int(10) unsigned NOT NULL COMMENT '所屬分類',
  `description` char(140) NOT NULL DEFAULT '' COMMENT '描述',
  `root` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '根節點',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所屬ID',
  `model_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容模型ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '內容類型',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '推薦位',
  `link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外鏈',
  `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面',
  `display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可見性',
  `deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '截至時間',
  `attach` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附件數量',
  `view` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '瀏覽量',
  `comment` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '評論數',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '擴展統計字段',
  `level` int(10) NOT NULL DEFAULT '0' COMMENT '優先級',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '數據狀態',
  `emails` varchar(255) NOT NULL COMMENT '電子郵箱',
  `detail` text NOT NULL COMMENT '內容',
  PRIMARY KEY (`id`),
  KEY `idx_category_status` (`category_id`,`status`),
  KEY `idx_status_type_pid` (`status`,`uid`,`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文檔模型基礎表' AUTO_INCREMENT=306 ;

--
-- 转存表中的数据 `aiyue_document`
--

INSERT INTO `aiyue_document` (`id`, `uid`, `name`, `title`, `category_id`, `description`, `root`, `pid`, `model_id`, `type`, `position`, `link_id`, `cover_id`, `display`, `deadline`, `attach`, `view`, `comment`, `extend`, `level`, `create_time`, `update_time`, `status`, `emails`, `detail`) VALUES
(2, 1, '', '學生守則', 47, '', 0, 0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1429695540, 1441960103, 1, '', '1.學生須愛護公物，如有損壞，照價賠償<br />\r\n2.禁止在琴室內飲食或塗鴉<br />\r\n3.學生須在第四堂課繳交下期學費<br />\r\n4.學生如需請假，至少提前一天，切勿臨時（當日）請假（特殊情況除外，如：身體不適）<br />\r\n5.學生臨時請假或無請假，該堂課照計，不予補課<br />\r\n6.學生上課須準時，遲到將不予補錢<br />\r\n7.凡繳交任何費用，一經獲簽發收據後，恕不退款<br />\r\n<div>\r\n	<br />\r\n</div>'),
(3, 1, '', '關於愛樂', 40, '', 0, 0, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1429698660, 1457611467, 1, '', '<p style="color:#333333;text-indent:2em;font-family:SimHei, Arial;font-size:15px;background-color:#FFFFFF;">\r\n	<span style="font-family:SimSun;">音樂是造物主所創造、賞賜人類最美的語言。美妙的音樂和旋律，把不同文化、種族的人們，心靈連結交融於真善美的大愛中，讓生命回歸到被創造源頭的那份純淨、美麗。</span> \r\n</p>\r\n<p style="color:#333333;text-indent:2em;font-family:SimHei, Arial;font-size:15px;background-color:#FFFFFF;">\r\n	<span style="font-family:SimSun;">鼓勵孩子盡早接受音樂的啟蒙，是益智、啟智的最佳方法。純正有益的音樂熏陶、左右手的運用練習，能訓練刺激左右大腦的功能發揮，培育孩子在身心靈方面發展平衡，更加出類拔萃、貢獻社會。成年人學習音樂同洋可幫助我們的人格發展，洗滌練淨被世俗沾染的心靈，培養優雅的氣質與內涵，讓生命與美妙的音符壹起飛揚！</span> \r\n</p>\r\n<p style="color:#333333;text-indent:2em;font-family:SimHei, Arial;font-size:15px;background-color:#FFFFFF;">\r\n	<span style="font-family:SimSun;">衷心為每壹位朋友祈禱，在音樂領域裡更深認識生命真諦，蒙恩蒙福！</span> \r\n</p>\r\n<div>\r\n	<img alt="" src="/Uploads/Editor/2016-03-10/56e162c8e5f2f.jpg" /><br />\r\n</div>'),
(67, 1, '', '愛樂琴行', 61, '', 0, 0, 8, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431482940, 1456677295, 1, '', ''),
(68, 1, '', '新愛樂培訓中心', 61, '', 0, 0, 8, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431483000, 1444887056, 1, '', ''),
(65, 1, '嘉應店', '筷子基店', 60, '', 0, 0, 12, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 4, 1431153300, 1457597118, 1, '', '<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>'),
(66, 1, '愛樂琴行', '黑沙環店', 60, '', 0, 0, 12, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 1431153360, 1456726670, 1, '', ''),
(116, 1, '', '琴房', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432520100, 1457595578, 1, '', ''),
(115, 1, '', '鼓房', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432373160, 1458202883, 1, '', ''),
(112, 1, '', 'Band房', 73, '', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432371120, 1432800931, 1, '', ''),
(228, 1, '', '其他服務', 78, '', 0, 0, 18, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1439864520, 1458201496, 1, '', '<div style="text-align:center;">\r\n	<img alt="" src="/Uploads/Editor/2016-03-17/56ea638c4060d.jpg" />\r\n</div>'),
(98, 1, '', '鋼琴', 52, '', 0, 0, 19, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432281540, 1457598176, -1, '', ''),
(113, 1, '', '鼓房', 73, '', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432371180, 1432800894, 1, '', ''),
(99, 1, '', '樂器及精品', 52, '', 0, 0, 19, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432281540, 1457598185, 1, '', ''),
(64, 1, '激成店-Modern Residence', '激成店', 60, '', 0, 0, 12, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 3, 1431153300, 1444886070, -1, '', ''),
(117, 1, '', 'logo', 49, '', 0, 0, 8, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432535880, 1432539617, 1, '', ''),
(114, 1, '', '琴房', 73, '', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432371240, 1441959572, 1, '', ''),
(89, 1, '', '新馬路店', 46, '', 0, 0, 16, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431920220, 1458202038, 1, '', ''),
(63, 1, '凱旋店-Modern Residence', '新馬路店', 60, '', 0, 0, 12, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1431152520, 1457597490, 1, '', ''),
(139, 1, '', '新馬路店', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437536280, 1458202972, 1, 'lovemusic.victory@gmail.com', ''),
(138, 1, '', '激成店', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437536220, 1457867331, -1, '425911677@qq.com', ''),
(137, 1, '', '黑沙環店', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437536220, 1457867364, 1, 'dasmile37@163.com', ''),
(74, 1, '', '服務內容', 61, '', 0, 0, 7, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431483060, 1457611098, 1, '', ''),
(76, 1, '', '音樂課程', 58, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431496920, 1458541096, 1, '', ''),
(78, 1, '', '樂器教學', 58, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431497040, 1458541119, 1, '', ''),
(79, 1, '', '樂器及精品', 58, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431497160, 1458539474, 1, '', '1323223'),
(80, 1, '', '導師介紹', 50, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431503340, 1456677986, 1, '', '<p style="text-align:center;">\r\n	<span style="font-size:13.3333px;"></span>\r\n</p>'),
(275, 1, '', '各類音樂課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1444889580, 1457866174, 1, '', '<p>\r\n	<strong><u>愛樂音樂啟蒙班</u></strong>\r\n</p>\r\n<p>\r\n	幼兒音樂啟蒙班課程開跑~<br />\r\n年齡:3~4歲<br />\r\n時間:逢周日4~5pm (可免費試堂)<br />\r\n課程大綱:透過遊戲方式學習唱歌、節奏、認識樂器、聽力訓練、音準、音樂欣賞。<br />\r\n共二期，每期十二課，課程後提供一條龍銜接鋼琴，小提琴或敲擊樂課程。<span class="text_exposed_show"><br />\r\n預約試堂:28971051</span>\r\n</p>\r\n<p>\r\n	<img alt="" src="/Uploads/Editor/2016-03-13/56e54473ab1d5.jpg" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<strong><u>Abrsm樂理及視唱練耳課程</u></strong>\r\n</p>\r\n<p>\r\n	專業經驗教師培訓一至八級樂理及視唱練耳,保送英國皇家音樂考試。\r\n</p>\r\n<p>\r\n	可選擇團體班或個人班，上課時間可就。\r\n</p>\r\n<p>\r\n	<img alt="" src="/Uploads/Editor/2016-03-13/56e54589c7ea7.jpg" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>'),
(81, 1, '', 'Abrsm視唱練耳(Aural)課程', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431505260, 1458294129, 1, '', ''),
(82, 1, '', 'Abrsm樂理課程', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431508680, 1458294115, 1, '', ''),
(83, 1, '', '導師介紹', 51, '', 0, 0, 9, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431512940, 1441958064, 1, '', '<p style="text-align:center;">\r\n	姓名：趙玲 &nbsp; &nbsp;性別：女 &nbsp; &nbsp; 職稱：教授 &nbsp; &nbsp;學院：音樂學院\r\n</p>\r\n<p style="text-align:center;">\r\n	&nbsp; &nbsp;從事音樂教育事業十多年，主要從事聲樂、音樂理論的教學與研究。曾成功\r\n</p>\r\n<p style="text-align:center;">\r\n	舉辦“個人獨唱”音樂會。&nbsp;培養學生杜靜、張潔心等參加全國及省市各類聲樂大\r\n</p>'),
(85, 1, '', '小提琴個別班', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431568500, 1458212618, 1, '', ''),
(87, 1, '', 'Band房', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1431585000, 1457595592, 1, '', ''),
(122, 1, '', '琴房', 53, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432797840, 1457594805, 1, '', ''),
(123, 1, '', '鼓房', 53, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432797960, 1457594761, 1, '', ''),
(124, 1, '', 'band房', 53, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432797960, 1457594586, 1, '', ''),
(125, 1, '', '多功能音樂室', 53, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432798020, 1457594487, 1, '', ''),
(128, 1, '', '黑沙環店', 46, '', 0, 0, 16, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432803480, 1458201936, 1, '', ''),
(129, 1, '', '筷子基店(總店)', 46, '', 0, 0, 16, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1432803960, 1458201852, 1, '', ''),
(131, 1, '', '背景圖', 61, '', 0, 0, 22, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437377400, 1441955409, 1, '', ''),
(189, 1, '', '古典鋼琴課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437806940, 1456739374, -1, '', '<p style="text-align:center;">\r\n	<br />\r\n</p>\r\n<p>\r\n	完整的學習系統，配合英國皇家音樂學院的考級制度，一對一的鋼琴個別授課，專業耐心的師資，學習時間彈性，並設有完整的配套課程(視唱練耳、一至八級樂理課程等)。\r\n</p>\r\n<p>\r\n	不限年齡，歡迎查詢。\r\n</p>'),
(190, 1, '', '課程簡介', 76, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1437807000, 1458212094, 1, '', '<p style="text-align:center;">\r\n	<p>\r\n		兒童鋼琴啓蒙課程：年齡4～6歲。故事形式引入，從小培養小朋友興趣，發展協調能力，師資保證，歡迎查詢。\r\n	</p>\r\n	<p>\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<img alt="" src="/Uploads/Editor/2016-03-17/56ea8683e1721.jpg" /> \r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		古典鋼琴課程:\r\n	</p>\r\n	<p style="text-align:center;">\r\n		專業耐心師資，適合各年齡層人士。訓練多種音樂能力，學習多樣著名的曲目及依循完整的教學步驟。\r\n	</p>\r\n	<p style="text-align:center;">\r\n		保送英國皇家考試，課程顧問更為您安排提供聽力及樂理課程。\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<img alt="" src="/Uploads/Editor/2016-03-17/56ea884952cde.jpg" /> \r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<span class="fbPhotosPhotoCaption" id="fbPhotoSnowliftCaption"><span class="hasCaption"><span>流行鋼琴課程：自彈自唱、流行樂曲、專長訓練、樂隊夾b</span><span class="word_break"></span>and、爵士藍調</span></span>\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<img alt="" src="/Uploads/Editor/2016-03-17/56ea8cf6e1ca8.jpg" />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		<br />\r\n	</p>\r\n	<p style="text-align:center;">\r\n		爵士鼓課程:從現代音樂(Contemporary Music)中體驗各種音樂風格，各式各樣的節奏型態;\r\n	</p>\r\n	<p style="text-align:center;">\r\n		廣泛地學習音樂，培養表現音樂的能力。鼓棒的掌握，手腳全人的協調，基礎的節奏訓練，\r\n	</p>\r\n	<p style="text-align:center;">\r\n		課堂由淺至深，專業耐心的師資，學習節奏音樂也可以很輕鬆有趣。歡迎喜愛鼓音色的大小\r\n	</p>\r\n	<p>\r\n		<div style="text-align:center;">\r\n			<span style="line-height:1.5;">朋友們。上課時間可就。另設有非洲鼓，木箱鼓課程。</span> \r\n		</div>\r\n		<p style="text-align:center;">\r\n			<span style="line-height:1.5;"><br />\r\n</span> \r\n		</p>\r\n		<p style="text-align:center;">\r\n			<span style="line-height:1.5;"><img alt="" src="/Uploads/Editor/2016-03-17/56ea69efcfe9e.jpg" /></span> \r\n		</p>\r\n		<p style="text-align:center;">\r\n			<span style="line-height:1.5;"><br />\r\n</span> \r\n		</p>\r\n<span style="line-height:1.5;"> \r\n		<p align="center" style="text-align:center;">\r\n			<br />\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			<br />\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			<br />\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			弦樂課程:非常受大小朋友歡迎的弦樂課程，\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			學習步驟完整，注重基本姿勢養成，學習曲風格多元，\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			師資優良耐心，提高學習動機和趣味，保送英國皇家音樂考試。\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			我們有專業課程顧問提供聽力、伴奏及樂理訓練一條龍課程安排服務。\r\n		</p>\r\n		<p align="center" style="text-align:center;">\r\n			歡迎喜愛弦樂的朋友們查詢，上課時間可就。\r\n		</p>\r\n</span> \r\n		<p style="text-align:center;">\r\n			<span style="line-height:1.5;"><img alt="" src="/Uploads/Editor/2016-03-17/56ea6ac803418.jpg" /></span> \r\n		</p>\r\n		<p style="text-align:center;">\r\n			<br />\r\n		</p>\r\n		<p style="text-align:center;">\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				木結他(古典/民謠)課程:從木吉他愉快的學習到多樣的演奏技巧及和絃的知識。\r\n			</p>\r\n			<p align="center">\r\n				以歌曲的伴奏為基礎，搭配和絃的基本伴奏並運用，充分發揮木吉他的音色魅力。\r\n			</p>\r\n			<p align="center">\r\n				課程對象:喜愛木結他音色的大家。上課時間可就。歡迎查詢。\r\n			</p>\r\n			<p>\r\n				<br />\r\n			</p>\r\n			<p style="text-align:center;">\r\n				<span style="line-height:1.5;"><img alt="" src="/Uploads/Editor/2016-03-17/56ea85109df5a.jpg" /><br />\r\n</span> \r\n			</p>\r\n			<p style="text-align:center;">\r\n				<span style="line-height:1.5;"></span> \r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				古箏課程:循序漸進學習古箏彈奏技巧，樂理，指法，歷史背景，並學習多首著名經典古曲，耐心專業師資，報送國際認可考試。\r\n			</p>\r\n			<p align="center">\r\n				<img alt="" src="/Uploads/Editor/2016-03-17/56ea8b0086aed.jpg" /> \r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				<br />\r\n			</p>\r\n			<p align="center">\r\n				薩克斯風課程: 專業認真的教學，完整的教學步驟，深入淺出的教程設計，讓學生接觸多種曲風，學習吹奏經典曲目。\r\n			</p>\r\n			<p align="center">\r\n				<img alt="" src="/Uploads/Editor/2016-03-17/56ea8c2ada68b.png" /> \r\n			</p>'),
(196, 1, '', '課程預約', 58, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1438833840, 1458541134, 1, '', '123'),
(197, 1, '', '音樂室租練', 58, '', 0, 0, 13, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1438840500, 1457611023, 1, '', ''),
(233, 1, '', '課程簡介', 80, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1439866800, 1441959269, 1, '', '<p style="text-align:center;">\r\n	爵士鼓課程:從現代音樂(Contemporary Music)中體驗各種音樂風格，各式各樣的節奏型態;\r\n</p>\r\n<p style="text-align:center;">\r\n	廣泛地學習音樂，培養表現音樂的能力。鼓棒的掌握，手腳全人的協調，基礎的節奏訓練，\r\n</p>\r\n<p style="text-align:center;">\r\n	課堂由淺至深，專業耐心的師資，學習節奏音樂也可以很輕鬆有趣。歡迎喜愛鼓音色的大小\r\n</p>\r\n<p style="text-align:center;">\r\n	朋友們。上課時間可就。另設有非洲鼓，木箱鼓課程。\r\n</p>\r\n<div style="text-align:center;">\r\n	<span style="line-height:1.5;">查詢:28971051</span>\r\n</div>\r\n<div style="text-align:center;">\r\n	<span style="line-height:1.5;">Drum Lessons. Enquiry: 28971051</span>\r\n</div>\r\n<div style="text-align:center;">\r\n	<br />\r\n</div>'),
(231, 1, '', '愛樂琴行', 66, '', 0, 0, 30, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1439865887, 1439865887, 1, '', ''),
(232, 1, '', '課程簡介', 79, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1439866800, 1457598089, -1, '', '<p style="text-align:center;">\r\n	爵士鼓課程:從現代音樂(Contemporary Music)中體驗各種音樂風格，各式各樣的節奏型態;\r\n</p>\r\n<p style="text-align:center;">\r\n	廣泛地學習音樂，培養表現音樂的能力。鼓棒的掌握，手腳全人的協調，基礎的節奏訓練，\r\n</p>\r\n<p style="text-align:center;">\r\n	課堂由淺至深，專業耐心的師資，學習節奏音樂也可以很輕鬆有趣。歡迎喜愛鼓音色的大小\r\n</p>\r\n<p style="text-align:center;">\r\n	朋友們。上課時間可就。另設有非洲鼓，木箱鼓課程。\r\n</p>\r\n<div style="text-align:center;">\r\n	<span style="line-height:1.5;">查詢:28971051</span>\r\n</div>\r\n<div style="text-align:center;">\r\n	<span style="line-height:1.5;">Drum Lessons. Enquiry: 28971051</span>\r\n</div>'),
(227, 1, '', 'ABRSM考試收費', 45, '', 0, 0, 4, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1439469600, 1457613010, 1, '', '<img alt="" src="/Uploads/Editor/2016-03-10/56e1674e35ea8.jpg" /><img alt="" src="/Uploads/Editor/2016-03-10/56e1674f697aa.jpg" /> \r\n<p style="text-align:center;">\r\n	<span style="line-height:1.5;"><br />\r\n</span> \r\n</p>\r\n<p style="text-align:center;">\r\n	<br />\r\n</p>'),
(243, 1, '', '音樂室租用', 59, '', 0, 0, 23, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441955520, 1456729529, 1, '', '<p>\r\n	<span style="color:#141823;font-family:helvetica, arial, sans-serif;font-size:14px;background-color:#FFFFFF;">龍嵩街音樂室長短期租用:琴室/Band房/大型排練室(適合choir,大師班,工作坊)/</span>\r\n</p>\r\n<p>\r\n	<span style="color:#141823;font-family:helvetica, arial, sans-serif;font-size:14px;background-color:#FFFFFF;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 三角琴室</span> \r\n</p>\r\n<p>\r\n	<span style="color:#141823;font-family:helvetica, arial, sans-serif;font-size:14px;background-color:#FFFFFF;">預約電話:28971051</span> \r\n</p>'),
(244, 1, '', '愛樂音樂教育中心招生中!', 59, '', 0, 0, 23, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441955580, 1456803279, 1, '', '<p>\r\n	<br />\r\n</p>'),
(274, 1, '', '小提琴課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1444889526, 1456739374, -1, '', '非常受大小朋友歡迎的小提琴課程，學習步驟完整，注重基本姿勢養成，學習曲風格多元，師資優良耐心，提高學習動機和趣味，保送英國皇家音樂考試。歡迎喜愛小提琴的朋友們查詢，上課時間可就。<br />'),
(273, 1, '', '流行鋼琴課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1444889311, 1456739374, -1, '', '<p>\r\n	-左右手協調能力及耐力訓練\r\n</p>\r\n<p>\r\n	-認識現代音樂各種特色\r\n</p>\r\n<p>\r\n	-學習不同音樂種類的伴奏形式\r\n</p>\r\n<p>\r\n	-與樂團的配搭\r\n</p>\r\n<p>\r\n	-自彈自唱能力\r\n</p>\r\n<p>\r\n	-配置和弦\r\n</p>\r\n<p>\r\n	-樂理知識\r\n</p>\r\n<p>\r\n	-聽力訓練\r\n</p>\r\n<p>\r\n	-可保送Rockschool 考試\r\n</p>'),
(272, 1, '', '兒童鋼琴啟蒙課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1444887780, 1456739374, -1, '', '<p>\r\n	兒童鋼琴啓蒙課程：適合年齡4～12歲。故事形式引入，從小培養小朋友興趣，發展協調能力，定期舉辦兒童鋼琴發表會，提升學生表演能力和學習動力，優良耐心師資保證。\r\n</p>'),
(245, 1, '', '筷子基店', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957080, 1458202996, 1, 'lovemusic.victory@gmail.com', ''),
(246, 1, '', '龍崇店1-3級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957257, 1441957257, 1, '', ''),
(247, 1, '', '龍崇店4-5級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957286, 1441957286, 1, '', ''),
(248, 1, '', '龍崇店6級', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957260, 1441957436, 1, '', ''),
(249, 1, '', '龍崇店7-8級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957331, 1441957331, 1, '', ''),
(250, 1, '', '激成店1-3級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957366, 1441957366, 1, '', ''),
(251, 1, '', '激成店1-3級2堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957389, 1441957389, 1, '', ''),
(252, 1, '', '龍崇店1-3級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957599, 1441957599, 1, '', ''),
(253, 1, '', '激成店1-3級1堂', 67, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441957636, 1441957636, 1, '', ''),
(262, 1, '', '龍崇店1人班1-3級', 70, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958295, 1441958295, 1, '', ''),
(263, 1, '', '龍崇店多人班1-3級', 70, '', 0, 0, 17, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958326, 1441958326, 1, '', ''),
(264, 1, '', '夏威夷小結他', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958460, 1457598735, 1, '', '各式夏威夷小結他,專業老師推介, 附有夏威夷小結他課程，詳情請親臨本店或聯絡我們。<br />'),
(265, 1, '', '小提琴', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958460, 1457598697, 1, '', '各尺寸及品牌小提琴，買琴送琴盒，保養及松香。報名小提琴課程可打折，詳情請親臨本店或聯絡我們。'),
(266, 1, '', '結他', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958520, 1457598905, 1, '', '各品牌木結他及電結他,附有結他課程，詳情請親臨本店或聯絡我們。'),
(267, 1, '', '音樂精品', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958520, 1457604605, 1, '', '各式音樂精品文具種類繁多,送禮自用兩相宜。音樂銀飾現正50% off,歡迎親臨挑選。'),
(268, 1, '', '鋼琴1', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958580, 1457610338, -1, '', ''),
(269, 1, '', '鋼琴2', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958610, 1457610338, -1, '', ''),
(270, 1, '', '鋼琴3', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441958631, 1457610338, -1, '', ''),
(271, 1, '', '鋼琴4', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1441959022, 1457610338, -1, '', ''),
(276, 1, '', '11111111', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456678166, 1456678200, -1, '', ''),
(277, 1, '', '1111', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456678318, 1456678358, -1, '', ''),
(278, 1, '', '1111', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456678755, 1456678817, -1, '1111111111@qq.com', ''),
(279, 1, '', '1111', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456679171, 1456680484, -1, '', '111111111111'),
(280, 1, '', '2222', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456680329, 1456680484, -1, '', ''),
(281, 1, '', '333', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456680370, 1456680484, -1, '', ''),
(282, 1, '', '444', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456680399, 1456680484, -1, '', ''),
(283, 1, '', '555', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456680412, 1456680484, -1, '', ''),
(284, 1, '', '111', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456680995, 1456681038, -1, '', ''),
(285, 1, '', '1111', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456681172, 1456681189, -1, '', ''),
(286, 1, '', '1111', 78, '', 0, 0, 18, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456681697, 1456681714, -1, '', '22222'),
(287, 1, '', '11', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1456727040, 1456727179, -1, '', ''),
(288, 1, '', '三角琴房', 73, '', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457595900, 1457595956, 1, '', ''),
(289, 1, '', '三角琴房', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457595960, 1457596445, 1, '', ''),
(290, 1, '', '多功能音樂室', 64, '', 0, 0, 15, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457596620, 1457596737, 1, '', ''),
(291, 1, '', '多功能音樂室', 73, '', 0, 0, 6, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457596620, 1457596682, 1, '', ''),
(292, 1, '', '三角琴房', 53, '', 0, 0, 5, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457596860, 1457612034, 1, '', ''),
(293, 1, '', '鋼琴', 71, '', 0, 0, 20, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457610859, 1457610859, 1, '', '<p>\r\n	代理Yamaha,Kawai 多個著名品牌二手/翻新鋼琴，價格貨品選擇良多，所有鋼琴由二十多年經驗徐詠師傅親自挑選和保養, 售後服務完善，讓您無後顧之憂。詳情請親臨店鋪或聯絡我們。\r\n</p>'),
(295, 1, '', '小提琴課程', 75, '', 0, 0, 25, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457865542, 1457865676, -1, '', '<p>\r\n	小提琴課程:非常受大小朋友歡迎的小提琴課程，學習步驟完整，注重基本姿勢養成，學習曲風格多元，師資優良耐心，提高學習動機和趣味，保送英國皇家音樂考試。歡迎喜愛小提琴的朋友們查詢，上課時間可就。\r\n</p>'),
(294, 1, '', '預約考琴', 45, '', 0, 0, 4, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457612700, 1457612958, 1, '', '<p style="text-align:center;">\r\n	預約考試琴步驟：\r\n</p>\r\n<div style="text-align:center;">\r\n	Step 1：致電考場（TEL:28410183)，話比我地你想試琴的具體日期及時段\r\n</div>\r\n<div style="text-align:center;">\r\n	Step 2：留低簡單的個人信息（如：考生姓名及聯繫電話）\r\n</div>\r\n<div style="text-align:center;">\r\n	Step 3：試琴當日繳納費用\r\n</div>\r\n<div style="text-align:center;">\r\n	就甘簡單！！！\r\n</div>\r\n<div style="text-align:center;">\r\n	<br />\r\n</div>\r\n<div style="text-align:center;">\r\n	~每次至少半小時，預約次數不設上限\r\n</div>\r\n<p style="text-align:center;">\r\n	~已完成預約，但無出席者，下次預約不作優先處理\r\n</p>\r\n<p style="text-align:center;">\r\n	<br />\r\n</p>\r\n<p style="text-align:center;">\r\n	<img alt="" src="/Uploads/Editor/2016-03-10/56e168992bf35.jpg" />\r\n</p>'),
(296, 1, '', '愛樂音樂啟蒙班', 62, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1457866740, 1458293871, 1, '', ''),
(297, 1, '', '大提琴，中提琴個別班', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458212640, 1458212753, 1, '', ''),
(298, 1, '', '薩克斯風個別班', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458212880, 1458212967, 1, '', ''),
(299, 1, '', '木結他課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213060, 1458213217, 1, '', ''),
(300, 1, '', '爵士鼓課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213174, 1458213174, 1, '', ''),
(301, 1, '', '古箏課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213180, 1458213251, 1, '', ''),
(302, 1, '', '烏克麗麗課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213360, 1458213416, 1, '', ''),
(303, 1, '', '流行鋼琴課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213457, 1458213457, 1, '', ''),
(304, 1, '', '古典鋼琴課程', 63, '', 0, 0, 14, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458213480, 1458305332, 1, '', ''),
(305, 1, '', '111', 74, '', 0, 0, 24, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1458293815, 1458293848, -1, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_abrsm`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_abrsm` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pic` varchar(255) NOT NULL COMMENT '上傳圖片',
  `content` text NOT NULL COMMENT '課程簡介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_ad`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pic` varchar(255) NOT NULL COMMENT '上傳圖片',
  `url` varchar(255) NOT NULL COMMENT '鏈接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=245 ;

--
-- 转存表中的数据 `aiyue_document_ad`
--

INSERT INTO `aiyue_document_ad` (`id`, `pic`, `url`) VALUES
(243, '139', ''),
(244, '144', 'http://127.0.0.1/project/aiyue/Home/Index/teach_music.html');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_article`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_article` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `show` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型文章表';

--
-- 转存表中的数据 `aiyue_document_article`
--

INSERT INTO `aiyue_document_article` (`id`, `show`) VALUES
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_bg`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_bg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pic` varchar(255) NOT NULL COMMENT '背景圖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=132 ;

--
-- 转存表中的数据 `aiyue_document_bg`
--

INSERT INTO `aiyue_document_bg` (`id`, `pic`) VALUES
(131, '93');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_characteristic`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_characteristic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `image` int(10) unsigned NOT NULL COMMENT '圖片',
  `content` text NOT NULL COMMENT '介紹',
  `link` varchar(255) NOT NULL COMMENT '鏈接',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=198 ;

--
-- 转存表中的数据 `aiyue_document_characteristic`
--

INSERT INTO `aiyue_document_characteristic` (`id`, `image`, `content`, `link`) VALUES
(76, 169, '租練琴房、鼓房、貝司房、吉他房', 'http://www.lovemusic.center/Home/Index/rent.html'),
(78, 168, '鍵盤類、弦樂類、敲擊類、特別類', 'http://www.lovemusic.center/Home/Index/teach_music.html'),
(79, 167, '各類品牌中西樂器銷售;音樂小精品零售', 'http://www.lovemusic.center/Home/Index/qin.html'),
(196, 151, '零基礎可學，優良師資，靈活約課。\r\n', 'http://www.lovemusic.center/Home/Index/teach_music.html'),
(197, 12, '琴房、鼓房、Band房、三角琴室、大型音樂室', 'http://www.lovemusic.center/Home/Index/rent.html');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_class`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `email` varchar(255) NOT NULL COMMENT '電子郵箱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=306 ;

--
-- 转存表中的数据 `aiyue_document_class`
--

INSERT INTO `aiyue_document_class` (`id`, `email`) VALUES
(137, '552023587@qq.com'),
(138, '552023587@qq.com'),
(139, '552023587@qq.com'),
(278, ''),
(245, ''),
(305, '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_config`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `copyright` varchar(255) NOT NULL COMMENT '版權信息',
  `facebook` varchar(255) NOT NULL COMMENT 'Facebook帳號',
  `youtube` varchar(255) NOT NULL COMMENT 'You Tube帳號',
  `description` text NOT NULL COMMENT '網站描述',
  `keyword` varchar(255) NOT NULL COMMENT '網站關鍵字',
  `GOOGLEANALYTICS` varchar(255) NOT NULL COMMENT 'GOOGLE ANALYTICS設置',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=232 ;

--
-- 转存表中的数据 `aiyue_document_config`
--

INSERT INTO `aiyue_document_config` (`id`, `copyright`, `facebook`, `youtube`, `description`, `keyword`, `GOOGLEANALYTICS`) VALUES
(231, 'Copyright © 2015 愛樂琴行', '', '', '', '愛樂琴行', '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_contact`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `phone` varchar(255) NOT NULL COMMENT '電話',
  `Email` varchar(255) NOT NULL COMMENT 'E－mail',
  `images` varchar(255) NOT NULL COMMENT '圖片',
  `map` varchar(255) NOT NULL COMMENT '坐標',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=130 ;

--
-- 转存表中的数据 `aiyue_document_contact`
--

INSERT INTO `aiyue_document_contact` (`id`, `address`, `phone`, `Email`, `images`, `map`) VALUES
(89, '龍嵩正街10號凱旋門廣場地下L舖', '28971051', 'lovemusic.victory@gmail.com', '154', '22.2105411443,113.5440065891'),
(128, '黑沙環斜路29號信毅花園地下D舖', '28535312', 'sanoilok@yahoo.com.hk', '74', '22.2078561443,113.5512195891'),
(129, '和樂巷嘉應花園地下QZ舖', '28381608', 'lovemusic.victory@gmail.com', '140', '22.1920874664,113.5389897407');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_course`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `address` varchar(100) NOT NULL COMMENT '上課地點',
  `num` varchar(255) NOT NULL COMMENT '班級人數',
  `grade` varchar(255) NOT NULL COMMENT '級別',
  `Hallnum` varchar(255) NOT NULL COMMENT '堂數',
  `price` varchar(255) NOT NULL COMMENT '每堂價格',
  `time` varchar(255) NOT NULL COMMENT '時間',
  `note` varchar(255) NOT NULL COMMENT '備註',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=305 ;

--
-- 转存表中的数据 `aiyue_document_course`
--

INSERT INTO `aiyue_document_course` (`id`, `address`, `num`, `grade`, `Hallnum`, `price`, `time`, `note`) VALUES
(81, '新馬路店,黑沙環店,筷子基店', '個人班，團體班', '1-3級，4-5級，6級，7-8級', '', '1-3級550,4-5級600,6級700,7-8級850', '45分鐘/堂，60分鐘/堂', '學費以四堂課計算'),
(82, '新馬路店,黑沙環店,筷子基店', '個人班，團體班', '1-3級，4-5級，6級，7-8級', '', '1-3級700,4-5級800,6級950,7-8級1200', '45分鐘/堂課 , 60分鐘/堂課', '學費以四堂課計算'),
(85, '新馬路店,黑沙環店,筷子基店', '1人班', '初級，2級，3級，4級，5級，6級，7級，8級，', '', '', '45分鐘/堂 ', '個別授課（學費以四堂課計算）'),
(153, '龍崇店', '', '', '', '', '', ''),
(276, '龍崇店,激成店', '11,111,1111', '222,22,2', '4,44,444', '', '233,33', '5555'),
(277, '龍崇店,激成店', '111', '111', '1111', '', '111', '1111'),
(287, '龍崇店,嘉應店,新益店', '11', '11', '11', '', '11', '11'),
(296, '新馬路店,黑沙環店,筷子基店', '團體班', '第一期 ,第二期, 第三期', '', '', '60分鐘/堂', '學費以四堂課計算。'),
(297, '黑沙環店,筷子基店', '1人班', '初級，2級，3級，4級，5級，6級，7級，8級，', '', '', '45分鐘/堂', '個別授課（學費以四堂課計算）'),
(298, '黑沙環店', '1人班', '一至五級，六級，七級，八級', '', '', '45分鐘/堂', '個別授課（學費以四堂課計算）'),
(299, '新馬路店,黑沙環店,筷子基店', '1人班，2人班，多人班', '初級，中級，高級', '', '', '45分鐘/堂', '（學費以四堂課計算）'),
(300, '新馬路店,黑沙環店,筷子基店', '1人班', '初級，中級，高級', '', '', '45分鐘/堂', '個別授課（學費以四堂課計算）'),
(301, '新馬路店,黑沙環店,筷子基店', '1人班', '初級，2級，3級，4級，5級，6級，7級，8級', '', '', '45分鐘/堂', '個別授課（學費以四堂課計算）'),
(302, '新馬路店,黑沙環店,筷子基店', '1人班，2人班，多人班', '初級，中級，高級', '', '', '45分鐘/堂', '（學費以四堂課計算）'),
(303, '新馬路店,黑沙環店,筷子基店', '1人班', '初級，中級，高級', '', '', '45分鐘/堂', '（學費以四堂課計算）'),
(304, '新馬路店,黑沙環店,筷子基店', '1人班', '初級,2級，3級，4級，5級，6級，7級，8級, Dip Abrsm', '', '', '45分鐘/堂', '個別授課（學費以四堂課計算）');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_courselist`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_courselist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `address` varchar(255) NOT NULL COMMENT '地點',
  `time` varchar(255) NOT NULL COMMENT '時間',
  `num` varchar(255) NOT NULL COMMENT '班級人數',
  `grade` varchar(255) NOT NULL COMMENT '級別',
  `Hallnum` varchar(255) NOT NULL COMMENT '堂數',
  `price` varchar(255) NOT NULL COMMENT '價格',
  `coursename` varchar(255) NOT NULL COMMENT '課程名稱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=264 ;

--
-- 转存表中的数据 `aiyue_document_courselist`
--

INSERT INTO `aiyue_document_courselist` (`id`, `address`, `time`, `num`, `grade`, `Hallnum`, `price`, `coursename`) VALUES
(246, '龍崇店', '45分鐘/堂課', '1人班', '1-3級', '1堂', '600', '82'),
(247, '龍崇店', '45分鐘/堂課', '1人班', '4-5級', '1堂', '800', '82'),
(248, '龍崇店', '45分鐘/堂課', '1人班', '6級', '1堂', '1000', '82'),
(249, '龍崇店', '45分鐘/堂課', '1人班', '7-8級', '1堂', '1200', '82'),
(250, '激成店', '45分鐘/堂課', '1人班', '1-3級', '1堂', '600', '82'),
(251, '激成店', '45分鐘/堂課', '1人班', '1-3級', '2堂', '700', '82'),
(252, '龍崇店', '60分鐘/堂', '多人班', '1-3級', '1堂', '450', '81'),
(253, '激成店', '60分鐘/堂', '多人班', '1-3級', '1堂', '500', '81'),
(262, '龍崇店', '45分鐘/堂', '1人班', '1-3級', '1堂', '500', '85'),
(263, '龍崇店', '45分鐘/堂', '多人班', '1-3級', '1堂', '400', '85');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_download`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_download` (
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文檔ID',
  `parse` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '內容解析類型',
  `content` text NOT NULL COMMENT '下載詳細描述',
  `template` varchar(100) NOT NULL DEFAULT '' COMMENT '詳情頁顯示模板',
  `file_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件ID',
  `download` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '下載次數',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文檔模型下載表';

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_exam`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_exam` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pic` varchar(255) NOT NULL COMMENT '考試安排表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=295 ;

--
-- 转存表中的数据 `aiyue_document_exam`
--

INSERT INTO `aiyue_document_exam` (`id`, `pic`) VALUES
(227, ''),
(294, '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_houseadd`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_houseadd` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `housetype` varchar(255) NOT NULL COMMENT '房間類型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_housetype`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_housetype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `houseName` varchar(255) NOT NULL COMMENT '房間名',
  `note` varchar(255) NOT NULL COMMENT '備註',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=292 ;

--
-- 转存表中的数据 `aiyue_document_housetype`
--

INSERT INTO `aiyue_document_housetype` (`id`, `houseName`, `note`) VALUES
(112, 'Band房', '*需提前電話預約'),
(113, '鼓房', '*需提前電話預約'),
(114, '琴房', '需提前電話預約'),
(288, '三角琴房', '*需提前電話預約'),
(291, '多功能音樂室', '*需提前電話預約');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_image`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `content` varchar(255) NOT NULL COMMENT '描述',
  `image` int(10) unsigned NOT NULL COMMENT '圖片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=293 ;

--
-- 转存表中的数据 `aiyue_document_image`
--

INSERT INTO `aiyue_document_image` (`id`, `content`, `image`) VALUES
(122, '直立式鋼琴，琴房附基本文具，空調', 150),
(123, '附drum set,音響,空調', 121),
(124, '附drum set, mic stand, PA system, 譜架, 結他bass amp,空調', 105),
(125, '附黑板，長桌，椅，三角琴，空調˙', 120),
(292, '英國皇家考試用琴', 166);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_instru`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_instru` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `instruName` varchar(255) NOT NULL COMMENT '樂器名稱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=100 ;

--
-- 转存表中的数据 `aiyue_document_instru`
--

INSERT INTO `aiyue_document_instru` (`id`, `instruName`) VALUES
(98, '鋼琴'),
(99, '樂器及精品');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_instruima`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_instruima` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `images` int(10) unsigned NOT NULL COMMENT '圖片',
  `instru` varchar(255) NOT NULL COMMENT '樂器名稱',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=294 ;

--
-- 转存表中的数据 `aiyue_document_instruima`
--

INSERT INTO `aiyue_document_instruima` (`id`, `images`, `instru`) VALUES
(282, 129, '99'),
(279, 127, '99'),
(280, 127, '99'),
(281, 128, '99'),
(264, 161, '99'),
(265, 160, '99'),
(266, 162, '99'),
(267, 163, '99'),
(268, 116, '98'),
(269, 117, '98'),
(270, 118, '98'),
(271, 119, '98'),
(283, 130, '99'),
(293, 164, '99');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_intro`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_intro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `sid` varchar(255) NOT NULL COMMENT '標識',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=296 ;

--
-- 转存表中的数据 `aiyue_document_intro`
--

INSERT INTO `aiyue_document_intro` (`id`, `sid`) VALUES
(189, ''),
(190, ''),
(232, ''),
(233, ''),
(272, ''),
(273, ''),
(274, ''),
(275, ''),
(295, '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_lists`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `content` text NOT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=119 ;

--
-- 转存表中的数据 `aiyue_document_lists`
--

INSERT INTO `aiyue_document_lists` (`id`, `content`) VALUES
(74, '提供英國皇家考試考場|保送英國皇家音樂考試|各類中西樂器批發及零售|各類音樂書籍批發及零售|各類樂器配件批發及零售|專業鋼琴調音，維修，搬運|場地租借');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_logo`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_logo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `image` int(10) unsigned NOT NULL COMMENT 'logo圖片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=118 ;

--
-- 转存表中的数据 `aiyue_document_logo`
--

INSERT INTO `aiyue_document_logo` (`id`, `image`) VALUES
(117, 4),
(67, 6),
(68, 7);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_rent`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_rent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `address` varchar(255) NOT NULL COMMENT '上課地點',
  `vipprice` varchar(255) NOT NULL COMMENT '會員價',
  `price` varchar(255) NOT NULL COMMENT '非會員價',
  `house` varchar(255) NOT NULL COMMENT '房間類型',
  `tel` varchar(255) NOT NULL COMMENT '电话',
  `add` varchar(255) NOT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=291 ;

--
-- 转存表中的数据 `aiyue_document_rent`
--

INSERT INTO `aiyue_document_rent` (`id`, `address`, `vipprice`, `price`, `house`, `tel`, `add`) VALUES
(87, '澳門龍崇街凱旋門廣場地下L鋪', '', '$80/小時；$130/2小時', '112', '28381608(筷子基),28535312(黑沙環),28971051(新馬路)', '筷子基店:澳門和樂巷嘉應花園地下Q、Z鋪/黑沙環店:黑沙環斜路29號信毅花園地下D鋪/新馬路店:龍嵩正街10號凱旋門廣場地舖'),
(115, '', '', '$40/小時;', '113', '28381608(筷子基),28535312(黑沙環),28971051(新馬路)', '筷子基店:澳門和樂巷嘉應花園地下Q、Z鋪/黑沙環店:黑沙環斜路29號信毅花園地下D鋪/新馬路店:龍嵩正街10號凱旋門廣場地舖 '),
(116, '', '', '$38/小時 ;340/10小時(開琴卡)', '114', '28381608(筷子基),28535312(黑沙環),28971051(新馬路)', '筷子基店:澳門和樂巷嘉應花園地下Q、Z鋪/黑沙環店:黑沙環斜路29號信毅花園地下D鋪/新馬路店:龍嵩正街10號凱旋門廣場地舖'),
(284, '1111', '2222', '3333', '114', '44', '55'),
(285, '2222', '333', '', '114', '', ''),
(289, '', '', '$50/一小時(一般三角琴房);$120/半小時(英國皇家考試專用琴房)', '288', '28381608(筷子基),28535312(黑沙環),28971051(新馬路)', '筷子基店:澳門和樂巷嘉應花園地下Q、Z鋪/新馬路店:龍嵩正街10號凱旋門廣場地舖 '),
(290, '', '', '$50/一小時;', '291', '28971051(新馬路店)', '新馬路店:龍嵩正街10號凱旋門廣場地舖 ');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_service`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_service` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `sevice` varchar(255) NOT NULL COMMENT '服務內容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=287 ;

--
-- 转存表中的数据 `aiyue_document_service`
--

INSERT INTO `aiyue_document_service` (`id`, `sevice`) VALUES
(228, ''),
(286, '');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_store`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_store` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `content` text NOT NULL COMMENT '詳情介紹',
  `images` varchar(255) NOT NULL COMMENT '圖片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=67 ;

--
-- 转存表中的数据 `aiyue_document_store`
--

INSERT INTO `aiyue_document_store` (`id`, `content`, `images`) VALUES
(63, '地址：澳門龍嵩街10號凱旋門廣場地下L鋪(新馬路Pizza Hut 旁斜巷上)\r\n電話：28971051', '153,139,154'),
(64, '地址：澳門激成工業中心第三期6樓W\r\n電話：28410183', '89,90,87,91'),
(65, '地址：澳門和樂巷嘉應花園地下Q、Z鋪\r\n電話：28381608', '140,141'),
(66, '中心地址：澳門黑沙環斜路29號信毅花園地下D鋪（聖保祿學校正對面）\r\n電話：28535312\r\n手機：66309549\r\nE-mail:sanoilok@yahoo.com.hk', '132');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_document_teacher`
--

CREATE TABLE IF NOT EXISTS `aiyue_document_teacher` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `pic` varchar(255) NOT NULL COMMENT '背景圖',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=84 ;

--
-- 转存表中的数据 `aiyue_document_teacher`
--

INSERT INTO `aiyue_document_teacher` (`id`, `pic`) VALUES
(80, ''),
(83, '111');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_fback`
--

CREATE TABLE IF NOT EXISTS `aiyue_fback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(255) NOT NULL COMMENT '姓名',
  `phone` varchar(255) NOT NULL COMMENT '電話',
  `email` varchar(255) NOT NULL COMMENT '郵箱',
  `content` text NOT NULL COMMENT '內容',
  `time` int(10) unsigned NOT NULL COMMENT '時間',
  `read_status` varchar(255) DEFAULT '未讀',
  `status` int(10) unsigned NOT NULL COMMENT '狀態',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=37 ;

--
-- 转存表中的数据 `aiyue_fback`
--

INSERT INTO `aiyue_fback` (`id`, `name`, `phone`, `email`, `content`, `time`, `read_status`, `status`) VALUES
(30, '周先生', '135641231', '552023587@qq.com', '我需要質詢個事', 1439470623, '已讀', 1),
(31, '梁先生', '135646123', '45644@qq.com', '124564', 1439470856, '已讀', 1),
(32, '46546', '455465', '45456', '456\r\n', 1441941233, '已讀', 1),
(33, 'fds', 'fds', 'fdsfds', 'fdfsd', 1441944277, '已讀', 1),
(34, 'mark357177@hotmail.com', '63693952616', 'mark357177@hotmail.com', 'G6M1cF http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 1454551003, '已讀', 1),
(35, 'mark357177@hotmail.com', '30061519594', 'mark357177@hotmail.com', 'ilzb8q http://www.FyLitCl7Pf7kjQdDUOLQOuaxTXbj5iNG.com', 1454577906, '未讀', 1),
(36, '11', '22', '333', '4444', 1456682092, '已讀', 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_file`
--

CREATE TABLE IF NOT EXISTS `aiyue_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` char(20) NOT NULL DEFAULT '' COMMENT '保存名稱',
  `savepath` char(30) NOT NULL DEFAULT '' COMMENT '文件保存路徑',
  `ext` char(5) NOT NULL DEFAULT '' COMMENT '文件後綴',
  `mime` char(40) NOT NULL DEFAULT '' COMMENT '文件mime類型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` int(10) unsigned NOT NULL COMMENT '上傳時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_md5` (`md5`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文件表' AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `aiyue_file`
--

INSERT INTO `aiyue_file` (`id`, `name`, `savename`, `savepath`, `ext`, `mime`, `size`, `md5`, `sha1`, `location`, `create_time`) VALUES
(1, 'wifi密码.txt', '55c33ae2e85ad.txt', '2015-08-06/', 'txt', 'application/octet-stream', 540, 'b3528d044ecb50cfcb2d20c4cd705f88', 'd00130fb7bf50c727dde00774fff99f29dcfcab5', 0, 1438857954),
(2, '51bef0fb1ea89e46520ab9b8d91f8c', '55c4528e8bba8.jpg', '2015-08-07/', 'jpg', 'application/octet-stream', 20173, '19bbd7afc3f58c367d9f612fe836c44f', '16e3f5a31b9575bfcf51d794bacc4f73365fdc32', 0, 1438929550),
(3, 'aa.txt', '55dbde09121cf.txt', '2015-08-25/', 'txt', 'application/octet-stream', 18, 'ebbf154260dc95af148921da22c37845', 'c6133c17111206e379b0275e5fd0c670dc8118ba', 0, 1440472585),
(4, 'FTP.xlsx', '55f241ada4118.xlsx', '2015-09-11/', 'xlsx', 'application/octet-stream', 13488, '09e03fe79afdf4e2625ed986865f7769', '1af522100548334e7963ed3a5372b703b879bee1', 0, 1441939885),
(5, '爱乐测试表.xlsx', '55f2922bdb717.xlsx', '2015-09-11/', 'xlsx', 'application/vnd.openxmlformats-officedoc', 8840, 'c0b2724389c7e3e3e3f5228dfb5c250d', '6e8d7b0df4fc9356abd16a910f8236166f7811d1', 0, 1441960491),
(6, '網站驗收測試表.xlsx', '55f292a287590.xlsx', '2015-09-11/', 'xlsx', 'application/vnd.openxmlformats-officedoc', 13630, '689020d9b8ef0ec246a88cc7e20bde47', 'e5a8c2cfdae11b4cb14ea318aaa54fd5491d8e24', 0, 1441960609),
(7, '新建文本文档.txt', '55f62f85291af.txt', '2015-09-14/', 'txt', 'text/plain', 0, 'd41d8cd98f00b204e9800998ecf8427e', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 0, 1442197381),
(8, '新建文本文档.txt', '55f6385858114.txt', '2015-09-14/', 'txt', 'text/plain', 6, 'a4f0dc8026624beec687c6b74e5adc46', '4ca840d55d5c74517e2f409da9aa6c92f344737f', 0, 1442199640),
(9, 'excel公式记录.docx', '55f6444695d07.docx', '2015-09-14/', 'docx', 'application/vnd.openxmlformats-officedoc', 1773637, 'c89f81db1a643acc08f1d2c674077ac2', 'e4da57abf6a2b4617e0a6a16415f8bd890c6d033', 0, 1442202661);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_hooks`
--

CREATE TABLE IF NOT EXISTS `aiyue_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '鉤子名稱',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '類型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '鉤子掛載的插件 ''，''分割',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `aiyue_hooks`
--

INSERT INTO `aiyue_hooks` (`id`, `name`, `description`, `type`, `update_time`, `addons`) VALUES
(1, 'pageHeader', '頁面header鉤子，壹般用於加載插件CSS文件和代碼', 1, 0, ''),
(2, 'pageFooter', '頁面footer鉤子，壹般用於加載插件JS文件和JS代碼', 1, 0, 'ReturnTop'),
(3, 'documentEditForm', '添加編輯表單的 擴展內容鉤子', 1, 0, 'Attachment'),
(4, 'documentDetailAfter', '文檔末尾顯示', 1, 0, 'Attachment,SocialComment'),
(5, 'documentDetailBefore', '頁面內容前顯示用鉤子', 1, 0, ''),
(6, 'documentSaveComplete', '保存文檔數據後的擴展鉤子', 2, 0, 'Attachment'),
(7, 'documentEditFormContent', '添加編輯表單的內容顯示鉤子', 1, 0, 'Editor'),
(8, 'adminArticleEdit', '後臺內容編輯頁編輯器', 1, 1378982734, 'EditorForAdmin'),
(13, 'AdminIndex', '首頁小格子個性化顯示', 1, 1382596073, 'SiteStat,SystemInfo,DevTeam'),
(14, 'topicComment', '評論提交方式擴展鉤子。', 1, 1380163518, 'Editor'),
(16, 'app_begin', '應用開始', 2, 1384481614, ''),
(17, 'UploadImages', 'UploadImages', 1, 1431142391, 'UploadImages');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_member`
--

CREATE TABLE IF NOT EXISTS `aiyue_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `nickname` char(16) NOT NULL DEFAULT '' COMMENT '昵稱',
  `sex` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '性別',
  `birthday` date NOT NULL DEFAULT '0000-00-00' COMMENT '生日',
  `qq` char(10) NOT NULL DEFAULT '' COMMENT 'qq號',
  `score` mediumint(8) NOT NULL DEFAULT '0' COMMENT '用戶積分',
  `login` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登錄次數',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '會員狀態',
  PRIMARY KEY (`uid`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='會員表' AUTO_INCREMENT=28 ;

--
-- 转存表中的数据 `aiyue_member`
--

INSERT INTO `aiyue_member` (`uid`, `nickname`, `sex`, `birthday`, `qq`, `score`, `login`, `reg_ip`, `reg_time`, `last_login_ip`, `last_login_time`, `status`) VALUES
(1, 'admin', 0, '0000-00-00', '', 460, 176, 0, 1429084270, 242992661, 1458790659, 1),
(2, '123456', 0, '0000-00-00', '', 40, 12, 0, 0, 2130706433, 1432608272, 1),
(3, 'users', 0, '0000-00-00', '', 10, 10, 2130706433, 1431657736, 2130706433, 1431658454, 1),
(4, 'ad', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(5, '123', 0, '0000-00-00', '', 10, 1, 0, 0, 2005160675, 1432866824, 1),
(6, '569', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(7, '789', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(8, '法術打擊', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(9, 'cdsz1', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(10, 'fasd', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(11, '245', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(12, 'hwe', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(13, 'twa', 0, '0000-00-00', '', 10, 1, 0, 0, 2130706433, 1432024772, 1),
(14, 'gas', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(15, 'gsdf', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(16, '3uy5r', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(17, '5846', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(18, '564', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, -1),
(19, 'bing', 0, '0000-00-00', '', 10, 3, 0, 0, 1900867425, 1432870256, 1),
(20, 'hhh', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(21, '大冰', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(22, '784', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(23, '1231', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(24, '123456123', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(25, '564565', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(26, 'd45sa', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1),
(27, '21312', 0, '0000-00-00', '', 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_member_info`
--

CREATE TABLE IF NOT EXISTS `aiyue_member_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `ccount` varchar(255) NOT NULL COMMENT '賬號',
  `uids` varchar(255) NOT NULL COMMENT '會員卡號',
  `password` varchar(255) NOT NULL COMMENT '密碼',
  `tel` varchar(255) NOT NULL COMMENT '電話',
  `email` varchar(255) NOT NULL COMMENT '郵箱',
  `add` varchar(255) NOT NULL COMMENT '地址',
  `age` int(10) unsigned NOT NULL COMMENT '年齡',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL COMMENT '更新時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=26 ;

--
-- 转存表中的数据 `aiyue_member_info`
--

INSERT INTO `aiyue_member_info` (`id`, `ccount`, `uids`, `password`, `tel`, `email`, `add`, `age`, `status`, `create_time`) VALUES
(13, '李先生', '123456', 'c56d0e9a7ccec67b4ea131655038d604', '123456', '552023587@qq.com', '中國', 10, 1, 1442199665),
(14, '黃先生', '12345', '15c6d98082895abf1c20c0358aff2a67', '123456', '289065389@qq.com', '珠海市', 20, 1, 1439198601),
(16, '張小姐', '1234', '2e99bf4e42962410038bc6fa4ce40d97', '123456', '1061031727@qq.com', '珠海', 25, 1, 1439198597),
(17, '梁先生', '123456789', '70873e8580c9900986939611618d7b1e', '13456123456', '552023587@qq.com', '珠海', 20, 1, 1439198510),
(25, '冰', '1234567', '25d55ad283aa400af464c76d713c07ad', '159191901701', '794025348@qq.com', '拱北名门', 10, 1, 1442200226);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_member_notice`
--

CREATE TABLE IF NOT EXISTS `aiyue_member_notice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵',
  `title` varchar(255) NOT NULL COMMENT '通知标题',
  `txt` text NOT NULL COMMENT '内容',
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `pic` varchar(100) DEFAULT NULL,
  `file_id` int(50) DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=63 ;

--
-- 转存表中的数据 `aiyue_member_notice`
--

INSERT INTO `aiyue_member_notice` (`id`, `title`, `txt`, `uid`, `pic`, `file_id`, `time`) VALUES
(53, '', '哈哈', 13, '67', 1, '1440472217'),
(54, '', 'dsadas', 13, '49', 3, '1440472586'),
(55, '', 'dsada', 13, '71', 0, '1441941201'),
(56, '', '通知文件', 21, '123', 0, '1441960508'),
(57, '', '測試記錄表-愛樂', 21, '123', 0, '1441960626'),
(58, '', '爱乐琴行', 21, '124', 0, '1441968876'),
(59, '', '爱乐的文件', 21, '124', 0, '1441969034'),
(60, '', '21123123123', 13, '125', 8, '1442199650'),
(61, '', '爱乐通知1', 25, '123', 5, '1442201716'),
(62, '', '爱乐通知2', 25, '126', 9, '1442202697');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_menu`
--

CREATE TABLE IF NOT EXISTS `aiyue_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文檔ID',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '標題',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上級分類ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序（同級有效）',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `hide` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否隱藏',
  `tip` varchar(255) NOT NULL DEFAULT '' COMMENT '提示',
  `group` varchar(50) DEFAULT '' COMMENT '分組',
  `is_dev` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否僅開發者模式可見',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=129 ;

--
-- 转存表中的数据 `aiyue_menu`
--

INSERT INTO `aiyue_menu` (`id`, `title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`) VALUES
(1, '首頁', 0, 1, 'Index/index', 1, '', '', 0),
(2, '內容', 0, 2, 'Article/fback', 0, '', '', 0),
(3, '文檔列表', 2, 0, 'article/index', 1, '', '內容', 0),
(4, '新增', 3, 0, 'article/add', 0, '', '', 0),
(5, '編輯', 3, 0, 'article/edit', 0, '', '', 0),
(6, '改變狀態', 3, 0, 'article/setStatus', 0, '', '', 0),
(7, '保存', 3, 0, 'article/update', 0, '', '', 0),
(8, '保存草稿', 3, 0, 'article/autoSave', 0, '', '', 0),
(9, '移動', 3, 0, 'article/move', 0, '', '', 0),
(10, '復制', 3, 0, 'article/copy', 0, '', '', 0),
(11, '粘貼', 3, 0, 'article/paste', 0, '', '', 0),
(12, '導入', 3, 0, 'article/batchOperate', 0, '', '', 0),
(13, '回收站', 2, 0, 'article/recycle', 1, '', '內容', 0),
(14, '還原', 13, 0, 'article/permit', 0, '', '', 0),
(15, '清空', 13, 0, 'article/clear', 0, '', '', 0),
(16, '用戶', 0, 3, 'User/index', 0, '', '', 0),
(17, '用戶信息', 16, 0, 'User/index', 0, '', '用戶管理', 0),
(18, '新增用戶', 17, 0, 'User/add', 0, '添加新用戶', '', 0),
(19, '用戶行為', 16, 0, 'User/action', 0, '', '行為管理', 0),
(20, '新增用戶行為', 19, 0, 'User/addaction', 0, '', '', 0),
(21, '編輯用戶行為', 19, 0, 'User/editaction', 0, '', '', 0),
(22, '保存用戶行為', 19, 0, 'User/saveAction', 0, '"用戶->用戶行為"保存編輯和新增的用戶行為', '', 0),
(23, '變更行為狀態', 19, 0, 'User/setStatus', 0, '"用戶->用戶行為"中的啟用,禁用和刪除權限', '', 0),
(24, '禁用會員', 19, 0, 'User/changeStatus?method=forbidUser', 0, '"用戶->用戶信息"中的禁用', '', 0),
(25, '啟用會員', 19, 0, 'User/changeStatus?method=resumeUser', 0, '"用戶->用戶信息"中的啟用', '', 0),
(26, '刪除會員', 19, 0, 'User/changeStatus?method=deleteUser', 0, '"用戶->用戶信息"中的刪除', '', 0),
(27, '權限管理', 16, 0, 'AuthManager/index', 0, '', '用戶管理', 0),
(28, '刪除', 27, 0, 'AuthManager/changeStatus?method=deleteGroup', 0, '刪除用戶組', '', 0),
(29, '禁用', 27, 0, 'AuthManager/changeStatus?method=forbidGroup', 0, '禁用用戶組', '', 0),
(30, '恢復', 27, 0, 'AuthManager/changeStatus?method=resumeGroup', 0, '恢復已禁用的用戶組', '', 0),
(31, '新增', 27, 0, 'AuthManager/createGroup', 0, '創建新的用戶組', '', 0),
(32, '編輯', 27, 0, 'AuthManager/editGroup', 0, '編輯用戶組名稱和描述', '', 0),
(33, '保存用戶組', 27, 0, 'AuthManager/writeGroup', 0, '新增和編輯用戶組的"保存"按鈕', '', 0),
(34, '授權', 27, 0, 'AuthManager/group', 0, '"後臺 \\ 用戶 \\ 用戶信息"列表頁的"授權"操作按鈕,用於設置用戶所屬用戶組', '', 0),
(35, '訪問授權', 27, 0, 'AuthManager/access', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"訪問授權"操作按鈕', '', 0),
(36, '成員授權', 27, 0, 'AuthManager/user', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"成員授權"操作按鈕', '', 0),
(37, '解除授權', 27, 0, 'AuthManager/removeFromGroup', 0, '"成員授權"列表頁內的解除授權操作按鈕', '', 0),
(38, '保存成員授權', 27, 0, 'AuthManager/addToGroup', 0, '"用戶信息"列表頁"授權"時的"保存"按鈕和"成員授權"裏右上角的"添加"按鈕)', '', 0),
(39, '分類授權', 27, 0, 'AuthManager/category', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"分類授權"操作按鈕', '', 0),
(40, '保存分類授權', 27, 0, 'AuthManager/addToCategory', 0, '"分類授權"頁面的"保存"按鈕', '', 0),
(41, '模型授權', 27, 0, 'AuthManager/modelauth', 0, '"後臺 \\ 用戶 \\ 權限管理"列表頁的"模型授權"操作按鈕', '', 0),
(42, '保存模型授權', 27, 0, 'AuthManager/addToModel', 0, '"分類授權"頁面的"保存"按鈕', '', 0),
(43, '擴展', 0, 7, 'Addons/index', 0, '', '', 0),
(44, '插件管理', 43, 1, 'Addons/index', 0, '', '擴展', 0),
(45, '創建', 44, 0, 'Addons/create', 0, '服務器上創建插件結構向導', '', 0),
(46, '檢測創建', 44, 0, 'Addons/checkForm', 0, '檢測插件是否可以創建', '', 0),
(47, '預覽', 44, 0, 'Addons/preview', 0, '預覽插件定義類文件', '', 0),
(48, '快速生成插件', 44, 0, 'Addons/build', 0, '開始生成插件結構', '', 0),
(49, '設置', 44, 0, 'Addons/config', 0, '設置插件配置', '', 0),
(50, '禁用', 44, 0, 'Addons/disable', 0, '禁用插件', '', 0),
(51, '啟用', 44, 0, 'Addons/enable', 0, '啟用插件', '', 0),
(52, '安裝', 44, 0, 'Addons/install', 0, '安裝插件', '', 0),
(53, '卸載', 44, 0, 'Addons/uninstall', 0, '卸載插件', '', 0),
(54, '更新配置', 44, 0, 'Addons/saveconfig', 0, '更新插件配置處理', '', 0),
(55, '插件後臺列表', 44, 0, 'Addons/adminList', 0, '', '', 0),
(56, 'URL方式訪問插件', 44, 0, 'Addons/execute', 0, '控制是否有權限通過url訪問插件控制器方法', '', 0),
(57, '鉤子管理', 43, 2, 'Addons/hooks', 0, '', '擴展', 0),
(58, '模型管理', 68, 3, 'Model/index', 0, '', '系統設置', 0),
(59, '新增', 58, 0, 'model/add', 0, '', '', 0),
(60, '編輯', 58, 0, 'model/edit', 0, '', '', 0),
(61, '改變狀態', 58, 0, 'model/setStatus', 0, '', '', 0),
(62, '保存數據', 58, 0, 'model/update', 0, '', '', 0),
(63, '屬性管理', 68, 0, 'Attribute/index', 1, '網站屬性配置。', '', 0),
(64, '新增', 63, 0, 'Attribute/add', 0, '', '', 0),
(65, '編輯', 63, 0, 'Attribute/edit', 0, '', '', 0),
(66, '改變狀態', 63, 0, 'Attribute/setStatus', 0, '', '', 0),
(67, '保存數據', 63, 0, 'Attribute/update', 0, '', '', 0),
(68, '系統', 0, 4, 'Config/group', 0, '', '', 0),
(69, '網站設置', 68, 1, 'Config/group', 0, '', '系統設置', 0),
(70, '配置管理', 68, 4, 'Config/index', 0, '', '系統設置', 0),
(71, '編輯', 70, 0, 'Config/edit', 0, '新增編輯和保存配置', '', 0),
(72, '刪除', 70, 0, 'Config/del', 0, '刪除配置', '', 0),
(73, '新增', 70, 0, 'Config/add', 0, '新增配置', '', 0),
(74, '保存', 70, 0, 'Config/save', 0, '保存配置', '', 0),
(75, '菜單管理', 68, 5, 'Menu/index', 0, '', '系統設置', 0),
(76, '導航管理', 68, 6, 'Channel/index', 0, '', '系統設置', 0),
(77, '新增', 76, 0, 'Channel/add', 0, '', '', 0),
(78, '編輯', 76, 0, 'Channel/edit', 0, '', '', 0),
(79, '刪除', 76, 0, 'Channel/del', 0, '', '', 0),
(80, '分類管理', 68, 2, 'Category/index', 0, '', '系統設置', 0),
(81, '編輯', 80, 0, 'Category/edit', 0, '編輯和保存欄目分類', '', 0),
(82, '新增', 80, 0, 'Category/add', 0, '新增欄目分類', '', 0),
(83, '刪除', 80, 0, 'Category/remove', 0, '刪除欄目分類', '', 0),
(84, '移動', 80, 0, 'Category/operate/type/move', 0, '移動欄目分類', '', 0),
(85, '合並', 80, 0, 'Category/operate/type/merge', 0, '合並欄目分類', '', 0),
(86, '備份數據庫', 68, 0, 'Database/index?type=export', 0, '', '數據備份', 0),
(87, '備份', 86, 0, 'Database/export', 0, '備份數據庫', '', 0),
(88, '優化表', 86, 0, 'Database/optimize', 0, '優化數據表', '', 0),
(89, '修復表', 86, 0, 'Database/repair', 0, '修復數據表', '', 0),
(90, '還原數據庫', 68, 0, 'Database/index?type=import', 0, '', '數據備份', 0),
(91, '恢復', 90, 0, 'Database/import', 0, '數據庫恢復', '', 0),
(92, '刪除', 90, 0, 'Database/del', 0, '刪除備份文件', '', 0),
(93, '其他', 0, 5, 'other', 1, '', '', 0),
(96, '新增', 75, 0, 'Menu/add', 0, '', '系統設置', 0),
(98, '編輯', 75, 0, 'Menu/edit', 0, '', '', 0),
(104, '下載管理', 102, 0, 'Think/lists?model=download', 0, '', '', 0),
(105, '配置管理', 102, 0, 'Think/lists?model=config', 0, '', '', 0),
(106, '行為日誌', 16, 0, 'Action/actionlog', 0, '', '行為管理', 0),
(108, '修改密碼', 16, 0, 'User/updatePassword', 1, '', '', 0),
(109, '修改昵稱', 16, 0, 'User/updateNickname', 1, '', '', 0),
(110, '查看行為日誌', 106, 0, 'action/edit', 1, '', '', 0),
(112, '新增數據', 58, 0, 'think/add', 1, '', '', 0),
(113, '編輯數據', 58, 0, 'think/edit', 1, '', '', 0),
(114, '導入', 75, 0, 'Menu/import', 0, '', '', 0),
(115, '生成', 58, 0, 'Model/generate', 0, '', '', 0),
(116, '新增鉤子', 57, 0, 'Addons/addHook', 0, '', '', 0),
(117, '編輯鉤子', 57, 0, 'Addons/edithook', 0, '', '', 0),
(118, '文檔排序', 3, 0, 'Article/sort', 1, '', '', 0),
(119, '排序', 70, 0, 'Config/sort', 1, '', '', 0),
(120, '排序', 75, 0, 'Menu/sort', 1, '', '', 0),
(121, '排序', 76, 0, 'Channel/sort', 1, '', '', 0),
(123, '課程預約', 2, 0, 'Admin/article/appointment', 1, '課程預約', '', 0),
(122, '留言板', 2, 0, 'Admin/Article/fback', 1, '客護留言', '', 0),
(124, '會員管理', 2, 0, 'Article/userlist', 1, '會員管理', '', 0),
(125, ' 編輯會員信息', 124, 0, 'Article/useredit', 1, '編輯會員信息', '', 0),
(126, '新增', 124, 0, 'Article/useradd', 0, '新增會員', '', 0),
(127, '查看留言', 122, 0, 'Article/fbackdetail', 0, '留言板詳情頁', '', 0),
(128, '查看課程預約', 123, 0, 'Article/appointmentdetail', 0, '課程預約詳情頁', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_model`
--

CREATE TABLE IF NOT EXISTS `aiyue_model` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模型ID',
  `name` char(30) NOT NULL DEFAULT '' COMMENT '模型標識',
  `title` char(30) NOT NULL DEFAULT '' COMMENT '模型名稱',
  `extend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '繼承的模型',
  `relation` varchar(30) NOT NULL DEFAULT '' COMMENT '繼承與被繼承模型的關聯字段',
  `need_pk` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '新建表時是否需要主鍵字段',
  `field_sort` text NOT NULL COMMENT '表單字段排序',
  `field_group` varchar(255) NOT NULL DEFAULT '1:基礎' COMMENT '字段分組',
  `attribute_list` text NOT NULL COMMENT '屬性列表（表的字段）',
  `template_list` varchar(100) NOT NULL DEFAULT '' COMMENT '列表模板',
  `template_add` varchar(100) NOT NULL DEFAULT '' COMMENT '新增模板',
  `template_edit` varchar(100) NOT NULL DEFAULT '' COMMENT '編輯模板',
  `list_grid` text NOT NULL COMMENT '列表定義',
  `list_row` smallint(2) unsigned NOT NULL DEFAULT '10' COMMENT '列表數據長度',
  `search_key` varchar(50) NOT NULL DEFAULT '' COMMENT '默認搜索字段',
  `search_list` varchar(255) NOT NULL DEFAULT '' COMMENT '高級搜索的字段',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '狀態',
  `engine_type` varchar(25) NOT NULL DEFAULT 'MyISAM' COMMENT '數據庫引擎',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='文檔模型表' AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `aiyue_model`
--

INSERT INTO `aiyue_model` (`id`, `name`, `title`, `extend`, `relation`, `need_pk`, `field_sort`, `field_group`, `attribute_list`, `template_list`, `template_add`, `template_edit`, `list_grid`, `list_row`, `search_key`, `search_list`, `create_time`, `update_time`, `status`, `engine_type`) VALUES
(1, 'document', '基礎文檔', 0, '', 1, '{"1":["2","3","5","9","10","11","12","13","14","16","17","19","20"]}', '1:基礎', '', '', '', '', 'id:編號\r\ntitle:標題:[EDIT]&cate_id=[category_id]\r\nupdate_time|time_format:最後更新\r\nstatus_text:狀態\r\nid:操作:article/setstatus?status=-1&ids=[id]|刪除', 0, '', '', 1383891233, 1432623851, 1, 'MyISAM'),
(5, 'image', '圖片', 1, '', 1, '{"1":["3","37","38"],"2":["2","126","5","124","9","10","12","11","14","13","20","19","17","16"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1430273743, 1437806748, 1, 'MyISAM'),
(2, 'article', '文章', 1, '', 1, '{"1":["3","126"],"2":["9","13","124","143","19","10","12","2","16","5","17","20","14","11"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題:article/edit?cate_id=[category_id]&id=[id]\r\ncontent:內容', 0, '', '', 1383891243, 1439449393, 1, 'MyISAM'),
(3, 'download', '下載', 1, '', 1, '{"1":["3","28","30","32","2","5","31"],"2":["13","126","124","10","9","12","16","17","19","11","20","14","29"]}', '1:基礎,2:擴展', '', '', '', '', 'id:編號\r\ntitle:標題', 0, '', '', 1383891252, 1437806777, 1, 'MyISAM'),
(4, 'exam', '考試', 1, '', 1, '{"1":["3","34","126"],"2":["11","13","124","14","16","2","5","9","17","10","12","20","19"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1430126247, 1439469944, 1, 'MyISAM'),
(6, 'housetype', '房間類型', 1, '', 1, '{"1":["3","41","42"],"2":["10","11","12","126","124","13","14","16","2","17","5","9","19","20"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1430274974, 1437806753, 1, 'MyISAM'),
(7, 'lists', '經營列表', 1, '', 1, '{"1":["3","43"],"2":["2","126","5","124","9","10","12","14","13","16","11","17","20","19"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1430383140, 1437806760, 1, 'MyISAM'),
(8, 'logo', 'logo', 1, '', 1, '{"1":["3","44"],"2":["2","124","126","5","9","10","14","11","17","19","20","16","12","13"]}', '1:基礎;2:擴展；', '', '', '', '', 'id:編號', 10, '', '', 1430625162, 1437806733, 1, 'MyISAM'),
(9, 'teacher', '導師', 1, '', 1, '{"1":["3","120","126"],"2":["2","124","5","9","10","11","17","12","20","19","16","13","14"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1430966171, 1439455222, 1, 'MyISAM'),
(10, 'fback', '留言板', 0, '', 1, '{"1":["46","47","48","49","50","51"]}', '1:基礎', '', '', '', 'fbackedit', 'id:編號\r\nname:姓名\r\nphone:電話\r\ncontent:內容\r\ntime:時間\r\nstatus:狀態', 10, '', '', 1430977932, 1432611640, 1, 'MyISAM'),
(11, 'appointment', '課程預約', 0, '', 1, '', '1:基礎', '', '', '', '', '', 10, '', '', 1430987239, 1430987239, 1, 'MyISAM'),
(12, 'store', '連瑣店', 1, '', 1, '{"1":["3","2","56","57"],"2":["9","124","10","11","126","12","5","13","16","20","19","17","14"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1431140408, 1439455106, 1, 'MyISAM'),
(13, 'characteristic', '特色', 1, '', 1, '{"1":["3","59","58","60"],"2":["2","124","126","5","9","10","11","14","12","17","13","19","16","20"]}', '1:基礎;2:擴展；', '', '', '', '', 'id:編號', 10, '', '', 1431495856, 1438840480, 1, 'MyISAM'),
(14, 'course', '課程數據', 1, '', 1, '{"1":["3","62","63","65","70","67","82"],"2":["2","5","9","10","11","124","126","68","12","13","20","19","16","14","17"]}', '1:基礎;2:擴展;', '', '', '', '', 'id:編號', 10, '', '', 1431503595, 1441789886, 1, 'MyISAM'),
(15, 'rent', '租練', 1, '', 1, '{"1":["3","115","72","73","99"],"2":["12","124","126","13","14","16","2","17","5","19","9","20","11","10"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1431574502, 1437806679, 1, 'MyISAM'),
(16, 'contact', '聯繫我們', 1, '', 1, '{"1":["3","76","77","78","79","80"],"2":["9","10","124","11","12","126","13","14","2","19","17","20","5","16"]}', '1:基礎;2:擴展', '', '', 'contactadd', 'contactedit', 'id:編號', 10, '', '', 1431672516, 1439470239, 1, 'MyISAM'),
(17, 'courselist', '課程', 1, '', 1, '{"1":["3","95","84","83","85","86","87","88"],"2":["126","5","9","10","124","11","12","20","13","2","19","17","14","16"]}', '1:基礎;2:擴展', '', 'lists', '', 'courseedit', 'id:編號', 10, '', '', 1431936023, 1457341516, 1, 'MyISAM'),
(18, 'service', '服務', 1, '', 1, '{"1":["3","126"],"2":["9","5","148","124","10","2","11","17","19","20","16","12","14","13"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1431940678, 1439864498, 1, 'MyISAM'),
(19, 'instru', '樂器名稱', 1, '', 1, '{"1":["3","96"],"2":["2","5","9","124","10","11","126","13","14","12","16","17","19","20"]}', '1:基礎;2:擴展；', '', '', '', '', 'id:編號', 10, '', '', 1432281178, 1437806692, 1, 'MyISAM'),
(20, 'instruima', '樂器圖片', 1, '', 1, '{"1":["3","97","98","126"],"2":["14","13","124","16","2","12","9","5","20","10","11","19","17"]}', '1:基礎;2:擴展;', '', '', '', '', 'id:編號', 10, '', '', 1432281723, 1439878333, 1, 'MyISAM'),
(22, 'bg', '背景圖', 1, '', 1, '{"1":["3","116"],"2":["2","5","126","124","13","20","19","17","16","14","12","10","11","9"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1437375583, 1437806652, 1, 'MyISAM'),
(23, 'ad', '廣告', 1, '', 1, '{"1":["3","117","157"],"2":["2","124","5","16","11","20","19","126","17","14","13","12","10","9"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1437378067, 1456803260, 1, 'MyISAM'),
(24, 'class', '上課地點', 1, '', 1, '{"1":["3","124"],"2":["5","16","126","20","122","19","17","9","14","13","12","10","11","2"]}', '1:基礎;2:擴展', '', 'classlist', '', '', 'id:編號', 10, '', '', 1437536037, 1437806636, 1, 'MyISAM'),
(25, 'intro', '課程簡介', 1, '', 1, '{"1":["3"],"2":["2","16","124","20","19","17","12","14","13","10","9","11","125","5"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號', 10, '', '', 1437806466, 1437806564, 1, 'MyISAM'),
(26, 'member_info', '會員中心', 0, '', 1, '', '1:基礎', '', '', '', '', '', 10, '', '', 1437975684, 1437975684, 1, 'MyISAM'),
(28, 'member_notice', '会员通知', 0, '', 1, '', '1:基礎', '', '', '', '', '', 10, '', '', 1438159391, 1438159391, 1, 'MyISAM'),
(29, 'abrsm', 'ABRSM', 1, '', 1, '{"1":["3","144","145"],"2":["2","5","20","126","124","17","14","19","16","12","13","10","11","9"]}', '1:基礎;2:擴展 ', '', '', '', '', 'id:編號', 10, '', '', 1439469341, 1439469807, 1, 'MyISAM'),
(30, 'config', '網站設置', 1, '', 1, '{"1":["3","150","151","149","152","153","154"],"2":["2","5","9","16","12","10","19","17","126","124","20","14","13","11"]}', '1:基礎;2:擴展', '', '', '', '', 'id:編號 \r\n', 10, '', '', 1439865399, 1439865766, 1, 'MyISAM');

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_picture`
--

CREATE TABLE IF NOT EXISTS `aiyue_picture` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主鍵id自增',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '路徑',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '圖片鏈接',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT '文件 sha1編碼',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=170 ;

--
-- 转存表中的数据 `aiyue_picture`
--

INSERT INTO `aiyue_picture` (`id`, `path`, `url`, `md5`, `sha1`, `status`, `create_time`) VALUES
(1, '/Uploads/Picture/2015-04-29/554077467a18f.jpg', '', 'de7f71cb4cf560e3805242c9e96f4ac4', '5c9b7eac18e2b60a0d519d390562eecfd6110517', 1, 1430288198),
(144, '/Uploads/Picture/2016-02-29/56d4168b568bc.jpg', '', '97740370de95d110b98787664598945b', '309177ee3bd8142669f4659f5ea6d6e77746bc7c', 1, 1456739978),
(3, '/Uploads/Picture/2015-04-30/5541fd178c752.jpg', '', 'e5e0981789f249f9892c5c48e403e18f', '2f252e9fc31780ff0a2db523af85d2e6e0689b1b', 1, 1430387991),
(4, '/Uploads/Picture/2015-05-03/5545b5ffad3fa.png', '', '5b291bcca1c399d0feccc120050cc537', 'f5a0a61f65299825f27127ffa2c28b0793be6aae', 1, 1430631935),
(5, '/Uploads/Picture/2015-05-09/554da673b5efb.jpg', '', '9caa9867d4edecaa247e9d98197219dd', 'fd0c3bd89dc97ee4d789ca8fb98023cf2776d751', 1, 1431152243),
(6, '/Uploads/Picture/2015-05-13/5552b266c6719.png', '', 'ac34f822439102161999dd14408f2cfb', 'f137442455d6a440070823cc4f691a293f25e2c1', 1, 1431482982),
(7, '/Uploads/Picture/2015-05-13/5552b27729ef0.png', '', '2fc0e54be2520a0993886316e05a4bb9', '8225297eeef8837fbfb949f67985c41cb0b4809c', 1, 1431482999),
(8, '/Uploads/Picture/2015-05-13/5552e8446ad2a.jpg', '', '44290d27277cbdedde069917da993be8', '3d28a3955dee00322268a9278cf00b9adf38e047', 1, 1431496772),
(9, '/Uploads/Picture/2015-05-13/5552e8ff1f8c8.jpg', '', 'e2b94b31639f421f33f9257d756d13c5', 'b136de1e0045a66cdbcf8d28160a05dc9177a019', 1, 1431496959),
(10, '/Uploads/Picture/2015-05-13/5552e91dd7927.jpg', '', '5338019753aab4322a2b79b2a910847f', 'f16b37e664158284775c78737e25a704313f991f', 1, 1431496989),
(11, '/Uploads/Picture/2015-05-13/5552e96bb486c.jpg', '', '128e7707b4bb8bdd860c017259f81bde', 'acaa7b403a309815f22c95fb953e9ea22c6048c9', 1, 1431497067),
(12, '/Uploads/Picture/2015-05-13/5552e9cee390e.jpg', '', 'c2566b9303dd7335f64df595260968eb', 'dc6b56015ed189e23a17f160054f2700d08cd450', 1, 1431497166),
(13, '/Uploads/Picture/2015-05-14/555406e8d12d5.jpg', '', '582321778bca4d6ca7bc6c9e82e9c8a3', '6af068df24bd361eb10d7942b4d85c20d93cb51e', 1, 1431570152),
(169, '/Uploads/Picture/2016-03-21/56ef8bf181e9b.jpg', '', '5b4af2be075f897e93bf429a55246aec', 'b6fc4772e0c896f0b92d80b06c59dacb41cbf38f', 1, 1458539505),
(15, '/Uploads/Picture/2015-05-18/5559641e69e42.jpg', '', '51aea04d39eedb7bfdc18aebfab1d515', '6a2a83b918c6598eaaae549b23d1e9a10e9c2040', 1, 1431921694),
(16, '/Uploads/Picture/2015-05-25/5562c598b96c2.jpg', '', '1fe20b5578d64d86ab4145b9be71eaac', '411cc90cf3228c3b06173a3a7469f2d261f51006', 1, 1432536472),
(17, '/Uploads/Picture/2015-05-25/5562cf6d4e3ad.png', '', '31d37299d91f37af2c68a94e73ef3eae', 'c58e175313d7d5b47c41edb596bcb12cfca3cb3a', 1, 1432538989),
(18, '/Uploads/Picture/2015-05-25/5562d182e403e.png', '', 'be49e033109e11d8a9821984169388e1', 'ed000ad93848913bc947fdebbf15797a271cbd8e', 1, 1432539522),
(93, '/Uploads/Picture/2015-09-11/55f27e4c78c24.jpg', '', '03e4ab93de35310b67e19db2d18b3931', 'bd85739be7adfb0829d4d091f8f0c47ac36de8e2', 1, 1441955400),
(167, '/Uploads/Picture/2016-03-21/56ef8bd0594e8.jpg', '', '31cf649dec6e08cb3c526c832d261601', '712aed848ab0f2d014ed1a54d7922ec28fd64e73', 1, 1458539472),
(147, '/Uploads/Picture/2016-02-29/56d417cc86184.jpg', '', 'cf8a87e6d11b13261da297519cc1b1cc', '07782e1260077d05b86c19dc2990ecf0b0383509', 1, 1456740300),
(143, '/Uploads/Picture/2016-02-29/56d4158a1e80a.jpg', '', 'a2303a11da7f2798eddb2703f7614be5', 'a93c004bbd4918f6fc5dda7bd2b3d22e8b87ad15', 1, 1456739720),
(132, '/Uploads/Picture/2016-02-29/56d3e288069ed.jpg', '', '1bff7ee93cefcee590a926a83a8be989', '7bdd6c4bd3d6238f656d642ca93366149630f522', 1, 1456726651),
(168, '/Uploads/Picture/2016-03-21/56ef8bde3b13d.jpg', '', '905a8573fdfe78700c34b9c729b928fc', '1594ba9fb61263202630637a1b12948556421155', 1, 1458539486),
(139, '/Uploads/Picture/2016-02-29/56d3ed7d793da.jpg', '', '9718eaa4f7ba9f24c6f1b59a0f51f7bb', '7bd4603e6f5faa79ec546c75d26bff1caa30c303', 1, 1456729468),
(164, '/Uploads/Picture/2016-03-10/56e15ef86e985.jpg', '', '8aa5f28142bf8a67c9ebebce38529128', 'c53a9e01e4b951a024fd239e21d8bf32f323da42', 1, 1457610485),
(70, '/Uploads/Picture/2015-09-09/55effa5e34613.png', '', '44cd598c64d32d037ea6a1b331797bf8', '0ad084eca288f92adda63a2739e8713a23cdc65d', 1, 1441790558),
(128, '/Uploads/Picture/2016-02-29/56d32db0217b5.png', '', 'fdc9206573018668251c01cba561f717', '55a31e80214006ed96b406825ddde07bb52ecf2d', 1, 1456680367),
(151, '/Uploads/Picture/2016-03-10/56e125b88133d.jpg', '', 'c8df307db132bf125df064121efb7d78', 'ead6ef803d16240956b5267adb263941d69da3e9', 1, 1457595831),
(44, '/Uploads/Picture/2015-07-31/55bb13233d2e8.jpg', '', 'acb8194098663a43fe3ea636bdd7c95f', '8f00fdbb55a2213ab230ffd5a5725714fb8b4ca2', 1, 1438323491),
(111, '/Uploads/Picture/2015-09-11/55f288a6de111.jpg', '', '15657466bdfdc6ab4b11e8550e817a32', 'f8a92ed0decfc05c0f03dde7e847162ee07a9ecf', 1, 1441958052),
(46, '/Uploads/Picture/2015-08-06/55c2dcdfcacf7.jpg', '', 'b61cf836fff537963b5360ce3dfae240', '964785bb6be613947ab77672d21c5a58f8922e0b', 1, 1438833887),
(47, '/Uploads/Picture/2015-08-06/55c2f6ecebe13.jpg', '', '58926af89a1aa60aa7daf0be2c493718', '39a45df9cb429dba5d05ae226535b0bf7f6b8a63', 1, 1438840556),
(48, '/Uploads/Picture/2015-08-06/55c331fd67622.gif', '', 'df46993044576f83f2c2cc1a64e18f31', '1e0d02a9dd841f94d68bc0a1da91f858ac9874d7', 1, 1438855677),
(49, '/Uploads/Picture/2015-08-06/55c33bc8235c2.jpg', '', '11cdd6e541711c708a9c9a3de9b1e13a', 'ef517ee307224aaf39d4f0d0afdf99913897b010', 1, 1438858184),
(50, '/Uploads/Picture/2015-08-07/55c452880d2c3.jpg', '', 'd6c33e9c67a61dad3ec3ad5317689562', '4f891fa59a038239b7ad34ec8c4f073f772b171f', 1, 1438929544),
(141, '/Uploads/Picture/2016-02-29/56d3f0c7350fe.jpg', '', '87142bd58e9d3dde8be5764d233637df', '3ec36121e866cce6879cc62bfb0033ac4eca202d', 1, 1456730309),
(155, '/Uploads/Picture/2016-03-10/56e12dc936bf3.jpg', '', 'a60895681cbd9c2fb6106310f9fe0955', '31066f90cda111682c97963779be06b3a9642320', 1, 1457597896),
(140, '/Uploads/Picture/2016-02-29/56d3f06996ef1.jpg', '', '1a55b334c896fa76c3b44d26c726b2c2', '132188581dec4c49b8cab55f8c8d323ed7070b1f', 1, 1456730214),
(153, '/Uploads/Picture/2016-03-10/56e12bc930865.jpg', '', 'f0af93b252b210814600b67020a9156a', 'c48cf0e1b96dd3f666d2b425ec6f54c623ac7ff3', 1, 1457597361),
(154, '/Uploads/Picture/2016-03-10/56e12c301f858.jpg', '', '99516f7125fabd36c6c89ebbdc912411', '1af5e243fc1a2b93daa8fa5f1129903dd3bb77bf', 1, 1457597483),
(148, '/Uploads/Picture/2016-02-29/56d417cc90707.jpg', '', '31fa8a67231034f7747e804c600006d7', '563e0ea14af2dc1c61c6d964ad37a787eedd6122', 1, 1456740300),
(127, '/Uploads/Picture/2016-02-29/56d328fda4f51.jpg', '', '5c75270acf3ac891347dffbfd3534c34', '924e70c1ae29524ce79d944a6db9345b0c974b34', 1, 1456679163),
(161, '/Uploads/Picture/2016-03-10/56e1310cda136.jpg', '', '3e6c6a175106c0a74437fd6028229f30', '3866ca3833066653e6bb418cbe2a07261b881038', 1, 1457598731),
(160, '/Uploads/Picture/2016-03-10/56e13093a36a1.jpg', '', '43004e7804457622673367274cd82001', 'cd974540aac5436f4820fb3568a6a8e7a4a7b137', 1, 1457598610),
(162, '/Uploads/Picture/2016-03-10/56e1316dbfb51.jpg', '', 'f351dfc57db13e86d6d0bc6c36af89ee', '81286833dd0c1f590a6a551a1994dee606287e23', 1, 1457598829),
(163, '/Uploads/Picture/2016-03-10/56e1474ab7af5.jpg', '', '659edb948543a2fcc8766b3db2a89f97', '38b7c3fecdade7b29f1e0dd3a220cf9c12aac25b', 1, 1457604425),
(116, '/Uploads/Picture/2015-09-11/55f28ab8067bd.jpg', '', '5f9435e5dc364b48c260b56e71082be8', '9eda50fde04744c9a9af460eff36c99795c25c4c', 1, 1441958582),
(117, '/Uploads/Picture/2015-09-11/55f28ac9ab920.jpg', '', '11e9ece99661e418a837996535657e4d', 'cc88ab0fae13953a4fbb2b43d3f6954f6c170658', 1, 1441958601),
(118, '/Uploads/Picture/2015-09-11/55f28ae38e54c.jpg', '', '1cee156915157212cd0329134a9f4e81', 'd83936356060cc7eb0afda133e60dcbed5664ca5', 1, 1441958626),
(119, '/Uploads/Picture/2015-09-11/55f28c64cc7ed.jpg', '', 'a54b174bb17ea11c20a61fb45894f93a', 'e817fb981f931fb3dc7e81f7413271e081426bfd', 1, 1441959011),
(120, '/Uploads/Picture/2015-09-11/55f28c925004c.jpg', '', '2c709489fa257467a30b63a5c9303da0', 'debce247c9fe0cc1781674445f3ea1aa8dc26dee', 1, 1441959056),
(121, '/Uploads/Picture/2015-09-11/55f28cf26a442.jpg', '', 'a18c01f7413cdee946306b1b130b5946', '218c29c7235576e3f4eb6862fe99e850fce5f045', 1, 1441959152),
(150, '/Uploads/Picture/2016-03-10/56e121aa5e7d2.jpg', '', '941b02b7206667185ce9ae11e2d03218', '9d3e26a20eb8f23e59057c48c446691f0316d68b', 1, 1457594770),
(123, '/Uploads/Picture/2015-09-11/55f291fcea340.jpg', '', '2cdd5aeaa4e50170130cdcf0ebbb2fba', '3abbf7a4005d0b17be78413ce7c4491c1effa27c', 1, 1441960441),
(124, '/Uploads/Picture/2015-09-11/55f2b2db8a59d.jpg', '', 'b5e1b5d05de83f8e3d15158cb2d3a42c', '9003c5057cdddbe49db04512c4b63d347e05a909', 1, 1441968858),
(125, '/Uploads/Picture/2015-09-14/55f62f785a903.png', '', '0e1a17d1931d72816c5f518eb3928316', 'b2082f7ee4d247c12ba055d6c0f21422759e524b', 1, 1442197368),
(126, '/Uploads/Picture/2015-09-14/55f6440a05b5b.jpg', '', '9f95a49b7e59e14cd6337af59c7f9bd3', '1cecc7a062d66bf6d22765056101ea70f3d014ca', 1, 1442202631),
(129, '/Uploads/Picture/2016-02-29/56d32dcc375da.jpg', '', '98d28de1453e9313bf7e0dfb9cfcad51', '17d86214d632c94d13c08aae3f45e3a6377c2bcb', 1, 1456680396),
(130, '/Uploads/Picture/2016-02-29/56d32dd9b90e6.jpg', '', '9922fd3ebe31a54bf87c24b7cf69731d', 'e21ef0bd112b188fbac0aded9fea3c477dd72dad', 1, 1456680409),
(131, '/Uploads/Picture/2016-02-29/56d32f19243e3.jpg', '', '5edc0509ca17dc12ba5d978d13233442', 'e2364ffa5f47e18192e8ee32b9227c69d6234c1c', 1, 1456680728),
(166, '/Uploads/Picture/2016-03-10/56e164f72bd3b.JPG', '', '1656b631e23d7461ebd2b4b29451b2cc', 'be4b01c256b56919da16b890060bf870aa080643', 1, 1457611992);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_ucenter_admin`
--

CREATE TABLE IF NOT EXISTS `aiyue_ucenter_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理員ID',
  `member_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理員用戶ID',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '管理員狀態',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理員表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_ucenter_app`
--

CREATE TABLE IF NOT EXISTS `aiyue_ucenter_app` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '應用ID',
  `title` varchar(30) NOT NULL COMMENT '應用名稱',
  `url` varchar(100) NOT NULL COMMENT '應用URL',
  `ip` char(15) NOT NULL COMMENT '應用IP',
  `auth_key` varchar(100) NOT NULL COMMENT '加密KEY',
  `sys_login` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '同步登陸',
  `allow_ip` varchar(255) NOT NULL COMMENT '允許訪問的IP',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '應用狀態',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='應用表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_ucenter_member`
--

CREATE TABLE IF NOT EXISTS `aiyue_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用戶ID',
  `username` char(16) NOT NULL COMMENT '用戶名',
  `account` varchar(50) NOT NULL COMMENT '會員卡號',
  `password` char(32) NOT NULL COMMENT '密碼',
  `email` char(32) NOT NULL COMMENT '用戶郵箱',
  `mobile` char(15) NOT NULL COMMENT '用戶手機',
  `address` varchar(200) NOT NULL COMMENT '地址',
  `age` int(10) NOT NULL,
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '註冊時間',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '註冊IP',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最後登錄時間',
  `last_login_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '最後登錄IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新時間',
  `status` tinyint(4) DEFAULT '0' COMMENT '用戶狀態',
  PRIMARY KEY (`id`,`reg_ip`,`reg_time`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='用戶表' AUTO_INCREMENT=28 ;

--
-- 转存表中的数据 `aiyue_ucenter_member`
--

INSERT INTO `aiyue_ucenter_member` (`id`, `username`, `account`, `password`, `email`, `mobile`, `address`, `age`, `reg_time`, `reg_ip`, `last_login_time`, `last_login_ip`, `update_time`, `status`) VALUES
(1, 'admin', '', '0d12e487d521a3fe38a5b6e2f3e7d2c9', '1061031727@qq.com', '', '', 0, 1429084270, 2130706433, 1437967855, 2130706433, 1429084270, 1),
(2, '123456', '', '0b1f8c053f2d3a24c373074ddc9c8875', '1234567@qq.com', '', '', 0, 1431481525, 2130706433, 1432608272, 2130706433, 1431481525, 1),
(3, 'users', '', '9c317a07eb6c4be88488e1ca6311f251', '10610584@qq.com', '', '', 0, 1431656264, 2130706433, 1431658454, 2130706433, 1431656264, 1),
(4, 'ad', '', '9c317a07eb6c4be88488e1ca6311f251', '1061031725@qq.com', '', '', 0, 1432003792, 2130706433, 0, 0, 1432003792, 1),
(17, '5846', '', '9c317a07eb6c4be88488e1ca6311f251', '52101225@sina.cn', '', '', 0, 1432015067, 2130706433, 0, 0, 1432015067, 0),
(19, 'bing', '154744', '9c317a07eb6c4be88488e1ca6311f251', '2346511000@qq.com', '234564211', '344413212', 0, 1432805018, 460175284, 1432870256, 1900867425, 1432805018, 1),
(20, 'hhh', '212454', '9c317a07eb6c4be88488e1ca6311f251', '6842131313@1221.com', '2414654541', '2311', 0, 1432805753, 460175284, 0, 0, 1432805753, 1),
(21, '大冰', '123', '9c317a07eb6c4be88488e1ca6311f251', '35486786@qq.com', '56464112', '568441411', 0, 1432866870, 1900867425, 0, 0, 1432866870, 1),
(22, '784', '456789', '9c317a07eb6c4be88488e1ca6311f251', '1245454@sina.com', '1244545', 'gasdg', 0, 1433212698, 3682848929, 0, 0, 1433212698, 1),
(23, '1231', '123456', '9c317a07eb6c4be88488e1ca6311f251', '12312@qq.com', '', '', 0, 1436927616, 2130706433, 0, 0, 1436927616, 1),
(24, '123456123', '123456', '9c317a07eb6c4be88488e1ca6311f251', '12312123@qq.com', '', '', 0, 1436927657, 2130706433, 0, 0, 1436927657, 1),
(25, '564565', '', '9c317a07eb6c4be88488e1ca6311f251', '445456@qq.com', '', '', 0, 1437363273, 2130706433, 0, 0, 1437363273, 1),
(27, '21312', '321321', '9c317a07eb6c4be88488e1ca6311f251', '321321@qq.com', '321312', '321312', 0, 1437805219, 2130706433, 0, 0, 1437805219, 1);

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_ucenter_setting`
--

CREATE TABLE IF NOT EXISTS `aiyue_ucenter_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '設置ID',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '配置類型（1-用戶配置）',
  `value` text NOT NULL COMMENT '配置數據',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='設置表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_url`
--

CREATE TABLE IF NOT EXISTS `aiyue_url` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '鏈接唯壹標識',
  `url` char(255) NOT NULL DEFAULT '' COMMENT '鏈接地址',
  `short` char(100) NOT NULL DEFAULT '' COMMENT '短網址',
  `status` tinyint(2) NOT NULL DEFAULT '2' COMMENT '狀態',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '創建時間',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='鏈接表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `aiyue_userdata`
--

CREATE TABLE IF NOT EXISTS `aiyue_userdata` (
  `uid` int(10) unsigned NOT NULL COMMENT '用戶id',
  `type` tinyint(3) unsigned NOT NULL COMMENT '類型標識',
  `target_id` int(10) unsigned NOT NULL COMMENT '目標id',
  UNIQUE KEY `uid` (`uid`,`type`,`target_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
