<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Home\Logic;
/**
 * Description of TeacherLogic
 *
 * @author Tina
 */
class TeacherLogic extends BaseLogic{
	/* 自動驗證規則 */
	/*protected $_validate = array(
		array('content', 'getContent', '內容不能為空！', self::MUST_VALIDATE , 'callback', self::MODEL_BOTH),
	);*/

	/* 自動完成規則 */
	protected $_auto = array();

	/**
	 * 新增或添加壹條文章詳情
	 * @param  number $id 文章ID
	 * @return boolean    true-操作成功，false-操作失敗
	 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
	 */
	public function update($id = 0){
		/* 獲取文章數據 */
		$data = $this->create();
		if($data === false){
			return false;
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}

	/**
	 * 獲取文章的詳細內容
	 * @return boolean
	 * @author huajie <banhuajie@163.com>
	 */
	protected function getContent(){
		$type = I('post.type');
		$content = I('post.content');
		if($type > 1){	//主題和段落必須有內容
			if(empty($content)){
				return false;
			}
		}else{			//目錄沒內容則生成空字符串
			if(empty($content)){
				$_POST['content'] = ' ';
			}
		}
		return true;
	}

	/**
	 * 保存為草稿
	 * @return true 成功， false 保存出錯
	 * @author huajie <banhuajie@163.com>
	 */
	public function autoSave($id = 0){
		$this->_validate = array();

		/* 獲取文章數據 */
		$data = $this->create();
		if(!$data){
			return false;
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}
}