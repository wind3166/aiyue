<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Model;
use Think\Model;
use User\Api\UserApi;

/**
 * 文檔基礎模型
 */
class MemberModel extends Model{

    /* 用戶模型自動完成 */
    protected $_auto = array(
        array('login', 0, self::MODEL_INSERT),
        array('reg_ip', 'get_client_ip', self::MODEL_INSERT, 'function', 1),
        array('reg_time', NOW_TIME, self::MODEL_INSERT),
        array('last_login_ip', 0, self::MODEL_INSERT),
        array('last_login_time', 0, self::MODEL_INSERT),
        array('update_time', NOW_TIME),
        array('status', 1, self::MODEL_INSERT),
    );

    /**
     * 登錄指定用戶
     * @param  integer $uid 用戶ID
     * @return boolean      ture-登錄成功，false-登錄失敗
     */
    public function login($uid){
        /* 檢測是否在當前應用註冊 */
        $user = $this->field(true)->find($uid);
        if(!$user){ //未註冊
            /* 在當前應用中註冊用戶 */
        	$Api = new UserApi();
        	$info = $Api->info($uid);
            $user = $this->create(array('nickname' => $info[1], 'status' => 1));
            $user['uid'] = $uid;
            if(!$this->add($user)){
                $this->error = '前臺用戶信息註冊失敗，請重試！';
                return false;
            }
        } elseif(1 != $user['status']) {
            $this->error = '用戶未激活或已禁用！'; //應用級別禁用
            return false;
        }

        /* 登錄用戶 */
        $this->autoLogin($user);

        //記錄行為
        action_log('user_login', 'member', $uid, $uid);

        return true;
    }

    /**
     * 註銷當前用戶
     * @return void
     */
    public function logout(){
        session('user_auth', null);
        session('user_auth_sign', null);
        unset($_SESSION['user']);
    }

    /**
     * 自動登錄用戶
     * @param  integer $user 用戶信息數組
     */
    private function autoLogin($user){
        /* 更新登錄信息 */
        $data = array(
            'uid'             => $user['uid'],
            'login'           => array('exp', '`login`+1'),
            'last_login_time' => NOW_TIME,
            'last_login_ip'   => get_client_ip(1),
        );
        $this->save($data);

        /* 記錄登錄SESSION和COOKIES */
        $auth = array(
            'uid'             => $user['uid'],
            'username'        => get_username($user['uid']),
            'last_login_time' => $user['last_login_time'],
        );

        session('user_auth', $auth);
        session('user_auth_sign', data_auth_sign($auth));

    }

}