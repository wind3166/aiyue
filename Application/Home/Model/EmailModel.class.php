<?php

/**
 * 發送郵件
 *
 * @author Tina
 */

namespace Home\Model;

use Think\Model;

class EmailModel extends Model {
// 系統郵件發送函數 
// @param string $to 接收郵件者郵箱 
// @param string $name 接收郵件者名稱 
// @param string $subject 郵件主題 
// @param string $body 郵件內容 
// @param string $attachment 附件列表 
    public function send_mail($to = '', $subject = '', $body = '', $name = '', $attachment = null) {
        $from_email = C('MAIL_SMTP_USER'); 
        $from_name = C('WEB_SITE');
        $reply_email = '';
        $reply_name = '';
        require_once './Plugin/PHPMailer/PHPMailerAutoload.php';
        $mail = new \PHPMailer;
        $mail->CharSet    = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPDebug = 0;                       // 關閉SMTP調試功能
                                                    // 1 = errors and messages
                                                    // 2 = messages only
        $mail->SMTPAuth = true;                  // 啟用 SMTP 驗證功能
        $mail->SMTPSecure = '';                 // 使用安全協議 
        $mail->Host = C('MAIL_SMTP_HOST');  // SMTP 服務器
        $mail->Port = C('MAIL_SMTP_PORT');  // SMTP服務器的端口號
        $mail->Username = C('MAIL_SMTP_USER');  // SMTP服務器用戶名
        $mail->Password = C('MAIL_SMTP_PASS');  // SMTP服務器密碼
        $mail->SetFrom($from_email, $from_name);
        $replyEmail = $reply_email ? $reply_email : $from_email;
        $replyName = $reply_name ? $reply_name : $from_name;
        if ($to == '') {
            $to = C('MAIL_SMTP_CE'); //郵件地址為空時，默認使用後臺默認郵件測試地址
        }
        if ($name == '') {
            $name = C('WEB_SITE'); //發送者名稱為空時，默認使用網站名稱
        }
        if ($subject == '') {
            $subject = C('MAIL_SMTP_SUBJECT'); //郵件主題為空時，默認使用網站標題
        }
        if ($body == '') {
            $body = C('MAIL_SMTP_BODY'); //郵件內容為空時，默認使用網站描述
        }
        $mail->AddReplyTo($replyEmail, $replyName);
        $mail->Subject = $subject;
        $mail->MsgHTML($body); //解析
        $mail->AddAddress($to, $name);
        if (is_array($attachment)) { // 添加附件
            foreach ($attachment as $file) {
                is_file($file) && $mail->AddAttachment($file);
            }
        }
        return $mail->Send() ? true : $mail->ErrorInfo; //返回錯誤信息
    }
 

}