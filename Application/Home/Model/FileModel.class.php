<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Model;
use Think\Model;

/**
 * 文件模型
 * 負責文件的下載和上傳
 */

class FileModel extends Model{
	/**
	 * 文件模型自動完成
	 * @var array
	 */
	protected $_auto = array(
		array('create_time', NOW_TIME, self::MODEL_INSERT),
	);

	/**
	 * 文件模型字段映射
	 * @var array
	 */
	protected $_map = array(
		'type' => 'mime',
	);

	/**
	 * 文件上傳
	 * @param  array  $files   要上傳的文件列表（通常是$_FILES數組）
	 * @param  array  $setting 文件上傳配置
	 * @param  string $driver  上傳驅動名稱
	 * @param  array  $config  上傳驅動配置
	 * @return array           文件上傳成功後的信息
	 */
	public function upload($files, $setting, $driver = 'Local', $config = null){
		/* 上傳文件 */
		$setting['callback'] = array($this, 'isFile');
		$Upload = new \Think\Upload($setting, $driver, $config);
		$info   = $Upload->upload($files);

		/* 設置文件保存位置 */
		$this->_auto[] = array('location', 'Ftp' === $driver ? 1 : 0, self::MODEL_INSERT);

		if($info){ //文件上傳成功，記錄文件信息
			foreach ($info as $key => &$value) {
				/* 已經存在文件記錄 */
				if(isset($value['id']) && is_numeric($value['id'])){
					continue;
				}

				/* 記錄文件信息 */
				if($this->create($value) && ($id = $this->add())){
					$value['id'] = $id;
				} else {
					//TODO: 文件上傳成功，但是記錄文件信息失敗，需記錄日誌
					unset($info[$key]);
				}
			}
			return $info; //文件上傳成功
		} else {
			$this->error = $Upload->getError();
			return false;
		}
	}

	/**
	 * 下載指定文件
	 * @param  number  $root 文件存儲根目錄
	 * @param  integer $id   文件ID
	 * @param  string   $args     回調函數參數
	 * @return boolean       false-下載失敗，否則輸出下載文件
	 */
	public function download($root, $id, $callback = null, $args = null){
		/* 獲取下載文件信息 */
		$file = $this->find($id);
		if(!$file){
			$this->error = '不存在該文件！';
			return false;
		}

		/* 下載文件 */
		switch ($file['location']) {
			case 0: //下載本地文件
				$file['rootpath'] = $root;
				return $this->downLocalFile($file, $callback, $args);
			case 1: //TODO: 下載遠程FTP文件
				break;
			default:
				$this->error = '不支持的文件存儲類型！';
				return false;

		}

	}

	/**
	 * 檢測當前上傳的文件是否已經存在
	 * @param  array   $file 文件上傳數組
	 * @return boolean       文件信息， false - 不存在該文件
	 */
	public function isFile($file){
		if(empty($file['md5'])){
			throw new Exception('缺少參數:md5');
		}
		/* 查找文件 */
		$map = array('md5' => $file['md5']);
		return $this->field(true)->where($map)->find();
	}

	/**
	 * 下載本地文件
	 * @param  array    $file     文件信息數組
	 * @param  callable $callback 下載回調函數，壹般用於增加下載次數
	 * @param  string   $args     回調函數參數
	 * @return boolean            下載失敗返回false
	 */
	private function downLocalFile($file, $callback = null, $args = null){
		if(is_file($file['rootpath'].$file['savepath'].$file['savename'])){
			/* 調用回調函數新增下載數 */
			is_callable($callback) && call_user_func($callback, $args);

			/* 執行下載 */ //TODO: 大文件斷點續傳
			header("Content-Description: File Transfer");
			header('Content-type: ' . $file['type']);
			header('Content-Length:' . $file['size']);
			if (preg_match('/MSIE/', $_SERVER['HTTP_USER_AGENT'])) { //for IE
				header('Content-Disposition: attachment; filename="' . rawurlencode($file['name']) . '"');
			} else {
				header('Content-Disposition: attachment; filename="' . $file['name'] . '"');
			}
			readfile($file['rootpath'].$file['savepath'].$file['savename']);
			exit;
		} else {
			$this->error = '文件已被刪除！';
			return false;
		}
	}

}