<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Model;
use Think\Model;
use Think\Page;

/**
 * 文檔基礎模型
 */
class DocumentModel extends Model{

	/* 自動驗證規則 */
	protected $_validate = array(
		array('name', '/^[a-zA-Z]\w{0,30}$/', '文檔標識不合法', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
		array('name', '', '標識已經存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
		array('title', 'require', '標題不能為空', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
		array('category_id', 'require', '分類不能為空', self::MUST_VALIDATE , 'regex', self::MODEL_INSERT),
		array('category_id', 'require', '分類不能為空', self::EXISTS_VALIDATE , 'regex', self::MODEL_UPDATE),
		array('category_id,type', 'checkCategory', '內容類型不正確', self::MUST_VALIDATE , 'callback', self::MODEL_INSERT),
		array('category_id', 'checkCategory', '該分類不允許發布內容', self::EXISTS_VALIDATE , 'callback', self::MODEL_BOTH),
		array('model_id,category_id', 'checkModel', '該分類沒有綁定當前模型', self::MUST_VALIDATE , 'callback', self::MODEL_INSERT),
	);

	/* 自動完成規則 */
	protected $_auto = array(
		array('uid', 'session', self::MODEL_INSERT, 'function', 'user_auth.uid'),
		array('title', 'htmlspecialchars', self::MODEL_BOTH, 'function'),
		array('description', 'htmlspecialchars', self::MODEL_BOTH, 'function'),
		array('root', 'getRoot', self::MODEL_BOTH, 'callback'),
		array('attach', 0, self::MODEL_INSERT),
		array('view', 0, self::MODEL_INSERT),
		array('comment', 0, self::MODEL_INSERT),
		array('extend', 0, self::MODEL_INSERT),
		array('create_time', NOW_TIME, self::MODEL_INSERT),
		array('reply_time', NOW_TIME, self::MODEL_INSERT),
		array('update_time', NOW_TIME, self::MODEL_BOTH),
		array('status', 'getStatus', self::MODEL_BOTH, 'callback'),
	);

    public $page = '';

	/**
	 * 獲取文檔列表
	 * @param  integer  $category 分類ID
	 * @param  string   $order    排序規則
	 * @param  integer  $status   狀態
	 * @param  boolean  $count    是否返回總數
	 * @param  string   $field    字段 true-所有字段
	 * @return array              文檔列表
	 */
	public function lists($category, $order = '`id` DESC', $status = 1, $field = true){
		$map = $this->listMap($category, $status);
		return $this->field($field)->where($map)->order($order)->select();
	}

	/**
	 * 計算列表總數
	 * @param  number  $category 分類ID
	 * @param  integer $status   狀態
	 * @return integer           總數
	 */
	public function listCount($category, $status = 1){
		$map = $this->listMap($category, $status);
		return $this->where($map)->count('id');
	}

	/**
	 * 獲取詳情頁數據
	 * @param  integer $id 文檔ID
	 * @return array       詳細數據
	 */
	public function detail($id){
		/* 獲取基礎數據 */
		$info = $this->field(true)->find($id);
		if(!(is_array($info) || 1 !== $info['status'])){
			$this->error = '文檔被禁用或已刪除！';
			return false;
		}

		/* 獲取模型數據 */
		$logic  = $this->logic($info['model_id']);
		$detail = $logic->detail($id); //獲取指定ID的數據
		if(!$detail){
			$this->error = $logic->getError();
			return false;
		}

		return array_merge($info, $detail);
	}

	/**
	 * 返回前壹篇文檔信息
	 * @param  array $info 當前文檔信息
	 * @return array
	 */
	public function prev($info){
		$map = array(
			'id'          => array('lt', $info['id']),
			'pid'		  => 0,
			'category_id' => $info['category_id'],
			'status'      => 1,
		);

		/* 返回前壹條數據 */
		return $this->field(true)->where($map)->order('id DESC')->find();
	}

	/**
	 * 獲取下壹篇文檔基本信息
	 * @param  array    $info 當前文檔信息
	 * @return array
	 */
	public function next($info){
		$map = array(
			'id'          => array('gt', $info['id']),
			'pid'		  => 0,
			'category_id' => $info['category_id'],
			'status'      => 1,
		);

		/* 返回下壹條數據 */
		return $this->field(true)->where($map)->order('id')->find();
	}

	public function update(){
		/* 檢查文檔類型是否符合要求 */
		$Model = new \Admin\Model\DocumentModel();
		$res = $Model->checkDocumentType( I('type'), I('pid') );
		if(!$res['status']){
			$this->error = $res['info'];
			return false;
		}

		/* 獲取數據對象 */
		$data = $this->field('pos,display', true)->create();
		if(empty($data)){
			return false;
		}

		/* 添加或新增基礎內容 */
		if(empty($data['id'])){ //新增數據
			$id = $this->add(); //添加基礎內容

			if(!$id){
				$this->error = '添加基礎內容出錯！';
				return false;
			}
			$data['id'] = $id;
		} else { //更新數據
			$status = $this->save(); //更新基礎內容
			if(false === $status){
				$this->error = '更新基礎內容出錯！';
				return false;
			}
		}

		/* 添加或新增擴展內容 */
		$logic = $this->logic($data['model_id']);
		if(!$logic->update($data['id'])){
			if(isset($id)){ //新增失敗，刪除基礎數據
				$this->delete($data['id']);
			}
			$this->error = $logic->getError();
			return false;
		}

		//內容添加或更新完成
		return $data;

	}

	/**
	 * 獲取段落列表
	 * @param  integer $id    文檔ID
	 * @param  integer $page  顯示頁碼
	 * @param  boolean $field 查詢字段
	 * @param  boolean $logic 是否查詢模型數據
	 * @return array
	 */
	public function part($id, $page = 1, $field = true, $logic = true){
		$map  = array('status' => 1, 'pid' => $id, 'type' => 3);
		$info = $this->field($field)->where($map)->page($page, 10)->order('id')->select();
		if(!$info) {
			$this->error = '該文檔沒有段落！';
			return false;
		}

		/* 不獲取內容詳情 */
		if(!$logic){
			return $info;
		}

		/* 獲取內容詳情 */
		$model = $logic = array();
		foreach ($info as $value) {
			$model[$value['model_id']][] = $value['id'];
		}
		foreach ($model as $model_id => $ids) {
			$data   = $this->logic($model_id)->lists($ids);
			$logic += $data;
		}

		/* 合並數據 */
		foreach ($info as &$value) {
			$value = array_merge($value, $logic[$value['id']]);
		}

		return $info;
	}

	/**
	 * 獲取指定文檔的段落總數
	 * @param  number $id 段落ID
	 * @return number     總數
	 */
	public function partCount($id){
		$map = array('status' => 1, 'pid' => $id, 'type' => 3);
		return $this->where($map)->count('id');
	}

	/**
	 * 獲取推薦位數據列表
	 * @param  number  $pos      推薦位 1-列表推薦，2-頻道頁推薦，4-首頁推薦
	 * @param  number  $category 分類ID
	 * @param  number  $limit    列表行數
	 * @param  boolean $filed    查詢字段
	 * @return array             數據列表
	 */
	public function position($pos, $category = null, $limit = null, $field = true){
		$map = $this->listMap($category, 1, $pos);

		/* 設置列表數量 */
		is_numeric($limit) && $this->limit($limit);

		/* 讀取數據 */
		return $this->field($field)->where($map)->select();
	}

	/**
     * 獲取數據狀態
     * @return integer 數據狀態
     * @author huajie <banhuajie@163.com>
     */
    protected function getStatus(){
        $cate = I('post.category_id');
        $check = M('Category')->getFieldById($cate, 'check');
        if($check){
            $status = 2;
        }else{
        	$status = 1;
        }
        return $status;
    }

    /**
     * 獲取根節點id
     * @return integer 數據id
     * @author huajie <banhuajie@163.com>
     */
    protected function getRoot(){
    	$pid = I('post.pid');
    	if($pid == 0){
    		return 0;
    	}
    	$p_root = $this->getFieldById($pid, 'root');
    	return $p_root == 0 ? $pid : $p_root;
    }

	/**
	 * 驗證分類是否允許發布內容
	 * @param  integer $id 分類ID
	 * @return boolean     true-允許發布內容，false-不允許發布內容
	 */
	protected function checkCategory($id){
		if(is_array($id)){
			$type = get_category($id['category_id'], 'type');
			return in_array($id['type'], $type);
		} else {
			$publish = get_category($id, 'allow_publish');
			return $publish ? true : false;
		}
	}

	/**
	 * 檢測分類是否綁定了指定模型
	 * @param  array $info 模型ID和分類ID數組
	 * @return boolean     true-綁定了模型，false-未綁定模型
	 */
	protected function checkModel($info){
		$model = get_category($info['category_id'], 'model');
		return in_array($info['model_id'], $model);
	}

	/**
	 * 獲取擴展模型對象
	 * @param  integer $model 模型編號
	 * @return object         模型對象
	 */
	private function logic($model){
		return D(get_document_model($model, 'name'), 'Logic');
	}

	/**
	 * 設置where查詢條件
	 * @param  number  $category 分類ID
	 * @param  number  $pos      推薦位
	 * @param  integer $status   狀態
	 * @return array             查詢條件
	 */
	private function listMap($category, $status = 1, $pos = null){
		/* 設置狀態 */
		$map = array('status' => $status, 'pid' => 0);

		/* 設置分類 */
		if(!is_null($category)){
			if(is_numeric($category)){
				$map['category_id'] = $category;
			} else {
				$map['category_id'] = array('in', str2arr($category));
			}
		}

		$map['create_time'] = array('lt', NOW_TIME);
		$map['_string']     = 'deadline = 0 OR deadline > ' . NOW_TIME;

		/* 設置推薦位 */
		if(is_numeric($pos)){
			$map[] = "position & {$pos} = {$pos}";
		}

		return $map;
	}
        
        
    /*
     * 通過模型查詢數組
     */
     public function getlist($cate_id,$model_id,$order = '`id` DESC', $status = 1, $field = true){
         return $this->field($field)->where(array('category_id'=>$cate_id,'model_id'=>$model_id,'status'=>$status))->order($order)->select();
     }
   
             
   public function getlists($logic,$category=null,$field=FALSE,$order = "aiyue_document.level desc,aiyue_document.id desc") {
    $map = $this->listMap($category);
    $list = $this->join($logic .' ON  __DOCUMENT__.id =' . $logic . '.id')->field($field)->where($map)->order($order)->select();
     
    return $list;
    } 

}