<?php

// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;

use OT\DataDictionary;

/**
 * 前台首页控制器
 * 主要获取首页聚合数据
 */
class IndexController extends HomeController {

    //系统首页
    public function index() {
        //愛樂連鎖詳細頁
        if (IS_POST) {
            $list = D('Document')->detail($_POST['id']);
            foreach ($list as $key => $value) {
                if ($key == "images") {
                    $value = explode(",", $value);
                    foreach ($value as $k => $v) {
                        $value[$k] = __ROOT__ . get_cover($v, $field = path);
                    }
                    $list[$key] = $value;
                }
            }
            $this->ajaxReturn($list, 'json');
        }

     //首頁logo
        $logos = D('Document')->getlist(61, 8, '`id` DESC', '1', 'id');

        $logo = array_slice($logos, 0, 2);

        foreach ($logo as $k => $v) {

            $logoarr[] = D('Document')->detail($v['id']);

        }

        $this->assign('logos', $logoarr);
        
       //背景圖
       $list3 = M('Document_bg')->find();
       $this->assign('bg',$list3);
     
       //經營列表
        $lists = D('Document')->getlist(61, 7); 

        $arr = array();

        foreach ($lists as $key => $val) {

            $arr[] = D('Document')->detail($val['id']);

        }

        $this->assign('lists', array_slice($arr, 0, 6));
        $this->assign('list2',explode('|', $arr[0]['content']));
     
        //首頁推廣
        $pro=D('Document')->lists(61);
        foreach($pro as $k=>$v){
            $pros[]=D('Document')->detail($v['id']);
        }
        $this->assign('pros',$pros[0]);
        
        
        //廣告
        $advs = D('Document')->getlist(59,23);
        foreach ($advs as $k => $v) {
            $adv[] = D('Document')->detail($v['id']);
        }
     
        $this->assign('advs', $adv);
        

        //特色
        $chars = D('Document')->getlists("__DOCUMENT_CHARACTERISTIC__",58);
        
        $this->assign('chats',array_splice($chars,1));
        $this->assign('chatsf',$chars[0]); 
     


        //愛樂連鎖
        $store = D('Document')->getlist(60, 12 , 'level desc');
    
        $arr4 = array();
        foreach ($store as $key => $val) {
            $arr4[] = D('Document')->detail($val['id']);
        }
        
        
        
        
        $this->assign("store", $arr4);
        $this->display();
    }

    //关于爱乐
    public function about() {
        $lists = D('Document')->lists(40);
        $arr = array();
        foreach ($lists as $key => $val) {
            $arr[] = D('Document')->detail($val['id']);
        }
        $this->assign('list', $arr[0]);
        $this->display();
    }

    //音樂教程
    public function teach_music() {
        //導師介紹
        $arr = D('Document')->getlist(50, 9);
        foreach ($arr as $key => $value) {
            $arr1[] = D('Document')->detail($value['id']);
        }
        $this->assign("tmusic", $arr1[0]);

        //课程简介
        $courseDetal = D('Document')->getlist(75, 25);
        
        
        $this->assign("CateGoryList",$courseDetal[0]);
        
        
        
        
        //課程介紹
        $course = D('Document')->getlist(62, 14);
        foreach ($course as $key => $value) {
            $courses[] = D('Document')->detail($value['id']);
        }
        foreach ($courses as $k => $v) {
            foreach ($v as $ko => $vo) {
                if ($ko == "address" || $ko == "num" || $ko == "time" || $ko == "grade" || $ko == "Hallnum") {
                    if (strpos($vo, ',')) {
                        $vo = explode(",", $vo);
                    } elseif (strpos($vo, '，')) {
                        $vo = explode("，", $vo);
                    }
                    $v[$ko] = $vo;
                }
                $courses[$k] = $v;
            }
        }

   

        $this->assign('courses', $courses);
        $this->display();
    }

    //樂器教學
    public function teach_instru() {
        //導師介紹
        $arr = D('Document')->getlist(51, 9);

        foreach ($arr as $key => $value) {
  
            $arr1[] = D('Document')->detail($value['id']);
           
        }
        //课程简介
        $courseDetal = D('Document')->getlist(76, 25);
        
        //dump($courseDetal);
        $this->assign("CateGoryList",$courseDetal[0]);
        
        $this->assign("tinstru", $arr1[0]);

        //課程介紹
        $course = D('Document')->getlist(63, 14);
        foreach ($course as $key => $value) {
            $courses[] = D('Document')->detail($value['id']);
        }
        foreach ($courses as $k => $v) {
            foreach ($v as $ko => $vo) {
                if ($ko == "address" || $ko == "num" || $ko == "coefficient" || $ko == "grade" || $ko == "gCoefficient" || $ko == "Hallnum") {
                    if (strpos($vo, ',')) {
                        $vo = explode(",", $vo);
                    } elseif (strpos($vo, '，')) {
                        $vo = explode("，", $vo);
                    }

                    $v[$ko] = $vo;
                }
                $courses[$k] = $v;
            }
        }

        
        $this->assign('courses', $courses);
        $this->display();
    }

    //樂器
    public function qin() {
        //樂器名稱
        $arr = D('Document')->lists(52);
        foreach ($arr as $key => $value) {
            $arr1[] = D('Document')->detail($value['id']);
        }
        $this->assign("instru", $arr1);

        //樂器圖片
        $image = D('Document')->lists(71);
        foreach ($image as $k => $v) {
            $images[] = D('Document')->detail($v['id']);
        }
        $this->assign('images', $images);

        //其他服務
        $service = D('Document')->lists(72);
        foreach ($service as $ko => $vo) {
            $services[] = D('Document')->detail($vo['id']);

            foreach ($services[$ko] as $k1 => $v1) {
                if (strpos($v1, ';')) {
                    $services[$ko][$k1] = explode(";", $v1);
                } elseif (strpos($v1, '；')) {
                    $services[$ko][$k1] = explode("；", $v1);
                }
            }
        }
        
                        //课程简介
        $courseDetal = D('Document')->getlist(79,25);
        
        $this->assign("CateGoryList",$courseDetal[0]);
        $this->assign('services', $services[0]);
        $this->display();
    }

    //租練
    public function rent() {
        //房間類型
        $arr = D('Document')->lists(53);
       $arr1 = array();
       foreach ($arr as $key => $value) {
          $arr1[] = D('Document')->detail($value['id']);
          }
       $this->assign("rent", $arr1);
     

      //租琴房價格
       $rent = D('Document')->lists(64, $order = '`id` DESC', $status = 1, $field = 'id');
       foreach ($rent as $k => $v) {
           $rents[] = D('Document')->detail($v['id']);
           foreach ($rents[$k] as $ko => $vo) { 
              
              if (strpos($vo, '；')) {
                   $rents[$k][$ko] = explode("；", $vo);
               } else if (strpos($vo, ';')) {
                   $rents[$k][$ko] = explode(";", $vo);
              }
           }
       }      
       $this->assign("rents", $rents);
      $house=D('Document')->lists(73, $order = '`id` DESC', $status = 1, $field = 'id');
       foreach($house as $key=>$value){
           $houses[]=D('Document')->detail($value['id']);
       }
        
       //if(isset($_SESSION['user']))
    $var=[];
   foreach ($houses as $value) {
     $var[$value['id']]['a']=$value;
   }
   foreach ($rents as $value) {
       
       $var[$value['house']]['list'][]=$value;
   }
   
                           //课程简介
       $courseDetal = D('Document')->getlist(80,25);
       
       $this->assign("CateGoryList",$courseDetal[0]);
        $this->assign('array',$var);
      
        $this->display();    
     
    }
    

    //聯繫我們
    public function contact() {
        $arr = D('Document')->getlist(54, 5);
        $arr1 = array();
        foreach ($arr as $key => $value) {
            $arr1[] = D('Document')->detail($value['id']);
        }
        $this->assign("contact", array_slice($arr1, 0, 4));
        $contact = D('Document')->lists(46);
        foreach ($contact as $k => $v) {
            $contacts[] = D('Document')->detail($v['id']);
        }
        foreach ($contacts as $key => $value) {
            foreach ($value as $ko => $vo) {
                if ($ko == 'images') {
                    $vo = explode(',', $vo);
                }
                $value[$ko] = $vo;
            }
            $contacts[$key] = $value;
        }
        $this->assign('contacts', $contacts);
        $this->display();
    }

    //音樂考試
    public function exam() {
        $exams = D('Document')->lists(45);
        
        //课程简介
        $courseDetal = D('Document')->getlist(77, 25);
        
        //dump($courseDetal);
        $this->assign("CateGoryList",$courseDetal[0]);
        
        $this->assign("tinstru", $arr1[0]);
        
        
        
        $arr = array();
        foreach ($exams as $key => $val) {
            $arr[] = D('Document')->detail($val['id']);
        }
        $this->assign('exam', $arr[0]);
     
        $this->display();
    }

    //学生守则
    public function rules() {
        $lists = D('Document')->lists(47);
        $arr = array();
        foreach ($lists as $key => $val) {
            $arr[] = D('Document')->detail($val['id']);
        }
        $this->assign('rules', $arr[0]);

        $this->display();
    }

    //留言板
    public function fback() {
      
      
      
        $data = array(
            'name' => $_POST['contacts'],
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
            'content' => $_POST['content'],
            'time' => time(),
            'status' => 1,
        );
        if ($_POST['contacts'] == '' || $_POST['phone'] == '' || $_POST['email'] == '' || $_POST['content'] == '') {
            $this->error('请输入完整信息');
        }
        $id = D('Fback')->update($data);
        $mail_content = "<p>联系人:".$data['name']."</p>".
                        "<p>联系电话:".$data['phone']."</p>".
                        "<p>电子邮箱:".$data['email']."</p>".
                        "<p>内容:".$data['content']."</p>";
         sendMail(C('MAIL_SANDNAME'),C('MAIL_TITLE'), $mail_content ); 
        if (!$id) {
            $this->error('發送失敗，請重新發送');
        } else {
          
                
           $this->success('發送成功，已通知商家');
           
        }
    }
    /* 会员中心 */
    public function member(){
        $ID = session('menber_auth.id');
        if(empty($ID)){
             $this->redirect('Index/index');
        }
        
        $options['memberID'] = session('menber_auth.id');
        
        
        
        $appointmentList =  M('appointment')->where($options)->order('id asc')->select();
        $memberNoticeList = M("Member_notice")->where(array("uid"=>session('menber_auth.id')))->order('time desc')->select();
        
        
        
        $this->assign("appList",$appointmentList);
        $this->assign("NoticeList",$memberNoticeList);
        

        
        $this->display();
    }
    /*会员通知*/
    public function notice(){
         $ID = session('menber_auth.id');
        if(empty($ID)){
            $this->redirect('Index/index');
        }
        
        $NoticeRow = M('Member_notice')->where(array("id"=>$_GET['id']))->find();

        $this->assign("data",$NoticeRow);
  
        $this->display();
    }

        //地圖
    public function map() {

        $this->assign('data', $_GET);
        $this->display();
    }

    //預約
    public function appointment() {
        

        
        $ID = session("menber_auth");
        $app =M('appointment'); 
       
        $data = $app->create($data);
        //dump($data);die();
      
        if(isset($ID))
        {
           $data['memberID'] = session("menber_auth.id");    
        }    
        $result = $app->add($data);
        if ($result) {
            $classname = M('appointment')->where(array('id'=>$result))->find();
            $mail = M('Document')->where(array('title'=>$classname['address'],'category_id'=>74))->find();
            $content = "<p>課程名稱：".$data['course']."</p>".
                       "<p>門店：".$data['address']."</p>".
                       "<p>人數:".$data['number']."</p>".
                       "<p>級別:".$data['level']."</p>".
                       "<p>年級:".$data['class']."</p>".
                       "<p>時間:".$data['time']."</p>".
                       "<p>價格:".$data['price']."</p>".
                       "<p>姓名:".$data['name']."</p>".
                       "<p>聯繫電話:".$data['mobile']."</p>".
                       "<p>郵箱地址:".$data['email']."</p>".
                       "<p>年齡:".$data['age']."</p>";
            sendMail($mail['emails'],'愛樂琴行 -- 預約通知',$content);
            $this->success("預約成功");
        } else {
            $this->error("此課程你已預約過");
        }
        
        
       
    }

    //得到價格
    public function priceAjax() {
        $data = M('document_courselist')->create();
  
        $price = M('document_courselist')->where($data)->find()['price'];
        $this->ajaxReturn($price, 'JSON');
    }
    
    //得到租練價格
    public function rentAjax(){
        $data['id']=$_POST['id'];     
        if(isset($_SESSION['user'])){
            $rprice = M('document_rent')->where($data)->find()['vipprice'];
        }else{
            $rprice = M('document_rent')->where($data)->find()['price'];
        }
         $rprice = explode("；", $rprice);
         $this->ajaxReturn($rprice, 'JSON');
    }

    //忘記密碼
    public function misspwd(){
        $this->error('請聯繫商家');
    }
    
    public function addetail(){
        $id = I('get.id');
        $data = D('Document')->detail($id);
        $this->assign('list',$data);
        
        $this->display('Index/addetail');
    }
    public function others(){
         
        $data = D('Document')->lists(78);
        $this->assign('data',$data[0]);
        $this->display('Index/others');
    }
    
    public function getdetail(){
        
        $id = I('post.id');
        if(isset($id))
        {    
        $data =D('Document')->detail($id);
        $path = explode(',',$data['images']);
        foreach($path as $val)
        {
          $paths['path'][] = get_cover($val)["path"]; 
        }
        $data = array_merge($data,$paths);
        $this->ajaxReturn($data);        
        }
    }
}
