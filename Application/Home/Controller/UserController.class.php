<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Home\Controller;

use OT\DataDictionary;

class UserController extends HomeController {
    
    /* 空操作，用于输出404页面 */
    public function _empty(){
	$this->redirect('Index/index');
    }
        
    public function _initialize() {
         /* 读取站点配置 */
        $config = api('Config/lists');
        C($config); //添加配置

        if(!C('WEB_SITE_CLOSE')){
            $this->error('站点已经关闭，请稍后访问~');
        }
        
    }
    /* 用户登陆 */
    public function login(){
         if (IS_POST) {
         
             $data['uids']    = trim($_POST['usernameSignin']);  //用户名
             $data['password']  = md5($_POST['passwordSignin']);//密码
             
             $menberInfo = M("Member_info")->where($data)->find();
             
             if(!empty($menberInfo)){
                 if($menberInfo['status'] == "-1"){
                      $this->error("用戶不存在或被禁用！");
                 }else {
                   session("menber_auth",$menberInfo);
                   $this->success('登錄成功！');
                 }
             }else {
                $this->error("用戶名或密碼錯誤！",true);
             }
         }
        
        
    }
    /* 用户退出 */
    public function logout(){
         session('menber_auth',null);
         
         
         
         
         $this->success('退出成功！',U('Home/Index/index'));
    }
}