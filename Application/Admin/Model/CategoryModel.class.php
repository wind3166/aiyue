<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 分類模型
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class CategoryModel extends Model{

    protected $_validate = array(
        array('name', 'require', '標識不能為空', self::EXISTS_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '', '標識已經存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
        array('title', 'require', '名稱不能為空', self::MUST_VALIDATE , 'regex', self::MODEL_BOTH),
    	array('meta_title', '1,50', '網頁標題不能超過50個字符', self::VALUE_VALIDATE , 'length', self::MODEL_BOTH),
    	array('keywords', '1,255', '網頁關鍵字不能超過255個字符', self::VALUE_VALIDATE , 'length', self::MODEL_BOTH),
    	array('meta_title', '1,255', '網頁描述不能超過255個字符', self::VALUE_VALIDATE , 'length', self::MODEL_BOTH),
    );

    protected $_auto = array(
        array('model', 'arr2str', self::MODEL_BOTH, 'function'),
        array('model', null, self::MODEL_BOTH, 'ignore'),
        array('type', 'arr2str', self::MODEL_BOTH, 'function'),
        array('type', null, self::MODEL_BOTH, 'ignore'),
        array('reply_model', 'arr2str', self::MODEL_BOTH, 'function'),
        array('reply_model', null, self::MODEL_BOTH, 'ignore'),
        array('extend', 'json_encode', self::MODEL_BOTH, 'function'),
        array('extend', null, self::MODEL_BOTH, 'ignore'),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_BOTH),
    );


    /**
     * 獲取分類詳細信息
     * @param  milit   $id 分類ID或標識
     * @param  boolean $field 查詢字段
     * @return array     分類信息
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function info($id, $field = true){
        /* 獲取分類信息 */
        $map = array();
        if(is_numeric($id)){ //通過ID查詢
            $map['id'] = $id;
        } else { //通過標識查詢
            $map['name'] = $id;
        }
        return $this->field($field)->where($map)->find();
    }

    /**
     * 獲取分類樹，指定分類則返回指定分類極其子分類，不指定則返回所有分類樹
     * @param  integer $id    分類ID
     * @param  boolean $field 查詢字段
     * @return array          分類樹
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function getTree($id = 0, $field = true){
        /* 獲取當前分類信息 */
        if($id){
            $info = $this->info($id);
            $id   = $info['id'];
        }

        /* 獲取所有分類 */
        $map  = array('status' => array('gt', -1));
        $list = $this->field($field)->where($map)->order('sort')->select();
        $list = list_to_tree($list, $pk = 'id', $pid = 'pid', $child = '_', $root = $id);

        /* 獲取返回數據 */
        if(isset($info)){ //指定分類則返回當前分類極其子分類
            $info['_'] = $list;
        } else { //否則返回所有分類
            $info = $list;
        }

        return $info;
    }

    /**
     * 獲取指定分類的同級分類
     * @param  integer $id    分類ID
     * @param  boolean $field 查詢字段
     * @return array
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function getSameLevel($id, $field = true){
        $info = $this->info($id, 'pid');
        $map = array('pid' => $info['pid'], 'status' => 1);
        return $this->field($field)->where($map)->order('sort')->select();
    }

    /**
     * 更新分類信息
     * @return boolean 更新狀態
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function update(){
        $data = $this->create();
        if(!$data){ //數據對象創建錯誤
            return false;
        }

        /* 添加或更新數據 */
        if(empty($data['id'])){
            $res = $this->add();
        }else{
            $res = $this->save();
        }

        //更新分類緩存
        S('sys_category_list', null);

        //記錄行為
        action_log('update_category', 'category', $data['id'] ? $data['id'] : $res, UID);

        return $res;
    }

    /**
     * 查詢後解析擴展信息
     * @param  array $data 分類數據
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    protected function _after_find(&$data, $options){
        /* 分割模型 */
        if(!empty($data['model'])){
            $data['model'] = explode(',', $data['model']);
        }

        /* 分割文檔類型 */
        if(!empty($data['type'])){
            $data['type'] = explode(',', $data['type']);
        }

        /* 分割模型 */
        if(!empty($data['reply_model'])){
            $data['reply_model'] = explode(',', $data['reply_model']);
        }

        /* 分割文檔類型 */
        if(!empty($data['reply_type'])){
            $data['reply_type'] = explode(',', $data['reply_type']);
        }

        /* 還原擴展數據 */
        if(!empty($data['extend'])){
            $data['extend'] = json_decode($data['extend'], true);
        }
    }
       
}