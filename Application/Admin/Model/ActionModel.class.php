<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 行為模型
 * @author huajie <banhuajie@163.com>
 */

class ActionModel extends Model {

    /* 自動驗證規則 */
    protected $_validate = array(
        array('name', 'require', '行為標識必須', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '/^[a-zA-Z]\w{0,39}$/', '標識不合法', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '', '標識已經存在', self::MUST_VALIDATE, 'unique', self::MODEL_BOTH),
        array('title', 'require', '標題不能為空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('title', '1,80', '標題長度不能超過80個字符', self::MUST_VALIDATE, 'length', self::MODEL_BOTH),
        array('remark', 'require', '行為描述不能為空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('remark', '1,140', '行為描述不能超過140個字符', self::MUST_VALIDATE, 'length', self::MODEL_BOTH),
    );

    /* 自動完成規則 */
    protected $_auto = array(
        array('status', 1, self::MODEL_INSERT, 'string'),
        array('update_time', 'time', self::MODEL_BOTH, 'function'),
    );

    /**
     * 新增或更新壹個行為
     * @return boolean fasle 失敗 ， int  成功 返回完整的數據
     * @author huajie <banhuajie@163.com>
     */
    public function update(){
        /* 獲取數據對象 */
        $data = $this->create($_POST);
        if(empty($data)){
            return false;
        }

        /* 添加或新增行為 */
        if(empty($data['id'])){ //新增數據
            $id = $this->add(); //添加行為
            if(!$id){
                $this->error = '新增行為出錯！';
                return false;
            }
        } else { //更新數據
            $status = $this->save(); //更新基礎內容
            if(false === $status){
                $this->error = '更新行為出錯！';
                return false;
            }
        }
        //刪除緩存
        S('action_list', null);

        //內容添加或更新完成
        return $data;

    }

}