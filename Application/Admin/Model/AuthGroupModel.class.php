<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 用戶組模型類
 * Class AuthGroupModel 
 * @author 朱亞傑 <zhuyajie@topthink.net>
 */
class AuthGroupModel extends Model {
    const TYPE_ADMIN                = 1;                   // 管理員用戶組類型標識
    const MEMBER                    = 'member';
    const UCENTER_MEMBER            = 'ucenter_member';
    const AUTH_GROUP_ACCESS         = 'auth_group_access'; // 關系表表名
    const AUTH_EXTEND               = 'auth_extend';       // 動態權限擴展信息表
    const AUTH_GROUP                = 'auth_group';        // 用戶組表名
    const AUTH_EXTEND_CATEGORY_TYPE = 1;              // 分類權限標識
    const AUTH_EXTEND_MODEL_TYPE    = 2; //分類權限標識

    protected $_validate = array(
        array('title','require', '必須設置用戶組標題', Model::MUST_VALIDATE ,'regex',Model::MODEL_INSERT),
        //array('title','require', '必須設置用戶組標題', Model::EXISTS_VALIDATE  ,'regex',Model::MODEL_INSERT),
        array('description','0,80', '描述最多80字符', Model::VALUE_VALIDATE , 'length'  ,Model::MODEL_BOTH ),
       // array('rules','/^(\d,?)+(?<!,)$/', '規則數據不合法', Model::VALUE_VALIDATE , 'regex'  ,Model::MODEL_BOTH ),
    );

    /**
     * 返回用戶組列表
     * 默認返回正常狀態的管理員用戶組列表
     * @param array $where   查詢條件,供where()方法使用
     *
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    public function getGroups($where=array()){
        $map = array('status'=>1,'type'=>self::TYPE_ADMIN,'module'=>'admin');
        $map = array_merge($map,$where);
        return $this->where($map)->select();
    }

    /**
     * 把用戶添加到用戶組,支持批量添加用戶到用戶組
     * @author 朱亞傑 <zhuyajie@topthink.net>
     * 
     * 示例: 把uid=1的用戶添加到group_id為1,2的組 `AuthGroupModel->addToGroup(1,'1,2');`
     */
    public function addToGroup($uid,$gid){
        $uid = is_array($uid)?implode(',',$uid):trim($uid,',');
        $gid = is_array($gid)?$gid:explode( ',',trim($gid,',') );

        $Access = M(self::AUTH_GROUP_ACCESS);
        if( isset($_REQUEST['batch']) ){
            //為單個用戶批量添加用戶組時,先刪除舊數據
            $del = $Access->where( array('uid'=>array('in',$uid)) )->delete();
        }

        $uid_arr = explode(',',$uid);
		$uid_arr = array_diff($uid_arr,array(C('USER_ADMINISTRATOR')));
        $add = array();
        if( $del!==false ){
            foreach ($uid_arr as $u){
                foreach ($gid as $g){
                    if( is_numeric($u) && is_numeric($g) ){
                        $add[] = array('group_id'=>$g,'uid'=>$u);
                    }
                }
            }
            $Access->addAll($add);
        }
        if ($Access->getDbError()) {
            if( count($uid_arr)==1 && count($gid)==1 ){
                //單個添加時定制錯誤提示
                $this->error = "不能重復添加";
            }
            return false;
        }else{
            return true;
        }
    }

    /**
     * 返回用戶所屬用戶組信息
     * @param  int    $uid 用戶id
     * @return array  用戶所屬的用戶組 array(
     *                                         array('uid'=>'用戶id','group_id'=>'用戶組id','title'=>'用戶組名稱','rules'=>'用戶組擁有的規則id,多個,號隔開'),
     *                                         ...)   
     */
    static public function getUserGroup($uid){
        static $groups = array();
        if (isset($groups[$uid]))
            return $groups[$uid];
        $prefix = C('DB_PREFIX');
        $user_groups = M()
            ->field('uid,group_id,title,description,rules')
            ->table($prefix.self::AUTH_GROUP_ACCESS.' a')
            ->join ($prefix.self::AUTH_GROUP." g on a.group_id=g.id")
            ->where("a.uid='$uid' and g.status='1'")
            ->select();
        $groups[$uid]=$user_groups?$user_groups:array();
        return $groups[$uid];
    }
    
    /**
     * 返回用戶擁有管理權限的擴展數據id列表
     * 
     * @param int     $uid  用戶id
     * @param int     $type 擴展數據標識
     * @param int     $session  結果緩存標識
     * @return array
     *  
     *  array(2,4,8,13) 
     *
     * @author 朱亞傑 <xcoolcc@gmail.com>
     */
    static public function getAuthExtend($uid,$type,$session){
        if ( !$type ) {
            return false;
        }
        if ( $session ) {
            $result = session($session);
        }
        if ( $uid == UID && !empty($result) ) {
            return $result;
        }
        $prefix = C('DB_PREFIX');
        $result = M()
            ->table($prefix.self::AUTH_GROUP_ACCESS.' g')
            ->join($prefix.self::AUTH_EXTEND.' c on g.group_id=c.group_id')
            ->where("g.uid='$uid' and c.type='$type' and !isnull(extend_id)")
            ->getfield('extend_id',true);
        if ( $uid == UID && $session ) {
            session($session,$result);
        }
        return $result;
    }

    /**
     * 返回用戶擁有管理權限的分類id列表
     * 
     * @param int     $uid  用戶id
     * @return array
     *  
     *  array(2,4,8,13) 
     *
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    static public function getAuthCategories($uid){
        return self::getAuthExtend($uid,self::AUTH_EXTEND_CATEGORY_TYPE,'AUTH_CATEGORY');
    }



    /**
     * 獲取用戶組授權的擴展信息數據
     * 
     * @param int     $gid  用戶組id
     * @return array
     *  
     *  array(2,4,8,13) 
     *
     * @author 朱亞傑 <xcoolcc@gmail.com>
     */
    static public function getExtendOfGroup($gid,$type){
        if ( !is_numeric($type) ) {
            return false;
        }
        return M(self::AUTH_EXTEND)->where( array('group_id'=>$gid,'type'=>$type) )->getfield('extend_id',true);
    }

    /**
     * 獲取用戶組授權的分類id列表
     * 
     * @param int     $gid  用戶組id
     * @return array
     *  
     *  array(2,4,8,13) 
     *
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    static public function getCategoryOfGroup($gid){
        return self::getExtendOfGroup($gid,self::AUTH_EXTEND_CATEGORY_TYPE);
    }
    

    /**
     * 批量設置用戶組可管理的擴展權限數據
     *
     * @param int|string|array $gid   用戶組id
     * @param int|string|array $cid   分類id
     * 
     * @author 朱亞傑 <xcoolcc@gmail.com>
     */
    static public function addToExtend($gid,$cid,$type){
        $gid = is_array($gid)?implode(',',$gid):trim($gid,',');
        $cid = is_array($cid)?$cid:explode( ',',trim($cid,',') );

        $Access = M(self::AUTH_EXTEND);
        $del = $Access->where( array('group_id'=>array('in',$gid),'type'=>$type) )->delete();

        $gid = explode(',',$gid);
        $add = array();
        if( $del!==false ){
            foreach ($gid as $g){
                foreach ($cid as $c){
                    if( is_numeric($g) && is_numeric($c) ){
                        $add[] = array('group_id'=>$g,'extend_id'=>$c,'type'=>$type);
                    }
                }
            }
            $Access->addAll($add);
        }
        if ($Access->getDbError()) {
            return false;
        }else{
            return true;
        }
    }

    /**
     * 批量設置用戶組可管理的分類
     *
     * @param int|string|array $gid   用戶組id
     * @param int|string|array $cid   分類id
     * 
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    static public function addToCategory($gid,$cid){
        return self::addToExtend($gid,$cid,self::AUTH_EXTEND_CATEGORY_TYPE);
    }


    /**
     * 將用戶從用戶組中移除
     * @param int|string|array $gid   用戶組id
     * @param int|string|array $cid   分類id
     * @author 朱亞傑 <xcoolcc@gmail.com>
     */
    public function removeFromGroup($uid,$gid){
        return M(self::AUTH_GROUP_ACCESS)->where( array( 'uid'=>$uid,'group_id'=>$gid) )->delete();
    }

    /**
     * 獲取某個用戶組的用戶列表
     *
     * @param int $group_id   用戶組id
     * 
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    static public function memberInGroup($group_id){
        $prefix   = C('DB_PREFIX');
        $l_table  = $prefix.self::MEMBER;
        $r_table  = $prefix.self::AUTH_GROUP_ACCESS;
        $r_table2 = $prefix.self::UCENTER_MEMBER;
        $list     = M() ->field('m.uid,u.username,m.last_login_time,m.last_login_ip,m.status')
                       ->table($l_table.' m')
                       ->join($r_table.' a ON m.uid=a.uid')
                       ->join($r_table2.' u ON m.uid=u.id')
                       ->where(array('a.group_id'=>$group_id))
                       ->select();
        return $list;
    }

    /**
     * 檢查id是否全部存在
     * @param array|string $gid  用戶組id列表
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    public function checkId($modelname,$mid,$msg = '以下id不存在:'){
        if(is_array($mid)){
            $count = count($mid);
            $ids   = implode(',',$mid);
        }else{
            $mid   = explode(',',$mid);
            $count = count($mid);
            $ids   = $mid;
        }

        $s = M($modelname)->where(array('id'=>array('IN',$ids)))->getField('id',true);
        if(count($s)===$count){
            return true;
        }else{
            $diff = implode(',',array_diff($mid,$s));
            $this->error = $msg.$diff;
            return false;
        }
    }

    /**
     * 檢查用戶組是否全部存在
     * @param array|string $gid  用戶組id列表
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    public function checkGroupId($gid){
        return $this->checkId('AuthGroup',$gid, '以下用戶組id不存在:');
    }
    
    /**
     * 檢查分類是否全部存在
     * @param array|string $cid  欄目分類id列表
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    public function checkCategoryId($cid){
        return $this->checkId('Category',$cid, '以下分類id不存在:');
    }


}