<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 文檔基礎模型
 */
class ModelModel extends Model{

    /* 自動驗證規則 */
    protected $_validate = array(
        array('name', 'require', '標識不能為空', self::MUST_VALIDATE , 'regex', self::MODEL_INSERT),
        array('name', '/^[a-zA-Z]\w{0,39}$/', '文檔標識不合法', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '', '標識已經存在', self::VALUE_VALIDATE, 'unique', self::MODEL_BOTH),
        array('title', 'require', '標題不能為空', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('title', '1,30', '標題長度不能超過30個字符', self::MUST_VALIDATE, 'length', self::MODEL_BOTH),
    	array('list_grid', 'require', '列表定義不能為空', self::MUST_VALIDATE , 'regex', self::MODEL_UPDATE),
    );

    /* 自動完成規則 */
    protected $_auto = array(
    	array('name', 'strtolower', self::MODEL_INSERT, 'function'),
        array('create_time', NOW_TIME, self::MODEL_INSERT),
        array('update_time', NOW_TIME, self::MODEL_BOTH),
        array('status', '1', self::MODEL_INSERT, 'string'),
    	array('field_sort', 'getFields', self::MODEL_BOTH, 'callback'),
    );

    /**
     * 新增或更新壹個文檔
     * @return boolean fasle 失敗 ， int  成功 返回完整的數據
     * @author huajie <banhuajie@163.com>
     */
    public function update(){
        /* 獲取數據對象 */
        $data = $this->create();
        if(empty($data)){
            return false;
        }

        /* 添加或新增基礎內容 */
        if(empty($data['id'])){ //新增數據
            $id = $this->add(); //添加基礎內容
            if(!$id){
                $this->error = '新增模型出錯！';
                return false;
            }
        } else { //更新數據
            $status = $this->save(); //更新基礎內容
            if(false === $status){
                $this->error = '更新模型出錯！';
                return false;
            }
        }
		// 清除模型緩存數據
		S('DOCUMENT_MODEL_LIST', null);

		//記錄行為
		action_log('update_model','model',$data['id'] ? $data['id'] : $id,UID);

        //內容添加或更新完成
        return $data;
    }

    /**
     * 處理字段排序數據
     * @author huajie <banhuajie@163.com>
     */
    protected function getFields($fields){
    	return empty($fields) ? '' : json_encode($fields);
    }

    /**
     * 獲取指定數據庫的所有表名
     * @author huajie <banhuajie@163.com>
     */
    public function getTables($connection = null){
    	$tables = M()->query('SHOW TABLES;');
    	foreach ($tables as $key=>$value){
    		$tables[$key] = $value['Tables_in_'.C('DB_NAME')];
    	}
    	return $tables;
    }

    /**
     * 根據數據表生成模型及其屬性數據
     * @author huajie <banhuajie@163.com>
     */
    public function generate($table){
    	//新增模型數據
    	$name = substr($table, strlen(C('DB_PREFIX')));
    	$data = array('name'=>$name, 'title'=>$name);
    	$data = $this->create($data);
    	$res = $this->add($data);
    	if(!$res){
    		return false;
    	}

    	//新增屬性
		$fields = M()->query('SHOW FULL COLUMNS FROM '.$table);
		foreach ($fields as $key=>$value){
			//不新增id字段
			if(strcmp($value['Field'], 'id') == 0){
				continue;
			}

			//生成屬性數據
			$data = array();
			$data['name'] = $value['Field'];
			$data['title'] = $value['Comment'];
			$data['type'] = 'string';	//TODO:根據字段定義生成合適的數據類型
			//獲取字段定義
			$is_null = strcmp($value['Null'], 'NO') == 0 ? ' NOT NULL ' : ' NULL ';
			$data['field'] = $value['Type'].$is_null;
			$data['value'] = $value['Default'] == null ? '' : $value['Default'];
			$data['model_id'] = $res;
			$_POST = $data;		//便於自動驗證
			D('Attribute')->update($data, false);
		}
    	return $res;
    }

    /**
     * 刪除壹個模型
     * @param integer $id 模型id
     * @author huajie <banhuajie@163.com>
     */
    public function del($id){
    	//獲取表名
    	$model = $this->field('name')->find($id);
    	$table_name = C('DB_PREFIX').strtolower($model['name']);
    	//刪除屬性數據
    	M('Attribute')->where(array('model_id'=>$id))->delete();
    	//刪除模型數據
    	$this->delete($id);
    	//刪除該表
    	$sql = <<<sql
				DROP TABLE {$table_name};
sql;
    	$res = M()->execute($sql);
    	return $res !== false;
    }
}