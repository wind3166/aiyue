<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * 屬性模型
 * @author huajie <banhuajie@163.com>
 */

class AttributeModel extends Model {

    /* 自動驗證規則 */
    protected $_validate = array(
        array('name', 'require', '字段名必須', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('name', '/^[a-zA-Z][\w_]{1,29}$/', '字段名不合法', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    	array('name', 'checkName', '字段名已存在', self::MUST_VALIDATE, 'callback', self::MODEL_BOTH),
    	array('field', 'require', '字段定義必須', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    	array('field', '1,100', '註釋長度不能超過100個字符', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
        array('title', '1,100', '註釋長度不能超過100個字符', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
        array('remark', '1,100', '備註不能超過100個字符', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH),
    	array('model_id', 'require', '未選擇操作的模型', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /* 自動完成規則 */
    protected $_auto = array(
        array('status', 1, self::MODEL_INSERT, 'string'),
    	array('create_time', 'time', self::MODEL_INSERT, 'function'),
        array('update_time', 'time', self::MODEL_BOTH, 'function'),
    );

    /* 操作的表名 */
    protected $table_name = null;

    /**
     * 新增或更新壹個屬性
     * @return boolean fasle 失敗 ， int  成功 返回完整的數據
     * @author huajie <banhuajie@163.com>
     */
    public function update($data = null, $create = true){
        /* 獲取數據對象 */
    	$data = empty($data) ? $_POST : $data;
        $data = $this->create($data);
        if(empty($data)){
            return false;
        }
        /* 添加或新增屬性 */
        if(empty($data['id'])){ //新增屬性

            $id = $this->add();
            if(!$id){
                $this->error = '新增屬性出錯！';
                return false;
            }

            if($create){
            	//新增表字段
            	$res = $this->addField($data);
            	if(!$res){
            		$this->error = '新建字段出錯！';
            		//刪除新增數據
            		$this->delete($id);
            		return false;
            	}
            }

        } else { //更新數據
        	if($create){
        	//更新表字段
	        	$res = $this->updateField($data);
	        	if(!$res){
	        		$this->error = '更新字段出錯！';
	        		return false;
	        	}
        	}

            $status = $this->save();
            if(false === $status){
                $this->error = '更新屬性出錯！';
                return false;
            }
        }
        //刪除字段緩存文件
        $model_name = M('Model')->field('name')->find($data['model_id']);
        $cache_name = C('DB_NAME').'.'.preg_replace('/\W+|\_+/','',$model_name['name']);
        F($cache_name, null, DATA_PATH.'_fields/');

        //記錄行為
        action_log('update_attribute', 'attribute', $data['id'] ? $data['id'] : $id, UID);

        //內容添加或更新完成
        return $data;

    }

    /**
     * 檢查同壹張表是否有相同的字段
     * @author huajie <banhuajie@163.com>
     */
    protected function checkName(){
    	$name = I('post.name');
    	$model_id = I('post.model_id');
    	$id = I('post.id');
    	$map = array('name'=>$name, 'model_id'=>$model_id);
    	if(!empty($id)){
    		$map['id'] = array('neq', $id);
    	}
    	$res = $this->where($map)->find();
    	return empty($res);
    }

    /**
     * 檢查當前表是否存在
     * @param intger $model_id 模型id
     * @return intger 是否存在
     * @author huajie <banhuajie@163.com>
     */
    protected function checkTableExist($model_id){
    	$Model = M('Model');
    	//當前操作的表
		$model = $Model->where(array('id'=>$model_id))->field('name,extend')->find();

		if($model['extend'] == 0){	//獨立模型表名
			$table_name = $this->table_name = C('DB_PREFIX').strtolower($model['name']);
		}else{						//繼承模型表名
			$extend_model = $Model->where(array('id'=>$model['extend']))->field('name,extend')->find();
			$table_name = $this->table_name = C('DB_PREFIX').strtolower($extend_model['name']).'_'.strtolower($model['name']);
		}
		$sql = <<<sql
				SHOW TABLES LIKE '{$table_name}';
sql;
		$res = M()->query($sql);
		return count($res);
    }

    /**
     * 新建表字段
     * @param array $field 需要新建的字段屬性
     * @return boolean true 成功 ， false 失敗
     * @author huajie <banhuajie@163.com>
     */
    protected function addField($field){
    	//檢查表是否存在
    	$table_exist = $this->checkTableExist($field['model_id']);

    	//獲取默認值
    	if($field['value'] === ''){
    		$default = '';
    	}elseif (is_numeric($field['value'])){
    		$default = ' DEFAULT '.$field['value'];
    	}elseif (is_string($field['value'])){
    		$default = ' DEFAULT \''.$field['value'].'\'';
    	}else {
    		$default = '';
    	}

    	if($table_exist){
    		$sql = <<<sql
				ALTER TABLE `{$this->table_name}`
ADD COLUMN `{$field['name']}`  {$field['field']} {$default} COMMENT '{$field['title']}';
sql;
    	}else{
    		//新建表時是否默認新增“id主鍵”字段
    		$model_info = M('Model')->field('engine_type,need_pk')->getById($field['model_id']);
    		if($model_info['need_pk']){
    			$sql = <<<sql
				CREATE TABLE IF NOT EXISTS `{$this->table_name}` (
				`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主鍵' ,
				`{$field['name']}`  {$field['field']} {$default} COMMENT '{$field['title']}' ,
				PRIMARY KEY (`id`)
				)
				ENGINE={$model_info['engine_type']}
				DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
				CHECKSUM=0
				ROW_FORMAT=DYNAMIC
				DELAY_KEY_WRITE=0
				;
sql;
    		}else{
    			$sql = <<<sql
				CREATE TABLE IF NOT EXISTS `{$this->table_name}` (
				`{$field['name']}`  {$field['field']} {$default} COMMENT '{$field['title']}'
				)
				ENGINE={$model_info['engine_type']}
				DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
				CHECKSUM=0
				ROW_FORMAT=DYNAMIC
				DELAY_KEY_WRITE=0
				;
sql;
    		}

    	}
    	$res = M()->execute($sql);
    	return $res !== false;
    }

    /**
     * 更新表字段
     * @param array $field 需要更新的字段屬性
     * @return boolean true 成功 ， false 失敗
     * @author huajie <banhuajie@163.com>
     */
    protected function updateField($field){
    	//檢查表是否存在
    	$table_exist = $this->checkTableExist($field['model_id']);

    	//獲取原字段名
    	$last_field = $this->getFieldById($field['id'], 'name');

    	//獲取默認值
    	$default = $field['value']!='' ? ' DEFAULT '.$field['value'] : '';

    	$sql = <<<sql
			ALTER TABLE `{$this->table_name}`
CHANGE COLUMN `{$last_field}` `{$field['name']}`  {$field['field']} {$default} COMMENT '{$field['title']}' ;
sql;
    	$res = M()->execute($sql);
    	return $res !== false;
    }

    /**
     * 刪除壹個字段
     * @param array $field 需要刪除的字段屬性
     * @return boolean true 成功 ， false 失敗
     * @author huajie <banhuajie@163.com>
     */
    public function deleteField($field){
    	//檢查表是否存在
    	$table_exist = $this->checkTableExist($field['model_id']);

    	$sql = <<<sql
			ALTER TABLE `{$this->table_name}`
DROP COLUMN `{$field['name']}`;
sql;
    	$res = M()->execute($sql);
    	return $res !== false;
    }

}