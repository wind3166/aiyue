<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;
use Think\Model;

/**
 * URL模型
 * @author huajie <banhuajie@163.com>
 */

class UrlModel extends Model {

    /* 自動驗證規則 */
    protected $_validate = array(
        array('url', 'url', 'URL格式不正確', self::MUST_VALIDATE, 'regex', self::MODEL_BOTH),
        array('short', 'url', 'URL格式不正確', self::VALUE_VALIDATE, 'regex', self::MODEL_BOTH),
    );

    /* 自動完成規則 */
    protected $_auto = array(
        array('status', 1, self::MODEL_INSERT, 'string'),
        array('create_time', 'time', self::MODEL_BOTH, 'function'),
    );

    /**
     * 新增或更新壹個URL
     * @return boolean fasle 失敗 ， 成功 返回完整的數據
     * @author huajie <banhuajie@163.com>
     */
    public function update($data){
        /* 獲取數據對象 */
        $data = empty($data) ? $_POST : $data;
        $data = $this->create($data);
        if(empty($data)){
            return false;
        }

        /* 如果鏈接已存在則直接返回 */
        $info = $this->getByUrl($data['url']);
        if(!empty($info)){
            return $info;
        }

        /* 添加或新增行為 */
        if(empty($data['id'])){ //新增數據
            $id = $this->add();
            $data['id'] = $id;
            if(!$id){
                $this->error = '新增鏈接出錯！';
                return false;
            }
        } else { //更新數據
            $status = $this->save(); //更新基礎內容
            if(false === $status){
                $this->error = '更新鏈接出錯！';
                return false;
            }
        }

        //內容添加或更新完成
        return $data;
    }

}