<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

/**
 * 後臺公共文件
 * 主要定義後臺公共函數庫
 */

/* 解析列表定義規則*/

function get_list_field($data, $grid,$model){

	// 獲取當前字段數據
    foreach($grid['field'] as $field){
        $array  =   explode('|',$field);
        $temp  =	$data[$array[0]];
        // 函數支持
        if(isset($array[1])){
            $temp = call_user_func($array[1], $temp);
        }
        $data2[$array[0]]    =   $temp;
    }
    if(!empty($grid['format'])){
        $value  =   preg_replace_callback('/\[([a-z_]+)\]/', function($match) use($data2){return $data2[$match[1]];}, $grid['format']);
    }else{
        $value  =   implode(' ',$data2);
    }

	// 鏈接支持
	if(!empty($grid['href'])){
		$links  =   explode(',',$grid['href']);
        foreach($links as $link){
            $array  =   explode('|',$link);
            $href   =   $array[0];
            if(preg_match('/^\[([a-z_]+)\]$/',$href,$matches)){
                $val[]  =   $data2[$matches[1]];
            }else{
                $show   =   isset($array[1])?$array[1]:$value;
                // 替換系統特殊字符串
                $href	=	str_replace(
                    array('[DELETE]','[EDIT]','[MODEL]'),
                    array('del?ids=[id]&model=[MODEL]','edit?id=[id]&model=[MODEL]',$model['id']),
                    $href);

                // 替換數據變量
                $href	=	preg_replace_callback('/\[([a-z_]+)\]/', function($match) use($data){return $data[$match[1]];}, $href);

                $val[]	=	'<a href="'.U($href).'">'.$show.'</a>';
            }
        }
        $value  =   implode(' ',$val);
	}
    return $value;
}

// 獲取模型名稱
function get_model_by_id($id){
    return $model = M('Model')->getFieldById($id,'title');
}

// 獲取屬性類型信息
function get_attribute_type($type=''){
    // TODO 可以加入系統配置
    static $_type = array(
        'num'       =>  array('數字','int(10) UNSIGNED NOT NULL'),
        'string'    =>  array('字符串','varchar(255) NOT NULL'),
        'password'  =>array('非明文','varchar(255) NOT NULL'),
        'textarea'  =>  array('文本框','text NOT NULL'),
        'datetime'  =>  array('時間','int(10) UNSIGNED NOT NULL'),
        'bool'      =>  array('布爾','tinyint(2) NOT NULL'),
        'select'    =>  array('枚舉','char(50) NOT NULL'),
    	'radio'		=>	array('單選','char(10) NOT NULL'),
    	'checkbox'	=>	array('多選','varchar(100) NOT NULL'),
        'checkboxs'	=>	array('自定義數據多選','varchar(100) NOT NULL'),
    	'editor'    =>  array('編輯器','text NOT NULL'),
    	'picture'   =>  array('上傳圖片','int(10) UNSIGNED NOT NULL'),
    	'file'    	=>  array('上傳附件','int(10) UNSIGNED NOT NULL'),
        'pictures'   =>  array('上傳多圖','varchar(255) NOT NULL'),
    );
    return $type?$_type[$type][0]:$_type;
}

/**
 * 獲取對應狀態的文字信息
 * @param int $status
 * @return string 狀態文字 ，false 未獲取到
 * @author huajie <banhuajie@163.com>
 */
function get_status_title($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case -1 : return    '已刪除';   break;
        case 0  : return    '禁用';     break;
        case 1  : return    '正常';     break;
        case 2  : return    '待審核';   break;
        default : return    false;      break;
    }
}

// 獲取數據的狀態操作
function show_status_op($status) {
    switch ($status){
        case 0  : return    '啟用';     break;
        case 1  : return    '禁用';     break;
        case 2  : return    '審核';		break;
        default : return    false;      break;
    }
}

/**
 * 獲取文檔的類型文字
 * @param string $type
 * @return string 狀態文字 ，false 未獲取到
 * @author huajie <banhuajie@163.com>
 */
function get_document_type($type = null){
    if(!isset($type)){
        return false;
    }
    switch ($type){
        case 1  : return    '目錄'; break;
        case 2  : return    '主題'; break;
        case 3  : return    '段落'; break;
        default : return    false;  break;
    }
}

/**
 * 獲取配置的類型
 * @param string $type 配置類型
 * @return string
 */
function get_config_type($type=0){
    $list = C('CONFIG_TYPE_LIST');
    return $list[$type];
}

/**
 * 獲取配置的分組
 * @param string $group 配置分組
 * @return string
 */
function get_config_group($group=0){
    $list = C('CONFIG_GROUP_LIST');
    return $group?$list[$group]:'';
}

/**
 * select返回的數組進行整數映射轉換
 *
 * @param array $map  映射關系二維數組  array(
 *                                          '字段名1'=>array(映射關系數組),
 *                                          '字段名2'=>array(映射關系數組),
 *                                           ......
 *                                       )
 * @author 朱亞傑 <zhuyajie@topthink.net>
 * @return array
 *
 *  array(
 *      array('id'=>1,'title'=>'標題','status'=>'1','status_text'=>'正常')
 *      ....
 *  )
 *
 */
function int_to_string(&$data,$map=array('status'=>array(1=>'正常',-1=>'刪除',0=>'禁用',2=>'未審核',3=>'草稿'))) {
    if($data === false || $data === null ){
        return $data;
    }
    $data = (array)$data;
    foreach ($data as $key => $row){
        foreach ($map as $col=>$pair){
            if(isset($row[$col]) && isset($pair[$row[$col]])){
                $data[$key][$col.'_text'] = $pair[$row[$col]];
            }
        }
    }
    return $data;
}

/**
 * 動態擴展左側菜單,base.html裏用到
 * @author 朱亞傑 <zhuyajie@topthink.net>
 */
function extra_menu($extra_menu,&$base_menu){
    foreach ($extra_menu as $key=>$group){
        if( isset($base_menu['child'][$key]) ){
            $base_menu['child'][$key] = array_merge( $base_menu['child'][$key], $group);
        }else{
            $base_menu['child'][$key] = $group;
        }
    }
}

/**
 * 獲取參數的所有父級分類
 * @param int $cid 分類id
 * @return array 參數分類和父類的信息集合
 * @author huajie <banhuajie@163.com>
 */
function get_parent_category($cid){
    if(empty($cid)){
        return false;
    }
    $cates  =   M('Category')->where(array('status'=>1))->field('id,title,pid')->order('sort')->select();
    $child  =   get_category($cid);	//獲取參數分類的信息
    $pid    =   $child['pid'];
    $temp   =   array();
    $res[]  =   $child;
    while(true){
        foreach ($cates as $key=>$cate){
            if($cate['id'] == $pid){
                $pid = $cate['pid'];
                array_unshift($res, $cate);	//將父分類插入到數組第壹個元素前
            }
        }
        if($pid == 0){
            break;
        }
    }
    return $res;
}

/**
 * 檢測驗證碼
 * @param  integer $id 驗證碼ID
 * @return boolean     檢測結果
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
function check_verify($code, $id = 1){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}

/**
 * 獲取當前分類的文檔類型
 * @param int $id
 * @return array 文檔類型數組
 * @author huajie <banhuajie@163.com>
 */
function get_type_bycate($id = null){
    if(empty($id)){
        return false;
    }
    $type_list  =   C('DOCUMENT_MODEL_TYPE');
    $model_type =   M('Category')->getFieldById($id, 'type');
    $model_type =   explode(',', $model_type);
    foreach ($type_list as $key=>$value){
        if(!in_array($key, $model_type)){
            unset($type_list[$key]);
        }
    }
    return $type_list;
}

/**
 * 獲取當前文檔的分類
 * @param int $id
 * @return array 文檔類型數組
 * @author huajie <banhuajie@163.com>
 */
function get_cate($cate_id = null){
    if(empty($cate_id)){
        return false;
    }
    $cate   =   M('Category')->where('id='.$cate_id)->getField('title');
    return $cate;
}

 // 分析枚舉類型配置值 格式 a:名稱1,b:名稱2
function parse_config_attr($string) {
    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if(strpos($string,':')){
        $value  =   array();
        foreach ($array as $val) {
            list($k, $v) = explode(':', $val);
            $value[$k]   = $v;
        }
    }else{
        $value  =   $array;
    }
    return $value;
}

// 獲取子文檔數目
function get_subdocument_count($id=0){
    return  M('Document')->where('pid='.$id)->count();
}



 // 分析枚舉類型字段值 格式 a:名稱1,b:名稱2
 // 暫時和 parse_config_attr功能相同
 // 但請不要互相使用，後期會調整
function parse_field_attr($string) {
    if(0 === strpos($string,':')){
        // 采用函數定義
        return   eval(substr($string,1).';');
    }
    $array = preg_split('/[,;\r\n]+/', trim($string, ",;\r\n"));
    if(strpos($string,':')){
        $value  =   array();
        foreach ($array as $val) {
            list($k, $v) = explode(':', $val);
            $value[$k]   = $v;
        }
    }else{
        $value  =   $array;
    }
    return $value;
}

/**
 * 獲取行為數據
 * @param string $id 行為id
 * @param string $field 需要獲取的字段
 * @author huajie <banhuajie@163.com>
 */
function get_action($id = null, $field = null){
	if(empty($id) && !is_numeric($id)){
		return false;
	}
	$list = S('action_list');
	if(empty($list[$id])){
		$map = array('status'=>array('gt', -1), 'id'=>$id);
		$list[$id] = M('Action')->where($map)->field(true)->find();
	}
	return empty($field) ? $list[$id] : $list[$id][$field];
}

/**
 * 根據條件字段獲取數據
 * @param mixed $value 條件，可用常量或者數組
 * @param string $condition 條件字段
 * @param string $field 需要返回的字段，不傳則返回整個數據
 * @author huajie <banhuajie@163.com>
 */
function get_document_field($value = null, $condition = 'id', $field = null){
	if(empty($value)){
		return false;
	}

	//拼接參數
	$map[$condition] = $value;
	$info = M('Model')->where($map);
	if(empty($field)){
		$info = $info->field(true)->find();
	}else{
		$info = $info->getField($field);
	}
	return $info;
}

/**
 * 獲取行為類型
 * @param intger $type 類型
 * @param bool $all 是否返回全部類型
 * @author huajie <banhuajie@163.com>
 */
function get_action_type($type, $all = false){
	$list = array(
		1=>'系統',
		2=>'用戶',
	);
	if($all){
		return $list;
	}
	return $list[$type];
}
/**獲取課程分類
 * 
 */
function get_coursename($id){
    
    $info = M('Document')->join('__DOCUMENT_COURSE__ ON __DOCUMENT__.id =__DOCUMENT_COURSE__.id')->where(array('aiyue_document_course.id'=>$id))->find();
    return $info['title'];
}

function getchecked($id){
       
    $list = M('Document')->where(array('category_id'=>$id,'status'=>1))->select();
   
    return $list; 
    
    
        
}