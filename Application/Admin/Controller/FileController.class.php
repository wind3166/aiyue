<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------
namespace Admin\Controller;
/**
 * 文件控制器
 * 主要用於下載模型的文件上傳和下載
 */
class FileController extends AdminController {

    /* 文件上傳 */
    public function upload(){
		$return  = array('status' => 1, 'info' => '上傳成功', 'data' => '');
		/* 調用文件上傳組件上傳文件 */
		$File = D('File');
		$file_driver = C('DOWNLOAD_UPLOAD_DRIVER');
		$info = $File->upload(
			$_FILES,
			C('DOWNLOAD_UPLOAD'),
			C('DOWNLOAD_UPLOAD_DRIVER'),
			C("UPLOAD_{$file_driver}_CONFIG")
		);

        /* 記錄附件信息 */
        if($info){
            $return['data'] = think_encrypt(json_encode($info['download']));
            $return['id']  = $info['download']['id'];
            $return['info'] = $info['download']['name'];
        } else {
            $return['status'] = 0;
            $return['info']   = $File->getError();
        }

        /* 返回JSON數據 */
        $this->ajaxReturn($return);
    }

    /* 下載文件 */
    public function download($id = null){
        if(empty($id) || !is_numeric($id)){
            $this->error('參數錯誤！');
        }

        $logic = D('Download', 'Logic');
        if(!$logic->download($id)){
            $this->error($logic->getError());
        }

    }

    /**
     * 上傳圖片
     * @author huajie <banhuajie@163.com>
     */
    public function uploadPicture(){
        //TODO: 用戶登錄檢測

        /* 返回標準數據 */
        $return  = array('status' => 1, 'info' => '上傳成功', 'data' => '');

        /* 調用文件上傳組件上傳文件 */
        $Picture = D('Picture');
        $pic_driver = C('PICTURE_UPLOAD_DRIVER');
        $info = $Picture->upload(
            $_FILES,
            C('PICTURE_UPLOAD'),
            C('PICTURE_UPLOAD_DRIVER'),
            C("UPLOAD_{$pic_driver}_CONFIG")
        ); //TODO:上傳到遠程服務器

        /* 記錄圖片信息 */
        if($info){
            $return['status'] = 1;
            $return = array_merge($info['download'], $return);
        } else {
            $return['status'] = 0;
            $return['info']   = $Picture->getError();
        }

        /* 返回JSON數據 */
        $this->ajaxReturn($return);
    }
    
    /**
     * 刪除圖片
     */
    public function removePicture(){
        
        $data = array(
          "path"=>I('post.path'),
           "id" =>I('post.id')
        );
        D('Picture')->remove($data);
    }
}