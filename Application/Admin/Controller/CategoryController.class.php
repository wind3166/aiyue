<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 後臺分類管理控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class CategoryController extends AdminController {

    /**
     * 分類管理列表
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function index(){
        $tree = D('Category')->getTree(0,'id,name,title,sort,pid,allow_publish,status');
        $this->assign('tree', $tree);
        C('_SYS_GET_CATEGORY_TREE_', true); //標記系統獲取分類樹模板
        $this->meta_title = '分類管理';
        $this->display();
    }

    /**
     * 顯示分類樹，僅支持內部調
     * @param  array $tree 分類樹
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function tree($tree = null){
        C('_SYS_GET_CATEGORY_TREE_') || $this->_empty();
        $this->assign('tree', $tree);
        $this->display('tree');
    }

    /* 編輯分類 */
    public function edit($id = null, $pid = 0){
        $Category = D('Category');

        if(IS_POST){ //提交表單
            if(false !== $Category->update()){
                $this->success('編輯成功！', U('index'));
            } else {
                $error = $Category->getError();
                $this->error(empty($error) ? '未知錯誤！' : $error);
            }
        } else {
            $cate = '';
            if($pid){
                /* 獲取上級分類信息 */
                $cate = $Category->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上級分類不存在或被禁用！');
                }
            }

            /* 獲取分類信息 */
            $info = $id ? $Category->info($id) : '';

            $this->assign('info',       $info);
            $this->assign('category',   $cate);
            $this->meta_title = '編輯分類';
            $this->display();
        }
    }

    /* 新增分類 */
    public function add($pid = 0){
        $Category = D('Category');

        if(IS_POST){ //提交表單
            if(false !== $Category->update()){
                $this->success('新增成功！', U('index'));
            } else {
                $error = $Category->getError();
                $this->error(empty($error) ? '未知錯誤！' : $error);
            }
        } else {
            $cate = array();
            if($pid){
                /* 獲取上級分類信息 */
                $cate = $Category->info($pid, 'id,name,title,status');
                if(!($cate && 1 == $cate['status'])){
                    $this->error('指定的上級分類不存在或被禁用！');
                }
            }

            /* 獲取分類信息 */
            $this->assign('category', $cate);
            $this->meta_title = '新增分類';
            $this->display('edit');
        }
    }

    /**
     * 刪除壹個分類
     * @author huajie <banhuajie@163.com>
     */
    public function remove(){
        $cate_id = I('id');
        if(empty($cate_id)){
            $this->error('參數錯誤!');
        }

        //判斷該分類下有沒有子分類，有則不允許刪除
        $child = M('Category')->where(array('pid'=>$cate_id))->field('id')->select();
        if(!empty($child)){
            $this->error('請先刪除該分類下的子分類');
        }

        //判斷該分類下有沒有內容
        $document_list = M('Document')->where(array('category_id'=>$cate_id))->field('id')->select();
        if(!empty($document_list)){
            $this->error('請先刪除該分類下的文章（包含回收站）');
        }

        //刪除該分類信息
        $res = M('Category')->delete($cate_id);
        if($res !== false){
            //記錄行為
            action_log('update_category', 'category', $cate_id, UID);
            $this->success('刪除分類成功！');
        }else{
            $this->error('刪除分類失敗！');
        }
    }

    /**
     * 操作分類初始化
     * @param string $type
     * @author huajie <banhuajie@163.com>
     */
    public function operate($type = 'move'){
        //檢查操作參數
        if(strcmp($type, 'move') == 0){
            $operate = '移動';
        }elseif(strcmp($type, 'merge') == 0){
            $operate = '合並';
        }else{
            $this->error('參數錯誤！');
        }
        $from = intval(I('get.from'));
        empty($from) && $this->error('參數錯誤！');

        //獲取分類
        $map = array('status'=>1, 'id'=>array('neq', $from));
        $list = M('Category')->where($map)->field('id,title')->select();

        $this->assign('type', $type);
        $this->assign('operate', $operate);
        $this->assign('from', $from);
        $this->assign('list', $list);
        $this->meta_title = $operate.'分類';
        $this->display();
    }

    /**
     * 移動分類
     * @author huajie <banhuajie@163.com>
     */
    public function move(){
        $to = I('post.to');
        $from = I('post.from');
        $res = M('Category')->where(array('id'=>$from))->setField('pid', $to);
        if($res !== false){
            $this->success('分類移動成功！', U('index'));
        }else{
            $this->error('分類移動失敗！');
        }
    }

    /**
     * 合並分類
     * @author huajie <banhuajie@163.com>
     */
    public function merge(){
        $to = I('post.to');
        $from = I('post.from');
        $Model = M('Category');

        //檢查分類綁定的模型
        $from_models = explode(',', $Model->getFieldById($from, 'model'));
        $to_models = explode(',', $Model->getFieldById($to, 'model'));
        foreach ($from_models as $value){
            if(!in_array($value, $to_models)){
                $this->error('請給目標分類綁定' . get_document_model($value, 'title') . '模型');
            }
        }

        //檢查分類選擇的文檔類型
        $from_types = explode(',', $Model->getFieldById($from, 'type'));
        $to_types = explode(',', $Model->getFieldById($to, 'type'));
        foreach ($from_types as $value){
            if(!in_array($value, $to_types)){
                $types = C('DOCUMENT_MODEL_TYPE');
                $this->error('請給目標分類綁定文檔類型：' . $types[$value]);
            }
        }

        //合並文檔
        $res = M('Document')->where(array('category_id'=>$from))->setField('category_id', $to);

        if($res){
            //刪除被合並的分類
            $Model->delete($from);
            $this->success('合並分類成功！', U('index'));
        }else{
            $this->error('合並分類失敗！');
        }

    }
}