<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Think\Db;
use OT\Database;

/**
 * 數據庫備份還原控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class DatabaseController extends AdminController{

    /**
     * 數據庫備份/還原列表
     * @param  String $type import-還原，export-備份
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function index($type = null){
        switch ($type) {
            /* 數據還原 */
            case 'import':
                //列出備份文件列表
                $path = realpath(C('DATA_BACKUP_PATH'));
                $flag = \FilesystemIterator::KEY_AS_FILENAME;
                $glob = new \FilesystemIterator($path,  $flag);

                $list = array();
                foreach ($glob as $name => $file) {
                    if(preg_match('/^\d{8,8}-\d{6,6}-\d+\.sql(?:\.gz)?$/', $name)){
                        $name = sscanf($name, '%4s%2s%2s-%2s%2s%2s-%d');

                        $date = "{$name[0]}-{$name[1]}-{$name[2]}";
                        $time = "{$name[3]}:{$name[4]}:{$name[5]}";
                        $part = $name[6];

                        if(isset($list["{$date} {$time}"])){
                            $info = $list["{$date} {$time}"];
                            $info['part'] = max($info['part'], $part);
                            $info['size'] = $info['size'] + $file->getSize();
                        } else {
                            $info['part'] = $part;
                            $info['size'] = $file->getSize();
                        }
                        $extension        = strtoupper(pathinfo($file->getFilename(), PATHINFO_EXTENSION));
                        $info['compress'] = ($extension === 'SQL') ? '-' : $extension;
                        $info['time']     = strtotime("{$date} {$time}");

                        $list["{$date} {$time}"] = $info;
                    }
                }
                $title = '數據還原';
                break;

            /* 數據備份 */
            case 'export':
                $Db    = Db::getInstance();
                $list  = $Db->query('SHOW TABLE STATUS');
                $list  = array_map('array_change_key_case', $list);
                $title = '數據備份';
                break;

            default:
                $this->error('參數錯誤！');
        }

        //渲染模板
        $this->assign('meta_title', $title);
        $this->assign('list', $list);
        $this->display($type);
    }

    /**
     * 優化表
     * @param  String $tables 表名
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function optimize($tables = null){
        if($tables) {
            $Db   = Db::getInstance();
            if(is_array($tables)){
                $tables = implode('`,`', $tables);
                $list = $Db->query("OPTIMIZE TABLE `{$tables}`");

                if($list){
                    $this->success("數據表優化完成！");
                } else {
                    $this->error("數據表優化出錯請重試！");
                }
            } else {
                $list = $Db->query("OPTIMIZE TABLE `{$tables}`");
                if($list){
                    $this->success("數據表'{$tables}'優化完成！");
                } else {
                    $this->error("數據表'{$tables}'優化出錯請重試！");
                }
            }
        } else {
            $this->error("請指定要優化的表！");
        }
    }

    /**
     * 修復表
     * @param  String $tables 表名
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function repair($tables = null){
        if($tables) {
            $Db   = Db::getInstance();
            if(is_array($tables)){
                $tables = implode('`,`', $tables);
                $list = $Db->query("REPAIR TABLE `{$tables}`");

                if($list){
                    $this->success("數據表修復完成！");
                } else {
                    $this->error("數據表修復出錯請重試！");
                }
            } else {
                $list = $Db->query("REPAIR TABLE `{$tables}`");
                if($list){
                    $this->success("數據表'{$tables}'修復完成！");
                } else {
                    $this->error("數據表'{$tables}'修復出錯請重試！");
                }
            }
        } else {
            $this->error("請指定要修復的表！");
        }
    }

    /**
     * 刪除備份文件
     * @param  Integer $time 備份時間
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function del($time = 0){
        if($time){
            $name  = date('Ymd-His', $time) . '-*.sql*';
            $path  = realpath(C('DATA_BACKUP_PATH')) . DIRECTORY_SEPARATOR . $name;
            array_map("unlink", glob($path));
            if(count(glob($path))){
                $this->success('備份文件刪除失敗，請檢查權限！');
            } else {
                $this->success('備份文件刪除成功！');
            }
        } else {
            $this->error('參數錯誤！');
        }
    }

    /**
     * 備份數據庫
     * @param  String  $tables 表名
     * @param  Integer $id     表ID
     * @param  Integer $start  起始行數
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function export($tables = null, $id = null, $start = null){
        if(IS_POST && !empty($tables) && is_array($tables)){ //初始化
            //讀取備份配置
            $config = array(
                'path'     => realpath(C('DATA_BACKUP_PATH')) . DIRECTORY_SEPARATOR,
                'part'     => C('DATA_BACKUP_PART_SIZE'),
                'compress' => C('DATA_BACKUP_COMPRESS'),
                'level'    => C('DATA_BACKUP_COMPRESS_LEVEL'),
            );

            //檢查是否有正在執行的任務
            $lock = "{$config['path']}backup.lock";
            if(is_file($lock)){
                $this->error('檢測到有壹個備份任務正在執行，請稍後再試！');
            } else {
                //創建鎖文件
                file_put_contents($lock, NOW_TIME);
            }

            //檢查備份目錄是否可寫
            is_writeable($config['path']) || $this->error('備份目錄不存在或不可寫，請檢查後重試！');
            session('backup_config', $config);

            //生成備份文件信息
            $file = array(
                'name' => date('Ymd-His', NOW_TIME),
                'part' => 1,
            );
            session('backup_file', $file);

            //緩存要備份的表
            session('backup_tables', $tables);

            //創建備份文件
            $Database = new Database($file, $config);
            if(false !== $Database->create()){
                $tab = array('id' => 0, 'start' => 0);
                $this->success('初始化成功！', '', array('tables' => $tables, 'tab' => $tab));
            } else {
                $this->error('初始化失敗，備份文件創建失敗！');
            }
        } elseif (IS_GET && is_numeric($id) && is_numeric($start)) { //備份數據
            $tables = session('backup_tables');
            //備份指定表
            $Database = new Database(session('backup_file'), session('backup_config'));
            $start  = $Database->backup($tables[$id], $start);
            if(false === $start){ //出錯
                $this->error('備份出錯！');
            } elseif (0 === $start) { //下壹表
                if(isset($tables[++$id])){
                    $tab = array('id' => $id, 'start' => 0);
                    $this->success('備份完成！', '', array('tab' => $tab));
                } else { //備份完成，清空緩存
                    unlink(session('backup_config.path') . 'backup.lock');
                    session('backup_tables', null);
                    session('backup_file', null);
                    session('backup_config', null);
                    $this->success('備份完成！');
                }
            } else {
                $tab  = array('id' => $id, 'start' => $start[0]);
                $rate = floor(100 * ($start[0] / $start[1]));
                $this->success("正在備份...({$rate}%)", '', array('tab' => $tab));
            }

        } else { //出錯
            $this->error('參數錯誤！');
        }
    }

    /**
     * 還原數據庫
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function import($time = 0, $part = null, $start = null){
        if(is_numeric($time) && is_null($part) && is_null($start)){ //初始化
            //獲取備份文件信息
            $name  = date('Ymd-His', $time) . '-*.sql*';
            $path  = realpath(C('DATA_BACKUP_PATH')) . DIRECTORY_SEPARATOR . $name;
            $files = glob($path);
            $list  = array();
            foreach($files as $name){
                $basename = basename($name);
                $match    = sscanf($basename, '%4s%2s%2s-%2s%2s%2s-%d');
                $gz       = preg_match('/^\d{8,8}-\d{6,6}-\d+\.sql.gz$/', $basename);
                $list[$match[6]] = array($match[6], $name, $gz);
            }
            ksort($list);

            //檢測文件正確性
            $last = end($list);
            if(count($list) === $last[0]){
                session('backup_list', $list); //緩存備份列表
                $this->success('初始化完成！', '', array('part' => 1, 'start' => 0));
            } else {
                $this->error('備份文件可能已經損壞，請檢查！');
            }
        } elseif(is_numeric($part) && is_numeric($start)) {
            $list  = session('backup_list');
            $db = new Database($list[$part], array(
                'path'     => realpath(C('DATA_BACKUP_PATH')) . DIRECTORY_SEPARATOR,
                'compress' => $list[$part][2]));

            $start = $db->import($start);

            if(false === $start){
                $this->error('還原數據出錯！');
            } elseif(0 === $start) { //下壹卷
                if(isset($list[++$part])){
                    $data = array('part' => $part, 'start' => 0);
                    $this->success("正在還原...#{$part}", '', $data);
                } else {
                    session('backup_list', null);
                    $this->success('還原完成！');
                }
            } else {
                $data = array('part' => $part, 'start' => $start[0]);
                if($start[1]){
                    $rate = floor(100 * ($start[0] / $start[1]));
                    $this->success("正在還原...#{$part} ({$rate}%)", '', $data);
                } else {
                    $data['gz'] = 1;
                    $this->success("正在還原...#{$part}", '', $data);
                }
            }

        } else {
            $this->error('參數錯誤！');
        }
    }

}