<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi;

/**
 * 後臺用戶控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class UserController extends AdminController {

    /**
     * 用戶管理首頁
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function index(){
        $nickname       =   I('nickname');
        $map['status']  =   array('egt',0);
        if(is_numeric($nickname)){
            $map['uid|nickname']=   array(intval($nickname),array('like','%'.$nickname.'%'),'_multi'=>true);
        }else{
            $map['nickname']    =   array('like', '%'.(string)$nickname.'%');
        }

        $list   = $this->lists('Member', $map);
        int_to_string($list);
        $this->assign('_list', $list);
        $this->meta_title = '用戶信息';
        $this->display();
    }

    /**
     * 修改昵稱初始化
     * @author huajie <banhuajie@163.com>
     */
    public function updateNickname(){
        $nickname = M('Member')->getFieldByUid(UID, 'nickname');
        $this->assign('nickname', $nickname);
        $this->meta_title = '修改昵稱';
        $this->display();
    }

    /**
     * 修改昵稱提交
     * @author huajie <banhuajie@163.com>
     */
    public function submitNickname(){
        //獲取參數
        $nickname = I('post.nickname');
        $password = I('post.password');
        empty($nickname) && $this->error('請輸入昵稱');
        empty($password) && $this->error('請輸入密碼');

        //密碼驗證
        $User   =   new UserApi();
        $uid    =   $User->login(UID, $password, 4);
        ($uid == -2) && $this->error('密碼不正確');

        $Member =   D('Member');
        $data   =   $Member->create(array('nickname'=>$nickname));
        if(!$data){
            $this->error($Member->getError());
        }

        $res = $Member->where(array('uid'=>$uid))->save($data);

        if($res){
            $user               =   session('user_auth');
            $user['username']   =   $data['nickname'];
            session('user_auth', $user);
            session('user_auth_sign', data_auth_sign($user));
            $this->success('修改昵稱成功！');
        }else{
            $this->error('修改昵稱失敗！');
        }
    }

    /**
     * 修改密碼初始化
     * @author huajie <banhuajie@163.com>
     */
    public function updatePassword(){
        $this->meta_title = '修改密碼';
        $this->display();
    }

    /**
     * 修改密碼提交
     * @author huajie <banhuajie@163.com>
     */
    public function submitPassword(){
        //獲取參數
        $password   =   I('post.old');
        empty($password) && $this->error('請輸入原密碼');
        $data['password'] = I('post.password');
        empty($data['password']) && $this->error('請輸入新密碼');
        $repassword = I('post.repassword');
        empty($repassword) && $this->error('請輸入確認密碼');

        if($data['password'] !== $repassword){
            $this->error('您輸入的新密碼與確認密碼不壹致');
        }

        $Api    =   new UserApi();
        $res    =   $Api->updateInfo(UID, $password, $data);
        if($res['status']){
            $this->success('修改密碼成功！');
        }else{
            $this->error($res['info']);
        }
    }

    /**
     * 用戶行為列表
     * @author huajie <banhuajie@163.com>
     */
    public function action(){
        //獲取列表數據
        $Action =   M('Action')->where(array('status'=>array('gt',-1)));
        $list   =   $this->lists($Action);
        int_to_string($list);
        // 記錄當前列表頁的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);

        $this->assign('_list', $list);
        $this->meta_title = '用戶行為';
        $this->display();
    }

    /**
     * 新增行為
     * @author huajie <banhuajie@163.com>
     */
    public function addAction(){
        $this->meta_title = '新增行為';
        $this->assign('data',null);
        $this->display('editaction');
    }

    /**
     * 編輯行為
     * @author huajie <banhuajie@163.com>
     */
    public function editAction(){
        $id = I('get.id');
        empty($id) && $this->error('參數不能為空！');
        $data = M('Action')->field(true)->find($id);

        $this->assign('data',$data);
        $this->meta_title = '編輯行為';
        $this->display();
    }

    /**
     * 更新行為
     * @author huajie <banhuajie@163.com>
     */
    public function saveAction(){
        $res = D('Action')->update();
        if(!$res){
            $this->error(D('Action')->getError());
        }else{
            $this->success($res['id']?'更新成功！':'新增成功！', Cookie('__forward__'));
        }
    }

    /**
     * 會員狀態修改
     * @author 朱亞傑 <zhuyajie@topthink.net>
     */
    public function changeStatus($method=null){
        $id = array_unique((array)I('id',0));
        if( in_array(C('USER_ADMINISTRATOR'), $id)){
            $this->error("不允許對超級管理員執行該操作!");
        }
        $id = is_array($id) ? implode(',',$id) : $id;
        if ( empty($id) ) {
            $this->error('請選擇要操作的數據!');
        }
        $map['uid'] =   array('in',$id);
        switch ( strtolower($method) ){
            case 'forbiduser':
                $this->forbid('Member', $map );
                break;
            case 'resumeuser':
                $this->resume('Member', $map );
                break;
            case 'deleteuser':
                $this->delete('Member', $map );
                break;
            default:
                $this->error('參數非法');
        }
    }

    public function add($username = '',$account='', $password = '', $repassword = '', $email = '',$phone='',$address='',$age='',$status=''){
        if(IS_POST){
            /* 檢測密碼 */
            if($password != $repassword){
                $this->error('密碼和重復密碼不壹致！');
            }
            
            /* 調用註冊接口註冊用戶 */
            $User   =   new UserApi;
           
            $uid    =   $User->register($username, $account,$password, $email,$phone,$address,$age,$status);
            if(0 < $uid){ //註冊成功
                $user = array('uid' => $uid, 'nickname' => $username, 'status' => 1);
                if(!M('Member')->add($user)){
                    $this->error('用戶添加失敗！');
                } else {
                    if(isset($_POST['status'])){
                        $this->success('用戶添加成功！',U('Admin/Article/userlist'));
                    }else{
                        $this->success('用戶添加成功！',U('index'));
                    }
                }
            } else { //註冊失敗，顯示錯誤信息
                $this->error($this->showRegError($uid));
            }
        } else {
            $this->meta_title = '新增用戶';
            $this->display();
        }
    }

    /**
     * 獲取用戶註冊錯誤信息
     * @param  integer $code 錯誤編碼
     * @return string        錯誤信息
     */
    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '用戶名長度必須在16個字符以內！'; break;
            case -2:  $error = '用戶名被禁止註冊！'; break;
            case -3:  $error = '用戶名被占用！'; break;
            case -4:  $error = '密碼長度必須在6-30個字符之間！'; break;
            case -5:  $error = '郵箱格式不正確！'; break;
            case -6:  $error = '郵箱長度必須在1-32個字符之間！'; break;
            case -7:  $error = '郵箱被禁止註冊！'; break;
            case -8:  $error = '郵箱被占用！'; break;
            case -9:  $error = '手機格式不正確！'; break;
            case -10: $error = '手機被禁止註冊！'; break;
            case -11: $error = '手機號被占用！'; break;
            default:  $error = '未知錯誤';
        }
        return $error;
    }

    
}