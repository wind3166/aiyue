<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 後臺頻道控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */

class ChannelController extends AdminController {

    /**
     * 頻道列表
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function index(){
        $pid = i('get.pid', 0);
        /* 獲取頻道列表 */
        $map  = array('status' => array('gt', -1), 'pid'=>$pid);
        $list = M('Channel')->where($map)->order('sort asc,id asc')->select();

        $this->assign('list', $list);
        $this->assign('pid', $pid);
        $this->meta_title = '導航管理';
        $this->display();
    }

    /**
     * 添加頻道
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function add(){
        if(IS_POST){
            $Channel = D('Channel');
            $data = $Channel->create();
            if($data){
                $id = $Channel->add();
                if($id){
                    $this->success('新增成功', U('index'));
                    //記錄行為
                    action_log('update_channel', 'channel', $id, UID);
                } else {
                    $this->error('新增失敗');
                }
            } else {
                $this->error($Channel->getError());
            }
        } else {
            $pid = i('get.pid', 0);
            //獲取父導航
            if(!empty($pid)){
                $parent = M('Channel')->where(array('id'=>$pid))->field('title')->find();
                $this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info',null);
            $this->meta_title = '新增導航';
            $this->display('edit');
        }
    }

    /**
     * 編輯頻道
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function edit($id = 0){
        if(IS_POST){
            $Channel = D('Channel');
            $data = $Channel->create();
            if($data){
                if($Channel->save()){
                    //記錄行為
                    action_log('update_channel', 'channel', $data['id'], UID);
                    $this->success('編輯成功', U('index'));
                } else {
                    $this->error('編輯失敗');
                }

            } else {
                $this->error($Channel->getError());
            }
        } else {
            $info = array();
            /* 獲取數據 */
            $info = M('Channel')->find($id);

            if(false === $info){
                $this->error('獲取配置信息錯誤');
            }

            $pid = i('get.pid', 0);
            //獲取父導航
            if(!empty($pid)){
            	$parent = M('Channel')->where(array('id'=>$pid))->field('title')->find();
            	$this->assign('parent', $parent);
            }

            $this->assign('pid', $pid);
            $this->assign('info', $info);
            $this->meta_title = '編輯導航';
            $this->display();
        }
    }

    /**
     * 刪除頻道
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function del(){
        $id = array_unique((array)I('id',0));

        if ( empty($id) ) {
            $this->error('請選擇要操作的數據!');
        }

        $map = array('id' => array('in', $id) );
        if(M('Channel')->where($map)->delete()){
            //記錄行為
            action_log('update_channel', 'channel', $id, UID);
            $this->success('刪除成功');
        } else {
            $this->error('刪除失敗！');
        }
    }

    /**
     * 導航排序
     * @author huajie <banhuajie@163.com>
     */
    public function sort(){
        if(IS_GET){
            $ids = I('get.ids');
            $pid = I('get.pid');

            //獲取排序的數據
            $map = array('status'=>array('gt',-1));
            if(!empty($ids)){
                $map['id'] = array('in',$ids);
            }else{
                if($pid !== ''){
                    $map['pid'] = $pid;
                }
            }
            $list = M('Channel')->where($map)->field('id,title')->order('sort asc,id asc')->select();

            $this->assign('list', $list);
            $this->meta_title = '導航排序';
            $this->display();
        }elseif (IS_POST){
            $ids = I('post.ids');
            $ids = explode(',', $ids);
            foreach ($ids as $key=>$value){
                $res = M('Channel')->where(array('id'=>$value))->setField('sort', $key+1);
            }
            if($res !== false){
                $this->success('排序成功！');
            }else{
                $this->eorror('排序失敗！');
            }
        }else{
            $this->error('非法請求！');
        }
    }
}