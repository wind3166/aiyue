<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\AuthGroupModel;

/**
 * 模型管理控制器
 * @author huajie <banhuajie@163.com>
 */
class ModelController extends AdminController {


    /**
     * 檢測是否是需要動態判斷的權限
     * @return boolean|null
     *      返回true則表示當前訪問有權限
     *      返回false則表示當前訪問無權限
     *      返回null，則會進入checkRule根據節點授權判斷權限
     *
     * @author 朱亞傑  <xcoolcc@gmail.com>
     */
    protected function checkDynamic(){
        if(IS_ROOT){
            return true;//管理員允許訪問任何頁面
        }
        //模型權限業務檢查邏輯
        //
        //提供的工具方法：
        //$AUTH_GROUP = D('AuthGroup');
        // $AUTH_GROUP->checkModelId($mid);      //檢查模型id列表是否全部存在
        // AuthGroupModel::getModelOfGroup($gid);//獲取某個用護組擁有權限的模型id
        $model_ids = AuthGroupModel::getAuthModels(UID);
        $id        = I('id');
        switch(strtolower(ACTION_NAME)){
            case 'edit':    //編輯
            case 'update':  //更新
                if ( in_array($id,$model_ids) ) {
                    return true;
                }else{
                    return false;
                }
            case 'setstatus': //更改狀態
                if ( is_array($id) && array_intersect($id,(array)$model_ids)==$id ) {
                    return true;
                }elseif( in_array($id,$model_ids) ){
                    return true;
                }else{
                    return false;
                }
        }

        return null;//不明,需checkRule
    }

    /**
     * 模型管理首頁
     * @author huajie <banhuajie@163.com>
     */
    public function index(){
        $map = array('status'=>array('gt',-1));
        $list = $this->lists('Model',$map);
        int_to_string($list);
        // 記錄當前列表頁的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);

        $this->assign('_list', $list);
        $this->meta_title = '模型管理';
        $this->display();
    }

    /**
     * 新增頁面初始化
     * @author huajie <banhuajie@163.com>
     */
    public function add(){
        //獲取所有的模型
        $models = M('Model')->where(array('extend'=>0))->field('id,title')->select();

        $this->assign('models', $models);
        $this->meta_title = '新增模型';
        $this->display();
    }

    /**
     * 編輯頁面初始化
     * @author huajie <banhuajie@163.com>
     */
    public function edit(){
        $id = I('get.id','');
        if(empty($id)){
            $this->error('慘數不能為空！');
        }

        /*獲取壹條記錄的詳細數據*/
        $Model = M('Model');
        $data = $Model->field(true)->find($id);                
        if(!$data){
            $this->error($Model->getError());
        }

        $fields = M('Attribute')->where(array('model_id'=>$data['id']))->field('id,name,title,is_show')->select();

        //是否繼承了其他模型
        if($data['extend'] != 0){
            $extend_fields = M('Attribute')->where(array('model_id'=>$data['extend']))->field('id,name,title,is_show')->select();
            $fields = array_merge($fields, $extend_fields);
        }

        /* 獲取模型排序字段 */
        $field_sort = json_decode($data['field_sort'], true);
        if(!empty($field_sort)){
        	/* 對字段數組重新整理 */
        	$fields_f = array();
        	foreach($fields as $v){
        		$fields_f[$v['id']] = $v;
        	}
        	$fields = array();
        	foreach($field_sort as $key => $groups){
        		foreach($groups as $group){
        			$fields[$fields_f[$group]['id']] = array(
        					'id' => $fields_f[$group]['id'],
        					'name' => $fields_f[$group]['name'],
        					'title' => $fields_f[$group]['title'],
        					'is_show' => $fields_f[$group]['is_show'],
        					'group' => $key
        			);
        		}
        	}                                    
        	/* 對新增字段進行處理 */
        	$new_fields = array_diff_key($fields_f,$fields);
        	foreach ($new_fields as $value){
        		if($value['is_show'] == 1){
        			array_unshift($fields, $value);
        		}
        	}
        }
//        print_r($data);
//print_r($fields);die;
        $this->assign('fields', $fields);
        $this->assign('info', $data);
        $this->meta_title = '編輯模型';
        $this->display();
    }

    /**
     * 刪除壹條數據
     * @author huajie <banhuajie@163.com>
     */
    public function del(){
        $ids = I('get.ids');
        empty($ids) && $this->error('慘數不能為空！');
        $ids = explode(',', $ids);
        foreach ($ids as $value){
            $res = D('Model')->del($value);
            if(!$res){
                break;
            }
        }
        if(!$res){
            $this->error(D('Model')->getError());
        }else{
            $this->success('刪除模型成功！');
        }
    }

    /**
     * 更新壹條數據
     * @author huajie <banhuajie@163.com>
     */
    public function update(){
        $res = D('Model')->update();

        if(!$res){
            $this->error(D('Model')->getError());
        }else{
            $this->success($res['id']?'更新成功':'新增成功', Cookie('__forward__'));
        }
    }

    /**
     * 生成壹個模型
     * @author huajie <banhuajie@163.com>
     */
    public function generate(){
        if(!IS_POST){
            //獲取所有的數據表
            $tables = D('Model')->getTables();

            $this->assign('tables', $tables);
            $this->meta_title = '生成模型';
            $this->display();
        }else{
            $table = I('post.table');
            empty($table) && $this->error('請選擇要生成的數據表！');
            $res = D('Model')->generate($table);
            if($res){
                $this->success('生成模型成功！', U('index'));
            }else{
                $this->error(D('Model')->getError());
            }
        }
    }
}