<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 屬性控制器
 * @author huajie <banhuajie@163.com>
 */
class AttributeController extends AdminController {

    /**
     * 屬性列表
     * @author huajie <banhuajie@163.com>
     */
    public function index(){
        $model_id = I('get.model_id');
        /* 查詢條件初始化 */
        $map['model_id']    =   $model_id;

        $list = $this->lists('Attribute', $map);
        int_to_string($list);

        // 記錄當前列表頁的cookie
        Cookie('__forward__',       $_SERVER['REQUEST_URI']);
        $this->assign('_list',      $list);
        $this->assign('model_id',   $model_id);
        $this->meta_title = '屬性列表';
        $this->display();
    }

    /**
     * 新增頁面初始化
     * @author huajie <banhuajie@163.com>
     */
    public function add(){
        $model_id   =   I('get.model_id');
        $model      =   M('Model')->field('title,name,field_group')->find($model_id);
        $this->assign('model',$model);
        $this->assign('info', array('model_id'=>$model_id));
        $this->meta_title = '新增屬性';
        $this->display('edit');
    }

    /**
     * 編輯頁面初始化
     * @author huajie <banhuajie@163.com>
     */
    public function edit(){
        $id = I('get.id','');
        if(empty($id)){
            $this->error('參數不能為空！');
        }

        /*獲取壹條記錄的詳細數據*/
        $Model = M('Attribute');
        $data = $Model->field(true)->find($id);
        if(!$data){
            $this->error($Model->getError());
        }
        $model  =   M('Model')->field('title,name,field_group')->find($data['model_id']);
        $this->assign('model',$model);
        $this->assign('info', $data);
        $this->meta_title = '編輯屬性';
        $this->display();
    }

    /**
     * 更新壹條數據
     * @author huajie <banhuajie@163.com>
     */
    public function update(){
        $res = D('Attribute')->update();
        if(!$res){
            $this->error(D('Attribute')->getError());
        }else{
            $this->success($res['id']?'更新成功':'新增成功', Cookie('__forward__'));
        }
    }

    /**
     * 刪除壹條數據
     * @author huajie <banhuajie@163.com>
     */
    public function remove(){
        $id = I('id');
        empty($id) && $this->error('參數錯誤！');

        $Model = D('Attribute');

        $info = $Model->getById($id);
        empty($info) && $this->error('該字段不存在！');

        //刪除屬性數據
        $res = $Model->delete($id);

        //刪除表字段
        $Model->deleteField($info);
        if(!$res){
            $this->error(D('Attribute')->getError());
        }else{
            //記錄行為
            action_log('update_attribute', 'attribute', $id, UID);
            $this->success('刪除成功', U('index','model_id='.$info['model_id']));
        }
    }
}