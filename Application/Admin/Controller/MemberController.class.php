<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Admin\Controller;
use Think\Controller;
Class MemberController extends ArticleController{
    
    public function Index(){
        
        $this->getMenu();    
        if(!empty($_GET['CourseSelect'])){
             $list = M('Member_info')->join("__APPOINTMENT__ ON __APPOINTMENT__.memberID = __MEMBER___info.id")->where(array('status'=>1,'course'=>$_GET['CourseSelect']))->group("memberID")->order('create_time desc')->select();
             for($i=0;$i<count($list);$i++)
             {
            
               $list[$i]['id'] = $list[$i]['memberID'];  
             
               
             }
             
             
        }else {
             $list = M('Member_info')->where(array('status'=>1))->order('create_time desc')->select();
            
        }
        
     
    
        $this->assign('list',$list);
        
       $list1 = D('Document')->lists(62);
       $list2 = D('Document')->lists(63);
       $appointmentList =  array_merge($list1,$list2);
            
        $this->assign("AppList",$appointmentList);
        $field['name'] = "file_id";
        $this->assign('field',$field);
        $this->meta_title = C('WEB_SITE_TITLE');
        $this->display('Think/memberlist');
        
        
        
    }
    public function adds(){
         $this->getMenu();  
        $this->meta_title = C('WEB_SITE_TITLE');
        $this->display("Think/add");
         
    }
    public function updates($data = null){
        
     $rules = array(
     array('uids','','卡号已存在！',0,'unique',1), // 在新增的时候验证name字段是否唯一
     );
        $Member = M('Member_info');
     
        $data = $Member->create($data); 
       
        if(empty($data)){
            return false;
        }
        if($Member->validate($rules)->create($data))
        {
         $data['create_time'] = time();
        $data['password'] = md5($data['password']);
      
        if(empty($data['id']))
        {      
         $Member->add($data);  
         $this->success('添加成功',U('Member/index'));
        }
        else
        {
          $Member->save($data);     
          $this->success('更新成功',U('Member/index'));
      
        }   
           
        }
        else 
        {
           
            $this->error('卡號已存在');
        }
        
      
      
        
    }
    public function edits(){
        $this->getMenu();
        $id = I('get.id');
        $list = M('Member_info')->where(array('status'=>1,'id'=>$id))->find(); 
 
        $appointmentList = M('appointment')->where(array("memberID"=>$id))->order('id desc')->select();
        
  
        
        
        $this->assign('appointmentInfo',$appointmentList);
        $this->assign('info',$list);
        $this->display('Think/edit');
    }
    public function del(){
         $id = I('get.id');
         $res = M('Member_info')->where(array('status'=>1,'id'=>$id))->delete();
         if($res)
         {
             $this->success('刪除成功!');
         }
         else
         {
             $this->error('無效操作!');
         }    
        
    }

}