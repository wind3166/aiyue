<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use User\Api\UserApi as UserApi;

/**
 * 後臺首頁控制器
 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
 */
class IndexController extends AdminController {

    /**
     * 後臺首頁
     * @author 麥當苗兒 <zuojiazi@vip.qq.com>
     */
    public function index(){
        if(UID){
            $this->meta_title = '管理首頁';
            $this->display();
        } else {
            $this->redirect('Public/login');
        }
    }

}