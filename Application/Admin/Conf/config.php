<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.thinkphp.cn>
// +----------------------------------------------------------------------

/**
 * 前臺配置文件
 * 所有除開系統級別的前臺配置
 */
return array(
    /* 數據緩存設置 */
    'DATA_CACHE_PREFIX'    => 'onethink_', // 緩存前綴
    'DATA_CACHE_TYPE'      => 'File', // 數據緩存類型

    /* 文件上傳相關配置 */
    'DOWNLOAD_UPLOAD' => array(
        'mimes'    => '', //允許上傳的文件MiMe類型
        'maxSize'  => 5*1024*1024, //上傳的文件大小限制 (0-不做限制)
        'exts'     => 'zip,rar,tar,gz,7z,doc,docx,txt,xlsx,xls,xlsx,ppt', //允許上傳的文件後綴
        'autoSub'  => true, //自動子目錄保存文件
        'subName'  => array('date', 'Y-m-d'), //子目錄創建方式，[0]-函數名，[1]-參數，多個參數使用數組
        'rootPath' => './Uploads/Download/', //保存根路徑
        'savePath' => '', //保存路徑
        'saveName' => array('uniqid', ''), //上傳文件命名規則，[0]-函數名，[1]-參數，多個參數使用數組
        'saveExt'  => '', //文件保存後綴，空則使用原後綴
        'replace'  => false, //存在同名是否覆蓋
        'hash'     => true, //是否生成hash編碼
        'callback' => false, //檢測文件是否存在回調函數，如果存在返回文件信息數組
    ), //下載模型上傳配置（文件上傳類配置）

    /* 圖片上傳相關配置 */
    'PICTURE_UPLOAD' => array(
		'mimes'    => '', //允許上傳的文件MiMe類型
		'maxSize'  => 2*1024*1024, //上傳的文件大小限制 (0-不做限制)
		'exts'     => 'jpg,gif,png,jpeg', //允許上傳的文件後綴
		'autoSub'  => true, //自動子目錄保存文件
		'subName'  => array('date', 'Y-m-d'), //子目錄創建方式，[0]-函數名，[1]-參數，多個參數使用數組
		'rootPath' => './Uploads/Picture/', //保存根路徑
		'savePath' => '', //保存路徑
		'saveName' => array('uniqid', ''), //上傳文件命名規則，[0]-函數名，[1]-參數，多個參數使用數組
		'saveExt'  => '', //文件保存後綴，空則使用原後綴
		'replace'  => false, //存在同名是否覆蓋
		'hash'     => true, //是否生成hash編碼
		'callback' => false, //檢測文件是否存在回調函數，如果存在返回文件信息數組
    ), //圖片上傳相關配置（文件上傳類配置）

    'PICTURE_UPLOAD_DRIVER'=>'local',
    //本地上傳文件驅動配置
    'UPLOAD_LOCAL_CONFIG'=>array(),
    'UPLOAD_BCS_CONFIG'=>array(
        'AccessKey'=>'',
        'SecretKey'=>'',
        'bucket'=>'',
        'rename'=>false
    ),
    'UPLOAD_QINIU_CONFIG'=>array(
        'accessKey'=>'__ODsglZwwjRJNZHAu7vtcEf-zgIxdQAY-QqVrZD',
        'secrectKey'=>'Z9-RahGtXhKeTUYy9WCnLbQ98ZuZ_paiaoBjByKv',
        'bucket'=>'onethinktest',
        'domain'=>'onethinktest.u.qiniudn.com',
        'timeout'=>3600,
    ),


    /* 編輯器圖片上傳相關配置 */
    'EDITOR_UPLOAD' => array(
		'mimes'    => '', //允許上傳的文件MiMe類型
		'maxSize'  => 2*1024*1024, //上傳的文件大小限制 (0-不做限制)
		'exts'     => 'jpg,gif,png,jpeg', //允許上傳的文件後綴
		'autoSub'  => true, //自動子目錄保存文件
		'subName'  => array('date', 'Y-m-d'), //子目錄創建方式，[0]-函數名，[1]-參數，多個參數使用數組
		'rootPath' => './Uploads/Editor/', //保存根路徑
		'savePath' => '', //保存路徑
		'saveName' => array('uniqid', ''), //上傳文件命名規則，[0]-函數名，[1]-參數，多個參數使用數組
		'saveExt'  => '', //文件保存後綴，空則使用原後綴
		'replace'  => false, //存在同名是否覆蓋
		'hash'     => true, //是否生成hash編碼
		'callback' => false, //檢測文件是否存在回調函數，如果存在返回文件信息數組
    ),

    /* 模板相關配置 */
    'TMPL_PARSE_STRING' => array(
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
        '__IMG__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
        '__CSS__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
        '__JS__'     => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
    ),

    /* SESSION 和 COOKIE 配置 */
    'SESSION_PREFIX' => 'onethink_admin', //session前綴
    'COOKIE_PREFIX'  => 'onethink_admin_', // Cookie前綴 避免沖突
    'VAR_SESSION_ID' => 'session_id',	//修復uploadify插件無法傳遞session_id的bug

    /* 後臺錯誤頁面模板 */
    'TMPL_ACTION_ERROR'     =>  MODULE_PATH.'View/Public/error.html', // 默認錯誤跳轉對應的模板文件
    'TMPL_ACTION_SUCCESS'   =>  MODULE_PATH.'View/Public/success.html', // 默認成功跳轉對應的模板文件
    'TMPL_EXCEPTION_FILE'   =>  MODULE_PATH.'View/Public/exception.html',// 異常頁面的模板文件

);