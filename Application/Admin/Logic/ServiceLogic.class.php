<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Admin\Logic;
/**
 * Description of VipLogic
 *
 * @author Tina
 */
class ServiceLogic extends BaseLogic{
	/* 自動驗證規則 */
//	protected $_validate = array(
//		/* 驗證用戶名 */
//		array('uname', '1,30', -1, self::EXISTS_VALIDATE, 'length'), //用戶名長度不合法
//		array('uname', 'checkDenyMember', -2, self::EXISTS_VALIDATE, 'callback'), //用戶名禁止註冊
//		array('uname', '', -3, self::EXISTS_VALIDATE, 'unique'), //用戶名被占用
//
//		/* 驗證密碼 */
//		array('password', '6,30', -4, self::EXISTS_VALIDATE, 'length'), //密碼長度不合法
//
//		/* 驗證郵箱 */
//		array('email', 'email', -5, self::EXISTS_VALIDATE), //郵箱格式不正確
//		array('email', '1,32', -6, self::EXISTS_VALIDATE, 'length'), //郵箱長度不合法
//		array('email', 'checkDenyEmail', -7, self::EXISTS_VALIDATE, 'callback'), //郵箱禁止註冊
//		array('email', '', -8, self::EXISTS_VALIDATE, 'unique'), //郵箱被占用
//	);

	/* 自動完成規則 */
	protected $_auto = array(
            /* 驗證用戶名 */
		array('uname', '1,30', -1, self::EXISTS_VALIDATE, 'length'), //用戶名長度不合法
		array('uname', 'checkDenyMember', -2, self::EXISTS_VALIDATE, 'callback'), //用戶名禁止註冊
		array('uname', '', -3, self::EXISTS_VALIDATE, 'unique'), //用戶名被占用

		/* 驗證密碼 */
		array('password', '6,30', -4, self::EXISTS_VALIDATE, 'length'), //密碼長度不合法

		/* 驗證郵箱 */
		array('email', 'email', -5, self::EXISTS_VALIDATE), //郵箱格式不正確
		array('email', '1,32', -6, self::EXISTS_VALIDATE, 'length'), //郵箱長度不合法
		array('email', 'checkDenyEmail', -7, self::EXISTS_VALIDATE, 'callback'), //郵箱禁止註冊
		array('email', '', -8, self::EXISTS_VALIDATE, 'unique'), //郵箱被占用
        );
	/**
	 * 新增或添加壹條文章詳情
	 * @param  number $id 文章ID
	 * @return boolean    true-操作成功，false-操作失敗
	 * @author 麥當苗兒 <zuojiazi@vip.qq.com>
	 */
	public function update($id = 0){
		/* 獲取文章數據 */
		$data = $this->create();
		if($data === false){
			return false;
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}

	/**
	 * 獲取文章的詳細內容
	 * @return boolean
	 * @author huajie <banhuajie@163.com>
	 */
	protected function getContent(){
		$type = I('post.type');
		$content = I('post.content');
		if($type > 1){	//主題和段落必須有內容
			if(empty($content)){
				return false;
			}
		}else{			//目錄沒內容則生成空字符串
			if(empty($content)){
				$_POST['content'] = ' ';
			}
		}
		return true;
	}

	/**
	 * 保存為草稿
	 * @return true 成功， false 保存出錯
	 * @author huajie <banhuajie@163.com>
	 */
	public function autoSave($id = 0){
		$this->_validate = array();

		/* 獲取文章數據 */
		$data = $this->create();
		if(!$data){
			return false;
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}
}