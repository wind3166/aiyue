<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Logic;

/**
 * 文檔模型子模型 - 下載模型
 */
class DownloadLogic extends BaseLogic{

	/* 自動驗證規則 */
	protected $_validate = array(
		array('content', 'require', '詳細內容不能為空！', self::MUST_VALIDATE , 'regex', self::MODEL_BOTH),
		array('download', 'number', '下載次數請輸入整數！', self::VALUE_VALIDATE , 'regex', self::MODEL_BOTH),
		array('file_id', 'require', '請上傳附件！', self::MUST_VALIDATE , 'regex', self::MODEL_BOTH),
	);

	/* 自動完成規則 */
	protected $_auto = array();

	/**
	 * 獲取模型詳細信息
	 * @param  integer $id 文檔ID
	 * @return array       當前模型詳細信息
	 * @author huajie <banhuajie@163.com>
	 */
	public function detail($id){
		$data = $this->field(true)->find($id);
		if(!$data){
			$this->error = '獲取詳細信息出錯！';
			return false;
		}
		$file = D('File')->field(true)->find($data['file_id']);
		return $data;
	}

	/**
	 * 更新數據
	 * @param intger $id
	 * @author huajie <banhuajie@163.com>
	 */
	public function update($id = 0){
		/* 獲取下載數據 */ //TODO: 根據不同用戶獲取允許更改或添加的字段
		$data = $this->create();
		if(!$data){
			return false;
		}

		$file = json_decode(think_decrypt(I('post.file_id')), true);
		if(!empty($file)){
			$data['file_id'] = $file['id'];
			$data['size']    = $file['size'];
		} else {
			$this->error = '獲取上傳文件信息失敗！';
			return false;
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}

	/**
	 * 下載文件
	 * @param  number $id 文檔ID
	 * @return boolean    下載失敗返回false
	 */
	public function download($id){
		$info = $this->find($id);
		if(empty($info)){
			$this->error = "不存在的文檔ID：{$id}";
			return false;
		}

		$File = D('File');
		$root = C('DOWNLOAD_UPLOAD.rootPath');
		$call = array($this, 'setDownload');
		if(false === $File->download($root, $info['file_id'], $call, $info['id'])){
			$this->error = $File->getError();
		}
	}

	/**
	 * 新增下載次數（File模型回調方法）
	 */
	public function setDownload($id){
		$map = array('id' => $id);
		$this->where($map)->setInc('download');
	}

	/**
	 * 保存為草稿
	 * @return true 成功， false 保存出錯
	 * @author huajie <banhuajie@163.com>
	 */
	public function autoSave($id = 0){
		$this->_validate = array();

		/* 獲取文章數據 */
		$data = $this->create();
		if(!$data){
			return false;
		}

		$file = json_decode(think_decrypt(I('post.file_id')), true);
		if(!empty($file)){
			$data['file_id'] = $file['id'];
			$data['size']    = $file['size'];
		}

		/* 添加或更新數據 */
		if(empty($data['id'])){//新增數據
			$data['id'] = $id;
			$id = $this->add($data);
			if(!$id){
				$this->error = '新增詳細內容失敗！';
				return false;
			}
		} else { //更新數據
			$status = $this->save($data);
			if(false === $status){
				$this->error = '更新詳細內容失敗！';
				return false;
			}
		}

		return true;
	}

}