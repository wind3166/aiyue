<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麥當苗兒 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Logic;
use Think\Model;

/**
 * 文檔模型邏輯層公共模型
 * 所有邏輯層模型都需要繼承此模型
 */
abstract class BaseLogic extends Model{

	/**
	 * 構造函數，用於這是Logic層的表前綴
	 * @param string $name 模型名稱
     * @param string $tablePrefix 表前綴
     * @param mixed $connection 數據庫連接信息
	 */
	public function __construct($name = '', $tablePrefix = '', $connection = '') {
		/* 設置默認的表前綴 */
		$this->tablePrefix = C('DB_PREFIX') . 'document_';
		/* 執行構造方法 */
		parent::__construct($name, $tablePrefix, $connection);
	}

	/**
	 * 獲取模型詳細信息
	 * @param  integer $id 文檔ID
	 * @return array       當前模型詳細信息
	 */
	public function detail($id){
		$data = $this->field(true)->find($id);
		if(!$data){
			$this->error = '獲取詳細信息出錯！';
			return false;
		}
		return $data;
	}

	/**
	 * 獲取段落列表
	 * @param  array $ids 要獲取的段落ID列表
	 * @return array      段落數據列表
	 */
	public function lists($ids){
		$map = array();
		if(1 === count($ids)){
			$map['id'] = $ids[0];
		} else {
			$map['id'] = array('in', $ids);
		}

		$data = $this->field(true)->where($map)->select();
		$list = array();
		foreach ($data as $value) {
			$list[$value['id']] = $value;
		}
		return $list;
	}

	/**
	 * 新增或者更新數據
	 * @author huajie <banhuajie@163.com>
	 */
	abstract public function update($id = 0);

	/**
	 * 保存為草稿
	 * @author huajie <banhuajie@163.com>
	 */
	abstract public function autoSave($id = 0);
}